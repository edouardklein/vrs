
%In Plan 9, the \texttt{listen} and \texttt{listen1} commands are in charge of the server's network transparency: on an inbound call, they \texttt{exec} the specified program, relaying data between the network and the program's file descriptors 0, 1 and 2. The \texttt{exec}ed program need not care where the data comes from and where the reply should go.

%As for security, some servers will be run as the \texttt{none} user (a kind of \texttt{nobody} FIXME:check), some as the user who started the \texttt{listen} command, and finally some will use the \texttt{auth} library (described in \citet{secuplan9}'s write-up of Plan 9's security model or in the man page \url{https://9p.io/magic/man2html/2/aut}) to switch their \texttt{uid} to the \texttt{uid} chosen by the author of the inbound network call.

%This is secure because in Plan 9, trust is earned by the client (e.g. \texttt{alice}) proving to the remote factotum that she is who she says she is. The server uses a very fine-grained capability given to it after the authentication is complete to switch its \texttt{uid} to \texttt{alice}. There is nothing else this capability is good for. The process can not switch its identity to anybody else or gain any other right. This is enforced by the kernel.

%Some servers are not completely security agnostic because they must still use \texttt{auth}'s functions to switch their identity, but the boilerplate is minimal (less than 10 lines, error managment included) and wrappers such as \texttt{rexexec} exist that will do the identity switching before \texttt{execl}-ling to the truly security agnostic server.

%In practice, some servers expecting UNIX clients or using deprecated forms of authentication such as \texttt{telnetd} or \texttt{ftpd} are not security agnostic at all. Others (e.g. \texttt{cdfs}) are completely security agnostic and their actions will only be limited by the kernel, given their owner's \texttt{uid}.

%On Linux by contrast, to change its owner a process must be privileged. Even the newly introduced capability system can not restrict who the new owner of a process will be. If a process can change its owner, it can change it to anyone.

%  If we were to naively mimic Plan 9's security model, we would have to start all non security-agnostic servers as privileged processes, let them check that the client is who she says she is (using \texttt{pam}, the boilerplate is minimal) and switch their identity.
%  A rogue or compromised server could change its identity to any user. The linux capability system could limit the damage (by disallowing file access for example) but not by much, especially given that at this early stage the process is unlikely to have lost many capabilities (restrictions usually happen later when the startup is done).


Given a dedicated wrapper able to interface any 9P request coming from \texttt{vrs} into understandable inputs, our solution turns any UNIX local program into a ``network transparent and security agnostic'' server - which obeys to any incoming order, no matter if they come via the standard shell, from another process or from the network. This achieves the porting of Plan 9's distributed architecture on UNIX. The emulation of a single UNIX machine by several specialised entities is also made possible.

In our usecase example, if Alice has a read-only access on Charlie's files besides her own directory and tries to erase them, then her process running on Bob's machine must try to do so: the kernel won't allow him to do so in agreement with the files' permission bits. Conceptors may thus narrow their focus on building efficient ``security agnostic'' servers before wrapping them in a secure layer. As their access rights are delineated by the kernel, these serving processes shall be obediant to any request they receive. It doesn't matter how flawed their implementations are, since their resilience against attacks is no less than the resilience of the authentication mechanism and the system's user rights management.

Since simple systems agregating modules often procure greater security than complex programs, the combination of an authentication mechanism, a multiplexer and a process working as a NT-SA server promises a high-leveled security with an easy maintainance for the adminstrator. Splitting a program into modules also allows to refine the permissions granularity and to keep the server itself unprivileged. This system is therefore no weaker than a common server, and is more resilent to attacks based on privilege escalation.

\appendix
\section{Configuration}
\paragraph{}
We create two separated user accounts: \textit{alice} for the client and \textit{bob} for the wrapping authentication server. We assume all connections are already TLS-encrypted and we will not consider how to actually implement it. Indeed, the user authentication is part of the application layer of the OSI model and involves neither transport nor session layers, whereas TLS only authenticates the server host machine and garantees the information integrity and and confidentiality.
\subsection{Client configuration}
\subsubsection{Prerequisites}
\paragraph{}
\texttt{Atlantis} shall have the plan9-like utilitaries \texttt{srv}, \texttt{mount} and \texttt{factotum} installed from the \texttt{plan9port} package \cite{plan9port}. It is preferable to install \texttt{plan9port} at the default location \textit{/usr/local/plan9} --but the reader may replace it with his favorite installation path. The privileged \texttt{mount} command should also be available, and the client user (called \textit{alice} here) must have the right to mount filesystems as \textit{root}.

\subsubsection{Running the client}
\paragraph{} We wrapped the client's run command in four shell scripts: two to respectively mount (\texttt{.mount}) and unmount (\texttt{.unmount}) the remote ressources on the local machine, and two to launch/stop the factotum agent (resp. w/ .launch\_factotum and .stop\_factotum) needed for authenticating purposes during mounting. Scripts are given in Appendix B. When the client starts, it performs a succession of tasks listed in the next paragraphs.

\paragraph{Connecting to a 9P server} To mount the remote ressource into the client's home directory, we first need to start the network file service, which is done by calling the plan9port command \texttt{srv} \cite{srv}. \texttt{srv}, when provided a distant address \texttt{address[:port]} and a server name \texttt{srvname}, will dial \texttt{address:port} (by default \texttt{address:564}), initialize the connection to serve the 9P protocol, and expose the distant address on a local UNIX socket in the current name space as \texttt{srvname} (in our case at \textit{/tmp/ns.alice.:0/srvname}). \texttt{srv} supports two options. Among them, the \texttt{-a} option is userful here to tell \texttt{srv} that we want to set an authenticated connection with the remote server, and that it must use \texttt{factotum} to do so. The full command is as such:
\begin{verse}
\texttt{srv -a address[:port] srvname}
\end{verse}
Once this binding is done, the user (\textit{alice} in our example) has to mount the UNIX socket into his home directory (or any prefered custom path). Calling \texttt{v9fs} may be done using the kernel \texttt{mount} command \cite{mount}, launched by user \textit{alice} as root, with the UNIX socket (here \textit{/tmp/ns.alice.:0/srvname}) as source and \textit{/exact/desired/path} as destination. \texttt{mount} must be provided the \texttt{-t 9p} option to declare that the UNIX socket uses the 9P protocol. The following 9P options shall also be provided: \texttt{trans=unix}, \texttt{uname=\$USER} (found in \texttt{srv} manpage \cite{srv}, but \textbf{I don't see the interest of it}, since it is overwritten by \texttt{srv}), \texttt{dfltuid=`id -u`} (to start a new process owned by current user, but same remark as uname), and \texttt{dfltgid=`id -g`} (to give the ownership to current group, but \textbf{I don't see the interest} for the same reasons). To be sure to refer to the correct UNIX socket, the call to mount may refer to \texttt{namespace} command, and address it by \textit{`namespace`/srvname}, with \textit{srvname} the chosen alias when initialising the connection at the \texttt{srv} step. The full syntax is:
{
  \begin{verse}
  \footnotesize
  \texttt{sudo mount -t 9p -o trans=unix,uname=\$USER,dfltuid=`id~-u`,dfltgid=`id~-g`} \\
  \texttt{`namespace`/srvname /exact/desired/path}
  \end{verse}}
\paragraph{Authenticating with factotum}
Provided the \texttt{-a} option, \texttt{srv} will attempt to call the process \textit{factotum} \cite{factotum} in the current namespace, and won't initialise the connection if it fails to do so.  A factotum instance must therefore be started before any attempt at connecting. Starting factotum during boot time is recommended, since other processes may also want to use it for various authentication purposes. \\
Factotum is a protocol agnostic agent that doesn't need to actually understand a cryptographic protocol to successfully authenticate: all it needs is a \texttt{Proto} structure and methods that tells him what to do when receiving a ``challenge'' (that may just be receiving a public key --when using ssh--, or for instance a zero knowledge challenge). In our example, we will attempt to connect to \texttt{vrs} using the texttt{p9sk1} (Plan9 Shared-Key protocol \cite{secuplan9}) authentication protocol, and we must provide to factotum a corresponding key:
\begin{verse}
  % \texttt{key proto=p9any role=client} \\
     \texttt{key proto=p9sk1 role=client user=alice dom=c3n !password=$secret$}
 % \texttt{key proto=gq role=client dom=c3n user=alice !priv=$secret \in (\mathbb{Z}/n\mathbb{Z})^*$}
   \end{verse}
   We could directly invoke Factotum's method associated with the p9sk1 protocol (\texttt{start proto=p9sk1 role=client user=alice dom=c3n}), but we would lose in modularity. It is indeed the system administrator's choice to trust a particular authentication protocol and deploy it on the network. Instead, we use Factotum's pseudo-protocol \texttt{p9any} to negociate with the server which protocol to use then when authenticating. We thus only need to initiate a new factotum conversation without any identity: \texttt{start proto=p9any role=client}. The key has the \texttt{role=client} attribute to limit its usage to client authentication, and avoid that an attacker uses the client as a server to then bounce onto the remote ressource, spoofing the client identity. This attribute is also used by factotum to pick the method needed to perform the authentication in its database. The \texttt{p9sk1} key has two public additional attributes \texttt{dom} and \texttt{user}, which are shared information with the \texttt{factotum} process on the server (\textit{user@dom} being public), and a secret attribute \texttt{!password} shared with the central authority, that shall not escape factotum storage, as indicated by the exclamation symbol. This secret's purpose is explained in Section 4. \\
%We could directly invoke Factotum's method associated with the GQ protocol (\texttt{start proto=gq role=client user=alice dom=c3n}), but we would lose in modularity. It is indeed the system administrator's choice to trust a particular authentication protocol and deploy it on the network. Instead, we use Factotum's pseudo-protocol \texttt{p9any} to negociate with the server which protocol to use then when authenticating. We thus only need to initiate a new factotum conversation without any identity: \texttt{start proto=p9any role=client}. The key has the \texttt{role=client} attribute to limit its usage to client authentication, and avoid that an attacker uses the client as a server to then bounce onto the remote ressource, spoofing the client identity. This attribute is also used by factotum to pick the method needed to perform the authentication in its database. The \texttt{gq} key has two public additional attributes \texttt{dom} and \texttt{user}, which are shared information with the \texttt{factotum} process on the server (\textit{user@dom} being public), and a secret attribute \texttt{!priv} that shall not escape factotum storage, as indicated by the exclamation symbol. This secret's purpose is explained in Section 4. \\
Before the authentication, the user needs to start \texttt{factotum} (this is typically done when the user logs in on his machine). The user may just start the process without any argument, but it si recommended to declare a mount point \texttt{-m /mount/path} to be able to manually add or remove keys, to disable an initial dial attempt to a secure storage called \texttt{secstore} (option \texttt{-n} and to hard set the service name to \textit{factotum}, so that \texttt{srv} doesn't fail to call it. To enable debugging, options \texttt{-Dd} may also be passed to \texttt{factotum}:
\begin{verse}
  \texttt{factotum [-Ddn] -m /mount/point/ -s factotum}
\end{verse}


%When launched, factotum will attempt to retrieve keys from a provided secure storage called \texttt{secstore}, served by the \texttt{secstored} plan9port file server (see. Authentication server configuration), it asks the user for a password to connect to the storage, gets the \textit{factotum} file located at the root of it and copies the content into the ctl file. The use of a secstore is optional,  and to avoid bad behavior, it is recommended to either activate it and specify its location, by exporting the environment variable \texttt{secstore} with the command \texttt{export secstore=addr} (with addr in the plan9 syntax, for instance:  'tcp!address!port'), or to deactivate its use with the \texttt{-n} option. In this case, the user has to manually \texttt{cat} the keys into \texttt{ctl} file located at \textit{/mount/point/ctl}:
% \\ \begin{tabular}{l}
%   \texttt{export secstore=addr} \\
%   \texttt{factotum [-Ddn] -m /mount/point -s factotum)}
% \end{tabular}

Factotum doesn't need to be stopped, and may run until the user logs out. If the user wants to reset it, a small script (.stop\_factotum) is provided to force factotum to stop in Appendix B.

\paragraph{unmounting the ressource}
Unmounting is easily done with the umount command:
\begin{verse}
  \texttt{sudo umount -t 9p /exact/desired/path}
\end{verse}

\texttt{srv} instance may also be stopped with the \texttt{killall} command:
\texttt{killall srv}

\subsection{Server implementation}
\subsubsection{Abstract}
\paragraph{} In this paper, we concentrate on authenticating processes and only implemented the secure environment \texttt{vrs}, that will act as the server is the Plan 9 Security model \cite{secuplan9}, forwarding all authentication attempt to factotum before forking into the NTSA server itself. The 9fans community already ported Plan9's \texttt{ramfs} and \texttt{u9fs}, which are network-transparent (although they speak using Plan9's 9P protocol, they communicate via their standard input and output and are not aware of their interlocutor's locations) and security-agnostic (they does not support authentication).

\subsubsection{Prerequisite}
First, the plan9port library \cite{plan9port} has to be installed on the server hosting the ressources to share, as well as our \texttt{vrs} extension. Used in conjunction with \texttt{9pserve} (that merges several client requests into a single conversation), \texttt{vrs} will multiplex a single connection to several services depending on the performed authentication.

\subsubsection{Server implementation}
\paragraph{The vrs multiplexing daemon} In section 2.1 we chose a 9P client (\texttt{srv}) coded by the 9fans comunity. In order to stay network transparent, we also put the choice to interface web sockets with the standard I/O with the minimal \texttt{9pserve} process which forwards and merges 9P requests from the network to the shell. Our auth wrapper shall therefore only authenticate requests from its standard input using factotum, before forking, executing the NTSA server under the client identity and then forwarding the related application data to this new child. As it performs a task similar to \texttt{srv} on the server side, we called it \texttt{vrs} (\texttt{srv}'s syntaxic mirror). \texttt{vrs} opens and maintains user sessions on the local server. Upon each authentication request, it first interfaces the client with its owner's factotum instance. When the authentication is successful, it receives factotum's confirmation to grant a user access. It then forks and changes the child's UID. The child process execs an NT-SA server (for instance \texttt{ramfs}). On its side, the parent's process maintains the child processes' list and interfaces each of them with the associated client (although all conversations transit via the standard output, each conversation is uniquely identified and \texttt{9pserve} forwards them to the right client).

\paragraph{Starting vrs} Launching \texttt{vrs} does not require any option since it doesn't serve any filesystem. The option -D may be used to make it chatty and make the debug easier. Future implementations will also add an address option (\texttt{-a addr}) to specify on which address 9pserve (launched by vrs) should listen on. For now vrs just serves on a UNIX socket, which must be forwarded by a second 9pserve instance onto a TCP socket. This bad behavior will be fixed when implementing the \texttt{-a} option. By default vrs listens on the UNIX socket 'authserver':
\begin{verse}
  \texttt{vrs [-D]}  
\end{verse}

\paragraph{Binding vrs to a network socket}
For now, we use a second 9pserve instance to broadcast the listened UNIX socket on a TCP socket. In the future, this step will be directly handled by an initial fork of vrs. Separating the authentication wrapper and the listener --besides commiting to the network transparency principle-- allows the system administrator to refine privileges, only granting effective root privilege to the listener (if the port is restricted). \texttt{vrs} shall run under limited privilege most of the time, and gain root privileges only when swiching user before starting the NTSA process. The command to use is: \\
\begin{verse}
  \texttt{9pserve -c vrssocket 'tcp!*!port'}
\end{verse}

where vrssocket is the UNIX socket on which \texttt{vrs} listens (by default 'unix!authserver'), and \texttt{port} the port onto which 9pserve should make \texttt{vrs} available on the network.

\subsubsection{Completing the authentication with factotum}
When receiving a 9p \textit{Tauth} message (typically when a new client wants to access a ressource),\texttt{vrs} will attempt to call the process \textit{factotum} \cite{factotum} in his owner's namespace, and won't establish the connection if it fails to do so. A factotum instance must therefore be started on the server machine before any attempt of connection. In our example, we attempt to authenticate using \texttt{p9sk1} authentication, and we must provide to factotum the corresponding key:
\begin{verse}
  \texttt{key proto=p9sk1 role=server dom=c3n user=alice !password=$secret$}
\end{verse}

%This key is our Guillou-Quisquater public key needed to verify the client's claim to own the secret part. We can see here that no secret is stored on the server side, so their is no risk that the server impersonates \texttt{alice} maliciously. Launching factotum on the server side is done with the same command as on the client side, since its use is independant of the role of each machine on the network.

The server sends back to the client a file descriptor in which the client may read or write to talk to the server's factotum process. A dialog is by this way established between the client's and the server's \texttt{factotums}. If the authentication successes, an \texttt{AuthInfo} structure is generated, and the client may attach to the filesystem for which he wanted to authenticate. Upon a \texttt{Tattach} request, the NTSA server is launched specially for this given client under his identity and then all following requests from the client are forwarded to this server. When the connection is clunked by the client, the NTSA server is stopped.


% \subsection{Secstore server configuration (optional)}
% Factotum's server keys may be stored on a secure storage owned by any user on any machine, which associates to each user (authenticated with a password) a dedicated directory with read restrictions to avoid secrets to be stolen. This secret storage is implemented as part of \textit{plan9port}. To configure it, two commands are given: \texttt{secstore} \cite{secstore} and \texttt{secuser} \cite{secstored}. 
% \\ To add a new user, the secure store owner types the command
% \begin{tabular}{l}
%   \texttt{secuser \textit{username}}
% \end{tabular}
% the process asks a password (to be typed by the user) and a validity time period. Other options may be left to default.
% \\The secure storage is served by the file server \texttt{secstored} \cite{secstored}. To bind it to a socket, the storage administrator uses the \texttt{secstored} command with \texttt{-s} option to declare the ports to listen on:
% \begin{tabular}{l}
%   \texttt{sectored -s 'tcp!*!port'}
%   \end{tabular}
% \\ Once his account created, a user may upload his keys (stored as the \textit{factotum} file) into the storage with the \texttt{secstore} command, providing the server address (\texttt{-s} option), his identity (\texttt{-u} option) and the file to upload (\texttt{-p} option):
% \begin{tabular}{l}
%   \texttt{secstore -s 'tcp!address!port' -u user -p factotum}
% \end{tabular}{l}
% \\ As factotum starts, it will query the provided secret storage with the same command to retrieve the \textit{factotum} file, authenticating as his owner and using the \texttt{-g} option:
% \begin{tabular}{l}
%   \texttt{secstore -s 'provided address' -u owner -g factotum}
% \end{tabular}
% \paragraph{}This secure store may be located on the client machine or on a remote place, and may be replaced by any prefered key storage solution (able to write keys into factotum's \texttt{ctl} file)

% \section{Network Diagram}
% \diagram{5}

% % \section{The Guillou-Quisquater (GQ) zero-knowledge proof}

% % The GQ protocol has been proposed by Louis Guillou and Jean-Jeacques Quisquater to prove the knowledge of a clear message associated to a public RSA cypher text. This interactive protocol may be derived  into a non-iteractive authentication protocol by the Fiat-Shamir heuristic, but we put the choice on keeping this protocol interactive.

% % This protocol requires a RSA key $(N, e, d)$ with $e$ prime owned by an identification authority (AC) as well as a public hash function able to encode a public identity $I$ (typically user@domain) into a unit of the  quotient ring $\mathbb{Z}/n\mathbb{Z}$. The RSA key is typically owned by the authentication authority of the subnetwork on which a user wants to identify. This authority gives to every user their secret identity $S=I^{-d}$. To authenticate to another party, the user will then have to prove that he owns such secret $S$

% % The authentication scheme is built as a $\sigma$ protocol. When Alice wants to identify to Bob, she processes as follows:
% % \begin{enumerate}
% % \item commitment
% %   \begin{itemize}
% %   \item Alice picks a random unit $r \in \mathbb{Z}/n\mathbb{Z}$
% %   \item Alice computes $x = r^{e} mod n$ and sends it to Bob
% %   \end{itemize}
% % \item challenge
% %   \begin{itemize}
% %   \item Bob chooses a random $c$ such as $1 \leq c \leq e$
% %   \item Bob sends $c$ to Alice
% %   \end{itemize}
% % \item response
% %   \begin{itemize}
% %   \item Alice computes $y=rS^{c}_{A}$ and sends it to Bob
% %   \item Bob checks that $x=I^{c}_{A}y^{e}$ and that $x\neq 0$
% %   \end{itemize}
% % \end{enumerate}

% % This protocol may be followed several times in order that Bob has sufficient reason to trust Alice. As long as Bob wants so send challenges to Alice, the handshake continues.

% \newpage
% \section{Client scripts}
% \subsection{Mounting/Unmounting the remote shared file tree}
% \subsubsection*{.mount}
% \begin{lstlisting}[frame=single]
% #!/bin/bash
% service=fileserver

% if [ $# = 1 ]
% then
%     srv -a 127.0.0.1 $service
%     if sudo mount -t 9p -o trans=unix,uname=$USER,
%         dfltuid=`id -u`,dfltgid=`id -g`
%             --source `namespace`/$service --target $1
%     then
%         echo 'remote file tree mounted at $1'
%     else
%         echo 'could not mount remote file tree at $1'
%     fi
% else
%     echo 'usage: .mount mtpt'
% fi
% \end{lstlisting}

% \subsubsection*{.unmount}
% \begin{lstlisting}[frame=single]
% #!/bin/bash
% service=fileserver

% if [ $# = 1 ]
% then
%     result=unmount $1
%     killall srv
%     rm -rf /tmp/ns.$USER.:0/$service
%     if [result]
%     then
%         echo 'remote file tree unmounted'
%     else
%         echo 'unable to unmount file tree located at $1'
%     fi
% else
%     usage: .unmount mtpt
% fi
% \end{lstlisting}
% \newpage
% \subsection{Starting and stopping factotum agent}

% \subsubsection*{.launch\_factotum}
% \begin{lstlisting}[frame=single]
% #!/bin/bash

% factotumdir=/path/to/factotum
% PLAN9=/usr/local/plan9 

% rm -r $factotumdir 2>/dev/null
% rm -r $factotumdir 2>/dev/null
% mkdir $factotumdir
% $PLAN9/bin/factotum -D -d -m $factotumdir -s factotum -a 'tcp!localhost!5356'
% \end{lstlisting}

% \subsubsection*{.stop\_factotum}
% \begin{lstlisting}[frame=single]
% #!/bin/bash

% factotumdir=/path/to/factotum

% killall factotum
% rm -r $factotumdir 2>/dev/null
% rm -r $factotumdir 2>/dev/null
% \end{lstlisting}

% \section{Server scripts}
% %\subsection{xinetd configuration}
% \subsubsection*{factotum}
% same scripts as on the client machine
% \subsubsection*{launching the server}
% \texttt{vrs -D}\\
% \texttt{sudo 9pserve -c authserver 127.0.0.1:564}
% \newpage

% glossary

\begin{anfxnote}{Défauts de Kerberos}
  \verb|https://people.eecs.berkeley.edu/~fox/summaries/glomop/kerb_limit.html https://www.cs.cmu.edu/afs/cs/academic/class/15827-f98/www/Slides/lecture3/base.033.html http://www.cs.cmu.edu/~412/lectures/L08_Factotum.pdf slide 17|
\end{anfxnote}