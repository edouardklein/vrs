\message{ !name(main.tex)}% -.-latex-.-

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% main.tex                                                           %%
%% An article explaining a basic network configuration using          %%
%% the factotum authentication on a linux network                     %%
%% author: Guillaume Gette                                            %%
%% MIT licence                                                        %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper]{article}

%packages
\usepackage{amsmath}
\usepackage{url}
\usepackage{array}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{xcolor}
\usepackage{listings}

\begin{document}

\message{ !name(main.tex) !offset(-3) }

\lstset{language=sh}
\author{Guillaume Gette\\C3N}
\title{A basic UNIX network configuration using the factotum authentication}
\date{\today}
\maketitle

\begin{abstract}
  % Authentication is critical when it concerns securing sensitive information on a network, and often becomes a nightmare for network administrators when it comes to key management. Eventually standard solutions like Kerberos and SSH are heavy to secure or manage (Kerberos suffers a shared  private key vulnerability and SSH requires each server to personnally know each authorised user (i.e. having his/her public key stored).\newline
  % The 9P protocol allows many different types of resources to be accessed easily. It relies on the factotum agent to authenticate client processes, as described in Russ Cox's article ``Security in Plan 9'' \cite{secuplan9}. \newline In this paper is presented a basic configuration using the 9P protocol with a factotum authentication to share a repository over a local network.
The key words ``MUST'', ``MUST NOT'', ``REQUIRED'', ``SHALL'', ``SHALL
  NOT'', ``SHOULD'', ``SHOULD NOT'', ``RECOMMENDED'',  ``MAY'', and
  ``OPTIONAL'' in this document are to be interpreted as described in
        RFC 2119. \cite{rfc2119}
\end{abstract}
\section{Introduction}
Consider you want to secure a corporate network composed of 10 servers (including http, incoming and outcoming mail and file servers) and a hundred of clients. One solution to enable authentication is using SSH. Problem : each client needs to know all of the public keys of the servers on which he wants to log on, and each server also needs to know the clients' keys. This is a pain to configure and manage for the administrator ! You could also deploy Kerberos authentication for a centralised authentication. Alas Kerberos implies secret key sharing : this means that if one machine is pawned, the whole system is powned.

Developped since 1987, the operating system Plan 9 from Bell Labs includes a distributed authentication mechanism, in which each party runs and delegates any authentication attempt to a local process called \texttt{factotum}. Every attempt is therefore a dialog between two factotum instances, and is compatible with zero knowledge proof : sharing private keys is not mandatory and no secret transits on the network. This method has two advantages : the great modularity of the authentication mechanism, which makes it easier to administrate, and the possibility to deport the authentication code out of the servers implementation. Provided that a reliable authentication mechanism and a secure implementation of it are made available, we can only focus on building efficient servers before adding the security layer on top of them. It doesn't matter how flawy their implementations are, since their resilience against attacks is no less than the resilience of the authentication mechanism.

Moreover, if we also manage to mask the source of the requests from the server, restricting its opened ports to the standard input and output, we obtain a ``network transparent and security agnostic'' server - which serves any file in its filesystem to any client connected to its standard input and output. Since simple systems agregating modules often procure greater security than complex programs, the combination of an authentication mechanism, a multiplexer and an NT-SA server promises a high-leveled security with an easy maintainance for the adminstrator. Splitting a program into modules also allows to lower permissions, and to keep the server itself unpriviledged. This system is therefore no weaker than a common server, and is more resilent to attacks based on priviledge escalation.

Most of Plan 9's utilitaries (including Factotum) have been ported on Linux systems by the 9fans community, and releases under the name Plan 9 from Outer Space. Unfortunately this port's purpose is to allow UNIX systems to connect on Plan 9 and Inferno servers, and not to adapt the Plan 9 authentication mechanism on Linux and BSD networks -since Linux and Plan9 identity management greatly differ.

In this paper, we expose a new Linux distributed system based on Plan 9's authentication and compatible with pluggable NT-SA servers :
\begin{itemize}
\item We first adapted the Plan 9's distributed authentication mechanism based on factotum exchanges for UNIX systems. Whereas clients where already fully implemented on Linux by the 9fans community, we ported the authentication mechanism on the server by integrating the ability to converse with a local Factotum process on the server side.
\item We then wrote the resulting authentication mechanism as a PAM module, in order to simplify -and therefore securise- our implementation.
\item In order to lower the number of secrets transiting on the network, we also implemented the Guillou-Quisquater zero-knowledge identification protocol, which is based on RSA, but only requires a unique public RSA key on a domain to authenticate users.

\section{Authenticating on the network}
\paragraph{}Since all communications between each party is done via the 9P protocol (which usually uses TCP for transport), we do not have to worry about the physical location of each of them, and we may restrict our physical network to a single computer with a loopback interface.

\paragraph{}
We therefore create three separated user accounts : \textit{term} for the client, \textit{fileserver} for the server, and \textit{authserver} (optional), which is a trusted third-party during the authentication process. The use of a secret store to keep factotum's keys is not mandatory, as discussed in section \textbf{2.1.2} paragraph \textbf{Authenticating with factotum}.
\subsection{Client configuration}
\subsubsection{Prerequisites}
\paragraph{}The client's machine shall have plan9 UNIX utilitaries installed from the \texttt{plan9port} package \cite{plan9port}, in particular \texttt{factotum} and \texttt{srv} commands. Then \texttt{plan9port} is installed, preferrably at \textit{/usr/local/plan9} -but the reader may replace it with his favorite installation path. The priviledged \texttt{mount} command should also be available, and the client user (called \textit{term} here) must have the right to mount filesystems as \textit{root}.
\subsubsection{Client implementation}
\paragraph{}Our implementation consists of four shell scripts: two to respectively mount (\texttt{.mount}) and unmount (\texttt{.unmount}) the remote ressources on the local machine, and two to launch/stop the factotum agent (resp. w/ .launch\_factotum and .stop\_factotum) needed for authenticating purposes during mounting. Scripts are given in Appendix B.
\paragraph{Connecting to a 9P server} To mount the remote ressource into the client's home directory, we first need to start the network file service, which is done by calling the plan9port command \texttt{srv} \cite{srv}. \texttt{srv}, when provided a distant address \texttt{address[:port]} and a server name \texttt{srvname}, will dial \texttt{address:port} (by default \texttt{address:564}), initialize the connection to serve the 9P protocol, and expose the distant address on a local UNIX socket in the current name space as \texttt{srvname} (in our case at \textit{/tmp/ns.term.:0/srvname}). \texttt{srv} supports two options. Among them, the \texttt{-a} option is userful here to tell \texttt{srv} that we want to set a authenticated connection with the remote server, and that it must use \texttt{factotum} to do so. The full command is therefore:\\
\begin{tabular}{c}
\texttt{srv -a address[:port] srvname} \\
\end{tabular}
\\ Once this binding is done, the user (\textit{term} in our example) has to mount the UNIX socket into his home directory (or any prefered custom path). Since \texttt{srv} already serves the 9P protocol, the local mounting may be done using the kernel \texttt{mount} command \cite{mount}, launched by user \textit{term} as root, with source the UNIX socket (here \textit{/tmp/ns.term.:0/srvname}) and destination \textit{/exact/desired/path}. To properly mount the UNIX socket, \texttt{mount} must be provided the \texttt{-t 9p} option to declare that the UNIX socket uses the 9P protocol. The following 9P options shall also be provided : \texttt{trans=unix}, \texttt{uname=\$USER} (found in \texttt{srv} manpage \cite{srv}, but I don't see the interest of it, since it is overwritten by \texttt{srv}), \texttt{dfltuid=`id -u`} (to start a new process owned by current user, but same remark as uname), and \texttt{dfltgid=`id -g`} (to give the ownership to current group, but same thing). To be sure to refer to the correct UNIX socket, the call to mount may refer to \texttt{namespace} command, and address it by \textit{`namespace`/srvname}, with \textit{srvname} the chosen alias when initialising the connection at the \texttt{srv} step. Full syntax is : \\
\begin{tabular}{c}
  \texttt{sudo mount -t 9p -o trans=unix,uname=\$USER,dfltuid=`id -u`,dfltgid=`id -g`} \\\texttt{`namespace`/srvname /exact/desired/path} \\
  \end{tabular}
\paragraph{Authenticating with factotum}
Provided with \texttt{-a} option, \texttt{srv} will attempt to call the process \textit{factotum} \cite{factotum} in the current namespace, and won't initialise the connection if it fails to do so.  A factotum instance must therefore be started before any attempt at connecting. Starting factotum during boot time is recommended, since other processes may also want to use it for various authentication purposes. \\
Factotum is a protocol agnostic agent that doesn't need to actually understand a cryptographic protocol to successfully authenticate : all it needs is a file in its \texttt{proto} directory that tells him what to do when receiving a ``challenge'' (that may just be receiving a public key -- when using ssh--, or for instance a zero knowledge challenge). In our example, we will attempt to connect to \texttt{vrs} using the \texttt{p9sk1} authentication protocol, and we must provide to factotum a corresponding key :
\\ \begin{tabular}{l}
  \texttt{key proto=p9any role=client} \\
  \texttt{key proto=p9sk1 role=client dom=custom.domain.name user=term !password=secretpwd}
\end{tabular}
The first key belongs to the pseudo-protocol \texttt{p9any}, which is first used by factotum to negociate with the server which protocol to use then when authenticating : no identity is needed for this. Both keys have the \texttt{role=client} attribute to limit their usage to client authentication, and avoid that an attacker uses the client as a server to then bounce onto the remote ressource, spoofing the client identity. The \texttt{p9sk1} key has three additional attributes \texttt{dom}, \texttt{user} and \texttt{password}, which are shared information with the \texttt{factotum} process on the server, \textit{user@dom} beeing public, and \texttt{password} a secret value that shall not escape factotum storage, as indicated by the exclamation symbol. \\
To start \texttt{factotum}, the user may just launch the command without any argument, but it si recommended to declare a mount point \texttt{-m /mount/path} to be able to manually add or remove keys, and to hard set the service name to \textit{factotum}, so that \texttt{srv} doesn't fail to call it. To enable debugging, options \texttt{-Dd} may also be passed to \texttt{factotum} : \\
\begin{tabular}{l}
    \texttt{factotum [-Dd] -m /mount/point/ -s factotum}
\end{tabular}
When launched, factotum will attempt to retreive keys from a provided secure storage called \texttt{secstore}, served by the \texttt{secstored} plan9port file server (see. Authentication server configuration), it asks the user for a password to connect to the storage, gets the \textit{factotum} file located at the root of it and copies the content into the ctl file. The use of a secstore is optional,  and to avoid bad behavior, it is recommended to either activate it and specifie its location, by exporting the environment variable \texttt{secstore} with the command \texttt{export secstore=addr} (with addr in the plan9 syntax, for instance :  'tcp!address!port'), or to desactivate its use with the \texttt{-n} option. In this case, the user has to manually \texttt{cat} the keys into \texttt{ctl} file located at \textit{/mount/point/ctl} :
\\ \begin{tabular}{l}
  \texttt{export secstore=addr} \\
  \texttt{factotum [-Ddn] -m /mount/point -s factotum)}
\end{tabular}
Factotum doesn't need to be stopped, and may run until the machine shuts down. If the user wants to reinitialise it, a small script (.stop\_factotum) is provided to force factotum to stop in Appendix B.
\paragraph{unmounting the ressource}
Unmounting is easily done with the umount command :
\begin{tabular}{l}
  \texttt{sudo umount -t 9p /exact/desired/path}
\end{tabular}
\texttt{srv} instance may also be stopped with the \texttt{killall} command:
\texttt{killall srv}

\subsection{Server configuration}
\subsubsection{Abstract}
\paragraph{}Since we focus here on configuring a server able to authenticate users with factotum before running a network transparent and security agnostic server in an authenticated environment, we will just use ramfs as the NTSA server.
This server implementation is not recommended in an operational context (everything is stored in the ram and lost when the server stops), but is more convenient to run a such simple server when developping a factotum authentication over a UNIX network.
During production, it may then be replaced by any 9P server interfacing via its standard input/output (or a UNIX socket).
\subsubsection{Prerequisite}
First, the plan9port library \cite{plan9port} has to be installed on the server hosting the ressources to share, as well as our \texttt{vrs} extension. This extension, used in conjunction with \texttt{9pserve} acts as an authentication server multiplexing a single connection to several services depending on the performed authentication.

\subsubsection{Server implementation}
\paragraph{Starting vrs} Launching \texttt{vrs} does not require any option since it doesn't serve any filesystem. The option -D may be used to make it chatty and make the debug easier. Future implementations will also add an address option (\texttt{-a addr}) to specify on which address 9pserve (launched by vrs) should listen on.  \\ For now vrs just serves on a unix socket, which must be forwarded by a second 9pserve instance onto a TCP socket, this bad behavior will be fixed when implementing the \texttt{-a} option. By default vrs listens on the UNIX socket 'authserver'.
\begin{tabular}{l}
  \texttt{vrs [-D]}  
\end{tabular}
\paragraph{Binding vrs to a network socket}
For now, we use a second 9pserve instance to broadcast the listened UNIX socket on a TCP socket. This is done for now to grant root priviledge only to this subprocess (depending on the port number : root priviledge is required to use ports number up to 3000). vrs shall have limited priviledge most of the time, and gain root priviledges only when starting a server owned by another user. The command to use is :
\begin{tabular}{l}
  \texttt{9pserve -c vrssocket  'tcp!*!port'} \\
\end{tabular}
Where vrssocket is the UNIX socket on which \texttt{vrs} listens (by default 'unix!authserver'), and \texttt{port} the port onto which 9pserve should make \texttt{vrs} available on the network.

\subsubsection{Completing the authentication with factotum}
When receiving a 9p \textit{Tauth} message (typically when a new client wants to access a ressource),\texttt{vrs} will attempt to call the process \textit{factotum} \cite{factotum} in his owner's namespace, and won't establish the connection if it fails to do so. A factotum instance must therefore be started on the server machine before any attempt of connection. In our example, we attempt to authenticate using \texttt{p9sk1} authentication, and we must provide to factotum the corresponding key :
\\ \begin{tabular}{l}
  \texttt{key proto=p9any role=server} \\
  \texttt{key proto=p9sk1 role=client dom=custom.domain.name user=term !password=secretpwd}
\end{tabular}
The first key is used by factotum to negociate with the server which protocol to use then when authenticating, this time with the server attribute. The second one is the client's shared key in order to be able to do the authentication without the need of a plan 9 Authserver. Launching factotum on the server side is done with the same command as on the client side, since its use is independant of the role of each machine on the network.

The server sends back to the client a file descriptor in which the client may read or write to talk to the server's factotum process. A dialog is by this way established between the client's and the server's \texttt{factota}. If the authentication successes, an AuthInfo structure is generated, and the client may attach to the filesystem for which he wanted to authenticate. Upon a \texttt{Tattach} request, the NTSA server is launched specially for this given client and then all following requests are forwarded to this server. When the connection is clunked by the client, the NTSA server is stopped.

\subsection{Secstore server configuration (optional)}
Factotum's server keys may be stored on a secure storage owned by any user on any machine, which associates to each user (authenticated with a password) a dedicated directory with read restrictions to avoid secrets to be stolen. This secret storage is implemented as part of \textit{plan9port}. To configure it, two commands are given : \texttt{secstore} \cite{secstore} and \texttt{secuser} \cite{secstored}. 
\\ To add a new user, the secure store owner types the command
\begin{tabular}{l}
  \texttt{secuser \textit{username}}
\end{tabular}
the process asks a password (to be typed by the user) and a validity time period. Other options may be left to default.
\\The secure storage is served by the file server \texttt{secstored} \cite{secstored}. To bind it to a socket, the storage administrator uses the \texttt{secstored} command with \texttt{-s} option to declare the ports to listen on :
\begin{tabular}{l}
  \texttt{sectored -s 'tcp!*!port'}
  \end{tabular}
\\ Once his account created, a user may upload his keys (stored as the \textit{factotum} file) into the storage with the \texttt{secstore} command, providing the server address (\texttt{-s} option), his identity (\texttt{-u} option) and the file to upload (\texttt{-p} option) :
\begin{tabular}{l}
  \texttt{secstore -s 'tcp!address!port' -u user -p factotum}
\end{tabular}{l}
\\ As factotum starts, it will query the provided secret storage with the same command to retrieve the \textit{factotum} file, authenticating as his owner and using the \texttt{-g} option :
\begin{tabular}{l}
  \texttt{secstore -s 'provided address' -u owner -g factotum}
\end{tabular}
\paragraph{}This secure store may be located on the client machine or on a remote place, and may be replaced by any prefered key storage solution (able to write keys into factotum's \texttt{ctl} file)
\section{Changing the user id}
TODO
\section{Simplifying the authentication}

\subsection{The use of the PAM library}
\subsubsection{Delegating the authentication to PAM functions}

For now we used the Plan9 syntax in our C implementation, which restricts the portability of system : only machines with the \textit{plan9port} library installed may run it !
Luckily, since ``all is a file'' according to the Plan 9's philosophy, we may mount a factotum instance as a file system and implement the conversation with this process using the standard UNIX file management library. The resulting is a standard C implementation compatible with any UNIX system. Besides, delegating the authentication process to a PAM library also allows any other application developper to use the factotum authentication with no greater difficulty than using the standard UNIX authentication.

We therefore implemented the factotum conversation mechanism as part as the PAM authentication routine.

Switching users still has to be done !

\subsection{Using Guillou-Quisquater zero-knowledge authentication protocol}



\section{Summary}
To summarise the connection process, once \texttt{vrs} is launched, broadcasted with \texttt{9pserve}, and may call \texttt{factotum} whithin the server ; and that a secure store is configured and available (if this solution is chosen) :
\begin{enumerate}
\item The client starts its factotum
  \item Factotum retrieves its keys from the secstore if needed, or the user makes the keys available
\item The keys are copied into the \texttt{ctl} file
\item The client launches \texttt{srv}, which tries to connect to the server, the request is served by \texttt{9pserve} to \texttt{vrs}
\item \texttt{vrs} starts the authentication request, \texttt{srv} calls \texttt{factotum} to negociate with the server's own factotum, the client's \texttt{factotum} authenticates, and the server's factotum verifies that it shares a key with the client. Once the authentication is done, \texttt{vrs} forks into a process owned by the client, and serves its path
\item \texttt{srv} binds the connection to the UNIX socket called \textit{fileserver}
\item the client calls \texttt{mount} with \textit{root} priviledges to mount the ressource in his local directory
\item The ressource is shared with the client and available on his local machine. He may write, read and execute with his own permissions.
\end{enumerate}
A global graph resumes all the steps is a graphical way at Appendix A.
\appendix
\section{Network topology and scripts execution}
\hspace{-5cm}
\tikzstyle{term}=[minimum width=5cm, minimum height=5cm, rectangle, rounded corners=10pt, text = blue, draw]
\tikzstyle{roundt}=[circle, draw, text=blue]
\tikzstyle{rounds}=[circle, draw, text=red]
\tikzstyle{roundfs}=[circle, draw, text=purple]
\tikzstyle{fileserver}=[minimum width=5cm, minimum height=5cm, rectangle, rounded corners=10pt, text=purple,draw]
\tikzstyle{secstore}=[minimum width=5cm, minimum height=1cm, rectangle, rounded corners=10pt, text=red,draw]
\def\bottom#1#2{\hbox{\vbox to #1{\vfill\hbox{#2}}}}
\tikzstyle{secstoresocket}=[ellipse, draw]
\tikzstyle{termsocket}=[diamond,draw]
\tikzstyle{fileserversocket}=[ellipse,draw]
 \tikzstyle{termfs}=[minimum width=5cm, minimum height=3cm, rectangle, text = blue, draw]
 \tikzstyle{fileserverfs}=[minimum width=5cm, minimum height=5cm, rectangle, text=purple,draw]
 \tikzstyle{secstorefs}=[minimum width=5cm, minimum height=3.5cm, rectangle, text=red, draw]
\def\top#1#2{\hbox{\vtop to #1{\hbox{#2}\vfill}}}
\begin{tikzpicture}
  \node[term] (T) at (-1,1) {\bottom{5cm} term};
  \node[diamond, aspect=3, draw] (ST) at (3.5,1) {unix socket};
  \node[ellipse] (bla) at (3.5,0.2) {\tiny $/tmp/ns.term.:0/fileserver$};
  \node[roundt] (MNT) at (0,-0.2) {\Large $mount$};
  \node[roundt] (SRV) at (0.5,2.5) {\huge $srv$};
  \node[roundt] (FT) at (-2.2,2) {\large $factotum$};
  \node[termfs] (TT) at (-1,-5.5) {\top{7cm} home filesystem};
  \node[text=blue, text width=4.5cm] (TREET) at (-1,-5.7)
       { \hbox to 4.5cm{$\vdash$ .bashrc \hfil}
         \hbox to 4.5cm{$\vdash$ .launch\_factotum \hfil}
         \hbox to 4.5cm{$\vdash$ .stop\_factotum \hfil}
         \hbox to 4.5cm{$\vdash$ factotum \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}$\vdash$ rpc \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}$\vdash$ log \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}$\vdash$ proto \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}$\vdash$ confirm \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}$\vdash$ needkey \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}$\vdash$ ctl \hfil}
         
         \hbox to 4.5cm{$\vdash$ remote \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}$\vdash$ fileserver/ \hfil}

         \hbox to 4.5cm{$\vdash$ .mount\_fs \hfil}
         \hbox to 4.5cm{$\vdash$ .unmount\_fs \hfil}
       };
  
       \node[secstore] (A) at (-1,8.5) {\bottom{5cm}{secstore server}};
       \node[rounds] (SSTR) at (-1,8.5) {\large $secstored$};
       \node[secstoresocket] (SA) at (-1,5.2) {port 5356};
       \node[secstorefs] (TA1) at (-3.6,13.1) {\top{3cm} secure folders};
       \node[text=blue, text width=3.5cm, draw] (TREEA1) at (-3.6,13.1) { term folder \\\hbox to 3.5cm{$\vdash$ factotum \hfil} \hbox to 3.5cm{$\vdash$ ... \hfil}};
       \node[text=red, text width=3.5cm, draw] (TREEA2) at (-3.6,12) { ...};
       \node[secstorefs] (TA2) at (1.6,13.1) {\top{3cm} home filesystem};
       \node[text=blue, text width=3.5cm] (TREEA21) at (1.6,12.9) { term folder \\\hbox to 3.5cm{$\vdash$ .bashrc \hfil} \hbox to 3.5cm{$\vdash$ .launch\_secstore \hfil} \hbox to 3.5cm{$\vdash$ .stop\_secstore \hfil}};
       \node[fileserver] (FS) at (10,6) {\bottom{5cm}{fileserver}};
       \node[fileserversocket] (SFS) at (10,9.2) {port 564};
  \node[fileserverfs] (TFS) at (10,0) {\top{6cm} home filesystem};
  \node[text=purple, text width=4.5cm] (TREEFS) at (10,-0.2)
       { \hbox to 4.5cm{$\vdash$ .bashrc \hfil}
         \hbox to 4.5cm{$\vdash$ .launch\_fs \hfil}
         \hbox to 4.5cm{$\vdash$ .stop\_fs \hfil}

         \hbox to 4.5cm{$\vdash$ share \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}\textcolor{blue}{$\vdash$ folder1/} \hfil}
         \hbox to 4.5cm{\hspace{0.5cm}\textcolor{blue}{$\vdash$ file1}\hfil}
         \hbox to 4.5cm{\hspace{0.5cm}\textcolor{blue}{$\vdash$ ...}\hfil}

         %\hbox to 4.5cm{$\vdash$ u9fs \hfil}
         %\hbox to 4.5cm{\hspace{0.5cm}$\vdash$ u9fs.keys \hfil}
         %\hbox to 4.5cm{\hspace{0.5cm}$\vdash$ u9fs.log \hfil}
         \hbox to 4.5cm{\hfil}
         \hbox to 4.5cm{\hfil}
         \hbox to 4.5cm{\hfil}
         
       };
  
       \node[roundfs] (XFS) at (11.2, 7.2) {\large $9pserve$};
       \node[roundfs] (FFS) at (8.8, 4.7) {\large factotum};
  \node[roundfs,text width=1.5cm, text centered] (UFS) at (11.3,4.8) {{\Large \textcolor{blue}{$ramfs$}} \textcolor{blue}{$NTSA$}};
  \node[roundfs] (VRS) at (8.8,7.3) {\huge $vrs$};

  \draw[-,>=latex] (T) -- (TT);
  \draw[-,>=latex] (T) -- (ST);
  \draw[->,>=latex,color=blue]  (FT.50) to[bend right=30] node[left,pos=.85]{1a. auth} node[left,pos=.68]{1b. GET factotum} (SA.270);
  \draw[->,>=latex,color=blue,line width=3pt]  (SRV.70) to[out=70,in=180] node[pos=.3,above,sloped]{4. connect} node[midway,above,sloped]{5a. auth} node[pos=.7,above,sloped]{6a. bind} (SFS.west);
  \draw[->,>=latex,color=blue,line width=3pt] (SRV.-50) to[bend right=10] node[pos=.45,above,sloped]{6a. bind} (ST.170);
  \draw[<->,>=latex,color=blue] (SRV) -- node[sloped,above]{5a.} node[sloped,below]{auth} (FT);
  \draw[->,>=latex,color=blue,line width=3pt] (MNT) edge[bend right=30] node[midway,below,sloped]{7. mount} (bla.200) edge[bend left=30]  node[midway,below,sloped]{7. mount} (-.8,-7.5);
  \draw[-,>=latex] (A) -- (TA1);
  \draw[-,>=latex] (A) -- (TA2);
  \draw[-,>=latex] (A) -- (SA);
  \draw[->,>=latex,color=red] (SSTR.60) to[bend right=40] node[pos=0.15,left]{2. read access}(-3.4,13.1);
  \draw[<->,>=latex,color=red,line width=2pt] (SSTR.-50) to[bend left=40] node[pos=0.25,sloped,above]{listen} (SA.20);
  \draw[->,>=latex,color=blue] (FT.220) to[bend right=40] node[pos=.42,above, sloped, rotate=-5]{3. copy} node[pos=.28,above,sloped]{keys} node[pos=.18,sloped,above]{into} node[pos=.08,sloped,above]{ctl} (-2.8,-6.8);
  \draw[-,>=latex] (FS) -- (TFS);
  \draw[-,>=latex] (FS) -- (SFS);
  \draw[<->,>=latex,color=purple] (XFS) -- node[above,sloped]{std i/o} node[below,sloped]{5a. auth} (VRS);
  \draw[<->,>=latex,color=blue,dashed] (UFS.140) -- node[below]{5c.}(VRS.-50);
  \draw[->,>=latex,color=purple,dashed] (VRS) -- node[right]{5b. fork} (UFS);
  %r\draw[->,>=latex,color=purple] (VRS) to[bend left=90] node[below,sloped,pos=.7]{5a. auth} (10.2,-1.7);
  \draw[<->,>=latex,color=purple] (VRS) -- node[sloped,above]{5a.} node[sloped,below]{auth} (FFS);
  
  \draw[<->,>=latex,color=purple,line width=2pt] (SFS.-20) to[bend left=30] node[pos=0.5,above,sloped]{listen} (XFS.80);
  \draw[<->,>=latex,color=blue] (UFS) to[bend left=30] node[sloped,below]{6b. r/w access} (9.1,0.3);
  \draw[<->,>=latex,color=blue!50,thick,line width=5pt, loosely dashed] (-.5,-7.6) to[bend right=30] node[sloped,below]{\Large 8. sync} (8,0);
\end{tikzpicture}
\section{The Guillou-Quisquater (GQ) zero-knowledge proof}

The GQ protocol has been proposed by Louis Guillou and Jean-Jeacques Quisquater to prove the knowledge of a clear message associated to a public RSA cypher text. This interactive protocol may be derived  into a non-iteractive authentication protocol by the Fiat-Shamir heuristic, but we put the choice on keeping this protocol interactive.

This protocol requires a RSA key (N, e, d) owned by an identification authority (AC) as well as a public hash function able to encode a public identity (typically user@domain) into a unit of the  quotient ring $\Z/n\Z$ 
\section{Client scripts}
\subsection{Mounting/Unmounting the remote shared file tree}
\subsubsection*{.mount}
\begin{lstlisting}[frame=single]
#!/bin/bash
service=fileserver

if [ $# = 1 ]
then
    srv -a 127.0.0.1 $service
    if sudo mount -t 9p -o trans=unix,uname=$USER,
        dfltuid=`id -u`,dfltgid=`id -g`
            --source `namespace`/$service --target $1
    then
        echo 'remote file tree mounted at $1'
    else
        echo 'could not mount remote file tree at $1'
    fi
else
    echo 'usage: .mount mtpt'
fi
\end{lstlisting}

\subsubsection*{.unmount}
\begin{lstlisting}[frame=single]
#!/bin/bash
service=fileserver

if [ $# = 1 ]
then
    result=unmount $1
    killall srv
    rm -rf /tmp/ns.$USER.:0/$service
    if [result]
    then
        echo 'remote file tree unmounted'
    else
        echo 'unable to unmount file tree located at $1'
    fi
else
    usage: .unmount mtpt
fi
\end{lstlisting}
\newpage
\subsection{Starting and stopping factotum agent}

\subsubsection*{.launch\_factotum}
\begin{lstlisting}[frame=single]
#!/bin/bash

factotumdir=/path/to/factotum
PLAN9=/usr/local/plan9 

rm -r $factotumdir 2>/dev/null
rm -r $factotumdir 2>/dev/null
mkdir $factotumdir
$PLAN9/bin/factotum -D -d -m $factotumdir -s factotum -a 'tcp!localhost!5356'
\end{lstlisting}

\subsubsection*{.stop\_factotum}
\begin{lstlisting}[frame=single]
#!/bin/bash

factotumdir=/path/to/factotum

killall factotum
rm -r $factotumdir 2>/dev/null
rm -r $factotumdir 2>/dev/null
\end{lstlisting}

\section{Server scripts}
\subsection{xinetd configuration}
\subsubsection*{factotum}
same scripts as on the client machine
\subsubsection*{launching the server}
\texttt{vrs -D}\\
\texttt{sudo 9pserve -c authserver 127.0.0.1:564}
\newpage
%bibliography
\bibliographystyle{plain}
\bibliography{biblio}

\end{document}
%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: default
%%% End:
\message{ !name(main.tex) !offset(-376) }
