\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Authenticating on the network}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {section}{\numberline {3}Configuration}{4}
\contentsline {paragraph}{}{4}
\contentsline {subsection}{\numberline {3.1}Client configuration}{5}
\contentsline {subsubsection}{\numberline {3.1.1}Prerequisites}{5}
\contentsline {paragraph}{}{5}
\contentsline {subsubsection}{\numberline {3.1.2}Running the client}{5}
\contentsline {paragraph}{}{5}
\contentsline {paragraph}{Connecting to a 9P server}{5}
\contentsline {paragraph}{Authenticating with factotum}{5}
\contentsline {paragraph}{unmounting the ressource}{6}
\contentsline {subsection}{\numberline {3.2}Server implementation}{6}
\contentsline {subsubsection}{\numberline {3.2.1}Abstract}{6}
\contentsline {paragraph}{}{6}
\contentsline {subsubsection}{\numberline {3.2.2}Prerequisite}{6}
\contentsline {subsubsection}{\numberline {3.2.3}Server implementation}{6}
\contentsline {paragraph}{The vrs multiplexing daemon}{6}
\contentsline {paragraph}{Starting vrs}{6}
\contentsline {paragraph}{Binding vrs to a network socket}{6}
\contentsline {subsubsection}{\numberline {3.2.4}Completing the authentication with factotum}{7}
\contentsline {subsection}{\numberline {3.3}Switching identity}{7}
\contentsline {section}{\numberline {4}Simplifying the authentication}{7}
\contentsline {subsection}{\numberline {4.1}Delegating the authentication to the PAM library}{7}
\contentsline {subsection}{\numberline {4.2}Extending the UNIX user database with Name Service Switch (NSS)}{8}
\contentsline {section}{\numberline {5}Summary}{8}
\contentsline {section}{\numberline {6}Conclusion}{9}
\contentsline {section}{\numberline {A}Network Diagram}{9}
\contentsline {section}{\numberline {B}Client scripts}{10}
\contentsline {subsection}{\numberline {B.1}Mounting/Unmounting the remote shared file tree}{10}
\contentsline {subsection}{\numberline {B.2}Starting and stopping factotum agent}{10}
\contentsline {section}{\numberline {C}Server scripts}{10}
