% -*- mode: latex -*-
\begin{tikzpicture}
  \node[alice] (A) at (-1,1) {\makecell[c]{Atlantis \\\bottom{4.5cm}{alice}}};
  \node[diamond, aspect=3, draw] (AS) at (3.5,1) {unix socket};
  \node[ellipse] (bla) at (3.5,0.2) {\tiny $/tmp/ns.alice.:0/fileserver$};
  \node[roundt] (MNT) at (0.5,-0.7) {\large $mount$};
  \node[roundt] (SRV) at (0.5,2.3) {\huge $srv$};
  \node[roundt] (FTC) at (-2.2,2.3) {\large $factotum$};
  \node[roundt] (foo) at (-2, -0.2) {\Huge $foo$};
  \node[alicefs] (AH) at (-1,-3) {\top{2cm} home filesystem};
  \node[kernel] (KERNA) at (-1, 4.15) {Linux};
  \node[text=alicecolor, text width=4.5cm] (TREEA) at (-1,-3.2){
    \hbox to 4.5cm{$\vdash$ mnt \hfil}
    \hbox to 4.5cm{\hspace{0.5cm}$\vdash$ fileserver/ \hfil}
    
    \hbox to 4.5cm{$\vdash$ ... \hfil}
  };
  \node[factotum] (AUTH) at (-1,8.5) {\bottom{5cm}{central authority}};
  \node[rounds] (FTS) at (-1,8.5) {\large $factotum$};
  %\node[factotumsocket] (AUTHS) at (3.3,8.5) {port 6666};
       
  \node[fileserver] (FS) at (10,6) {\makecell[c]{Bermudes\\ \bottom{4.5cm}{fileserver (bob)}}};
  % \node[fileserversocket] (SFS) at (5.8,6) {port 9999};
  \node[kernel] (KERNFS) at (10, 9.15) {Linux}; 
  \node[fileserverfs] (TFS) at (10,1.5) {\top{3cm} filesystem};
  \node[text=bobcolor, text width=4.5cm] (TREEFS) at (10,0.8){ 
    \hbox to 4.5cm{$\vdash$ /home/share \hfil}
    \hbox to 4.5cm{\hspace{0.5cm} \textcolor{alicecolor}{$\vdash$ alice} \hfil}
    % \hbox to 4.5cm{\hspace{1cm}\textcolor{alicecolor}{$\vdash$ folder1/} \hfil}
    % \hbox to 4.5cm{\hspace{1cm}\textcolor{alicecolor}{$\vdash$ file1}\hfil}
    % \hbox to 4.5cm{\hspace{1cm}\textcolor{alicecolor}{$\vdash$ ...}\hfil}
    \hbox to 4.5cm{\hspace{0.5cm} \textcolor{charliecolor}{$\vdash$ charlie} \hfil}
    \hbox to 4.5cm{\hfil}
    \hbox to 4.5cm{\hfil}
    \hbox to 4.5cm{\hfil}
  };
  
  %\node[roundfs] (XFS) at (8.8,5) {\large $9pserve$};
  \node[roundfs,text width=1.5cm, text centered] (UFS) at (11.3,4.8) {{\textcolor{alicecolor}{$SA$} } {\Large \textcolor{alicecolor}{$server$}}};
  %\node[roundfs] (VRS) at (9.8,7.3) {\huge $vrs$};
  
      
  \draw[-,>=latex] (A) -- (AH);
  %\draw[-,>=latex] (A) -- (AS);
  % \draw[->,>=latex,color=alicecolor]  (FTC.50) to[bend right=30] node[left,pos=.85]{1a. auth} node[left,pos=.68]{1b. GET factotum} (AUTHS.270);
    
  % \draw[-,>=latex] (AUTH) -- (AUTHH);
  %\draw[-,>=latex] (AUTH) -- (AUTHS);

  %\draw[<-,>=latex,color=factotumcolor,line width=2pt] (FTS) to[bend left=40] node[pos=0.4,sloped,above]{listen} (AUTHS);
  %\draw[->,>=latex,color=bobcolor,line width=2pt] (SFS) to[bend right=30] node[pos=0.5,below,sloped]{listen} (XFS);

  \draw[-,>=latex] (FS) -- (TFS);
  %\draw[-,>=latex] (FS) -- (SFS);

  \draw[->,>=latex,color=alicecolor!50,thick,line width=5pt,loosely dashed] (SRV) to[bend right=20] node[left,pos=0.9]{auth alice} (FS.180) to[bend right=20] (FTS);
  \draw[->,>=latex,thick,loosely dashed] (KERNA) to[pos=0.3,bend right=10] node[right]{identify alice} (FTS);
  \draw[->,>=latex,thick,loosely dashed] (KERNFS) to[bend right=10] node[sloped,above]{identify alice} (FTS);
  
  %\draw[->,>=latex,color=alicecolor,line width=3pt]  (SRV.70) to[out=70,in=180] node[pos=.25,above,sloped]{1. connect} node[pos=.6,above,sloped]{2a. auth} (SFS.west);
  \draw[->,>=latex,color=alicecolor] (SRV) -- node[sloped,above]{auth} node[sloped,below]{alice} (FTC);
  %\draw[->,>=latex,color=bobcolor] (XFS) -- node[above,sloped]{std i/o} node[below,sloped]{2a. auth} (VRS);
  %\draw[<->,>=latex,color=bobcolor] (VRS) to[bend right=10] node[sloped, above]{2a. auth} (AUTHS);
  %\draw[->,>=latex,color=bobcolor,dashed] (VRS) to[bend left=10] node[right]{2b. fork} (UFS);
  % \draw[->,>=latex,color=bobcolor] (VRS) to[bend left=90] node[below,sloped,pos=.7]{5a. auth} (10.2,-1.7);
  %\draw[<-,>=latex,color=alicecolor,dashed] (UFS) to[bend left=10] node[below]{2c.}(VRS);
  \draw[<-,>=latex,color=alicecolor,line width=3pt] (SRV.-50) to[bend right=10] node[pos=.45,above,sloped]{bind} (AS.170);
  \draw[->,>=latex,color=alicecolor] (UFS) to[bend left=30] node[sloped,below]{r/w access} (9.6,1.4);
  \draw[->,>=latex,color=alicecolor,line width=3pt] (MNT) edge[bend right=30] node[midway,below,sloped]{mount} (bla) edge[<-,bend left=30]  node[midway,below,sloped]{mount} (-.8,-3.2);
  \draw[->,>=latex,color=alicecolor!50, line width=5pt, loosely dashed] (SRV) to[bend right=20] node[sloped,below]{\Large connect} (UFS);
  \draw[->,>=latex,color=alicecolor!50,thick,line width=5pt, loosely dashed] (-.5,-3.4) to[bend right=30] node[sloped,below]{\Large sync} (8.4,1.2);
  \draw[->,>=latex,color=alicecolor] (foo) to[bend right=40] node[pos=.4,above, sloped]{r/w access}  (-3,-3.1);

\end{tikzpicture}
