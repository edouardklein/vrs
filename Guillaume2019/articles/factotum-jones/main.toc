\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Authenticating on the network}{3}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{3}
\contentsline {subsection}{\numberline {2.1}Client configuration}{3}
\contentsline {subsubsection}{\numberline {2.1.1}Prerequisites}{3}
\contentsline {paragraph}{}{3}
\contentsline {subsubsection}{\numberline {2.1.2}Running the client}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{Connecting to a 9P server}{4}
\contentsline {paragraph}{Authenticating with factotum}{4}
\contentsline {paragraph}{unmounting the ressource}{5}
\contentsline {subsection}{\numberline {2.2}Server implementation}{5}
\contentsline {subsubsection}{\numberline {2.2.1}Abstract}{5}
\contentsline {paragraph}{}{5}
\contentsline {subsubsection}{\numberline {2.2.2}Prerequisite}{6}
\contentsline {subsubsection}{\numberline {2.2.3}Server implementation}{6}
\contentsline {paragraph}{The vrs multiplexing daemon}{6}
\contentsline {paragraph}{Starting vrs}{6}
\contentsline {paragraph}{Binding vrs to a network socket}{6}
\contentsline {subsubsection}{\numberline {2.2.4}Completing the authentication with factotum}{7}
\contentsline {subsection}{\numberline {2.3}Secstore server configuration (optional)}{7}
\contentsline {paragraph}{}{8}
\contentsline {section}{\numberline {3}Changing the user id}{8}
\contentsline {section}{\numberline {4}Simplifying the authentication}{8}
\contentsline {subsection}{\numberline {4.1}The use of the PAM library}{8}
\contentsline {subsubsection}{\numberline {4.1.1}Delegating the authentication to PAM functions}{8}
\contentsline {subsection}{\numberline {4.2}Using Guillou-Quisquater zero-knowledge authentication protocol}{8}
\contentsline {section}{\numberline {5}Summary}{9}
\contentsline {section}{\numberline {6}Conclusion}{10}
\contentsline {section}{\numberline {A}Network topology and scripts execution}{11}
\contentsline {section}{\numberline {B}The Guillou-Quisquater (GQ) zero-knowledge proof}{12}
\contentsline {section}{\numberline {C}Client scripts}{13}
\contentsline {subsection}{\numberline {C.1}Mounting/Unmounting the remote shared file tree}{13}
\contentsline {subsection}{\numberline {C.2}Starting and stopping factotum agent}{14}
\contentsline {section}{\numberline {D}Server scripts}{14}
