#ifndef FINTERFACE_H
#define FINTERFACE_H


typedef unsigned char uchar;
typedef unsigned int uint;

enum {
      RPC_OK = 0,
      RPC_OK_DATA,
      RPC_DONE,
      RPC_DONE_HAVEAI,
      RPC_PHASE,
      RPC_ERROR,
      RPC_PROTOCOL,
      RPC_NEEDKEY,
      RPC_TOOSMALL
};

struct rpc_message {
  int length;
  char msg[4096];
};

struct AuthInfo {
  char* cuid;
  char* suid;
  char* cap;
  int nsecret;
  uchar* secret;
};

int dorpc_fs(char *errstring, int fd, char *verb, char *val, int len, struct rpc_message *msg);
static uchar* gcarray(uchar *p, uchar *ep, uchar **s, int *np);
uchar* gstring(uchar *p, uchar *ep, char **s);
void auth_freeAI(struct AuthInfo *ai);
uchar* convM2AI(uchar *p, int n, struct AuthInfo **aip);

#endif
