/*
9p2h.c

provides a wrapper to any program which serves 9p on its standard input/output
allows human to prompt 9p messages

author: Guillaume Gette (C3N)
*/

#include <u.h>
#include <libc.h>
#include <fcall.h>
#include <thread.h>

void print_stat(char *buf, uchar *stat, uint nstat);
void get_stat(char **context, Fcall *f); 
int __string_to_qid(char **context, Qid *q);
vlong string_to_vlong(char *str);
void display_qid(char *buf, Qid *qid);
int print2h(char *msg, uint n);
int send_msg(int ifd, char *msg, uint n);
void f_h29p(void *ifd);
void f_9p2h(void *ofd);
void usage(void);


void
print_stat(char *buf, uchar *stat, uint nstat)
{
  Dir *d=malloc(sizeof(Dir)+nstat);
  
  if (convM2D(stat, nstat, d, (char*)&d[1])!=nstat)
    sysfatal("cannot read stat\n");
  sprint(buf+strlen(buf), "nstat:%ud type:%ud dev:%ud qid=(type:%ud vers:%ud path:%ud) mode:%ud atime:%ud mtime:%ud length:%ud name:\"%s\" uid:\"%s\" gid:\"%s\" muid:\"%s\"", nstat, d->type, d->dev, d->qid.type, d->qid.vers, d->qid.path, d->mode, d->atime, d->mtime, d->length, d->name, d->uid, d->gid, d->muid);

  free(d);
}

void
get_stat(char **context, Fcall *f)
{
  Dir d;
  char *tmp, *dump;
  uchar *buf = malloc(10000*sizeof(uchar));

  //strtok_r(NULL, "{", context);
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, " ", context);
  d.type = (ushort) strtoul(tmp, &dump, 10);
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, " ", context);
  d.dev = (uint) strtoul(tmp, &dump, 10);
  __string_to_qid(context, &(d.qid));
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, " ", context);
  d.mode = (ulong) strtoul(tmp, &dump, 10);
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, " ", context);
  d.atime = (ulong) strtoul(tmp, &dump, 10);
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, " ", context);
  d.mtime = (ulong) strtoul(tmp, &dump, 10);
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, " ", context);
  d.length = string_to_vlong(tmp);
  strtok_r(NULL, ":", context);
  d.name = strtok_r(NULL, " ", context)+1;
  d.name[strlen(d.name)-1]=0;
  strtok_r(NULL, ":", context);
  d.uid = strtok_r(NULL, " ", context)+1;
  d.uid[strlen(d.uid)-1]=0;
  strtok_r(NULL, ":", context);
  d.gid = strtok_r(NULL, " ", context)+1;
  d.gid[strlen(d.gid)-1]=0;
  strtok_r(NULL, ":", context);
  d.muid = strtok_r(NULL, "} ", context)+1;
  d.muid[strlen(d.muid)-1]=0;
  if((convD2M(&d, buf, 10000))==0)
    sysfatal("cannot get stat from input\n");
  f->nstat = GBIT16(buf)+2;
  f->stat = malloc((f->nstat)*sizeof(uchar));
  for(int i=0; i<f->nstat; i++){
    f->stat[i] = buf[i];
  }
  free(buf);
} 


int
__string_to_qid(char **context, Qid *q)
{
  char *tmp, *dump;
  
  if((tmp = strtok_r(NULL, "(", context))==NULL)
    return 0;
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, " ", context);
  q->type = (uchar) strtoul(tmp, &dump, 10);
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, " ", context);
  q->vers = strtoul(tmp, &dump, 10);
  strtok_r(NULL, ":", context);
  tmp = strtok_r(NULL, ") ", context);
  q->path = (uvlong) string_to_vlong(tmp);
  return 1;
}


vlong
string_to_vlong(char *str)
{
  vlong l=0;
  int i=0;
  while(*(str+i)>=48 && *(str+i)<58){
    l*=10;
    l+=*(str+i) - 48;
    i++;
  }
  return l;
}

void
display_qid(char *buf, Qid *qid)
{
  sprint(buf+strlen(buf),"type:%ud vers:%ud path:%ud", qid->type, qid->vers, qid->path);
}

int
print2h(char *msg, uint n)
{
  Fcall f;
  char *buf;
 

  buf = malloc(10000*sizeof(uchar));
  
  if(convM2S((uchar*) msg, n, &f)!=n)
    return 1;

  switch(f.type)
    {
    default:
      return 1;

    case Tversion:
      print(" -> Tversion tag:%ud msize:%ud version:\"%s\"\n", f.tag, f.msize, f.version);
      break;

    case Tflush:
      print(" -> Tflush tag:%ud oldtag:%ud\n", f.tag, f.oldtag);
      break;

    case Tauth:
      print(" -> Tauth tag:%ud afid:%ud uname:\"%s\" aname:\"%s\"\n", f.tag, f.afid, f.uname, f.aname);
      break;

    case Tattach:
      print(" -> Tattach tag:%ud fid:%ud afid:%ud uname:\"%s\" aname:\"%s\"\n", f.tag, f.fid, f.afid, f.uname, f.aname);
      break;

    case Twalk:
      sprint(buf, " -> Twalk tag:%ud fid:%ud newfid:%ud nwname:%ud", f.tag, f.fid, f.newfid, f.nwname);
      for(int i=0; i<f.nwname; i++){
	sprint(buf+strlen(buf)," wname%d:\"%s\"", i+1, f.wname[i]);
      }
      print("%s\n",buf);
      break;

    case Topen:
    case Topenfd:
      sprint(buf, " -> Topen");
      if (f.type==Topenfd) sprint(buf,"fd");
      sprint(buf+strlen(buf)," tag:%ud fid:%ud f.mode:%ud", f.tag, f.fid, f.mode);
      print("%s\n", buf);
      break;

    case Tcreate:
      print(" -> Tcreate tag:%ud fid:%ud name:\"%s\" perm:%ud mode:%ud\n", f.tag, f.fid, f.name, f.perm, f.mode);
      break;

    case Tread:
      print(" -> Tread tag:%ud fid:%ud offset:%d count:%ud\n", f.tag, f.fid, f.offset, f.count);
      break;

    case Twrite:
      print(" -> Twrite tag:%ud fid:%ud offset:%ud count:%d data:\"%s\"\n", f.tag, f.fid, f.offset, f.count, f.data);
      break;

    case Tclunk:
    case Tremove:
    case Tstat:
      if (f.type==Tclunk) {sprint(buf, " -> Tclunk");} else if (f.type==Tremove) {sprint(buf," -> Tremove");} else {sprint(buf," -> Tstat");}
      sprint(buf+strlen(buf)," tag:%ud fid:%ud", f.tag, f.fid);
      print("%s\n",buf);
      break;
      
    case Twstat:
      sprint(buf, " -> Twstat tag:%ud stat:(", f.tag);
      print_stat(buf+strlen(buf), f.stat, f.nstat);
      print("%s)\n", buf);
      break;

      /*
       */
    case Rversion:
      print(" <- Rversion tag:%ud msize:%ud version:\"%s\"\n", f.tag, f.msize, f.version);
      break;

    case Rerror:
      print(" <- Rerror tag:%ud ename:\"%s\"\n", f.tag, f.ename);
      break;

    case Rflush:
      print(" <- Rflush tag:%ud\n", f.tag);
      break;

    case Rauth:
      sprint(buf, " <- Rauth tag:%ud qid=(", f.tag);
      display_qid(buf, &(f.aqid));
      print("%s)\n", buf);
      break;

    case Rattach:
      sprint(buf, " <- Rattach tag:%ud qid=(", f.tag);
      display_qid(buf, &(f.qid));
      print("%s)\n",buf);
      break;

    case Rwalk:
      sprint(buf, " <- Rwalk tag:%ud nwqid:%ud", f.tag, f.nwqid);
      for(int i=0; i<f.nwqid; i++){
	sprint(buf+strlen(buf), " qid%d=(", i+1);
	display_qid(buf, &(f.wqid[i]));
	sprint(buf+strlen(buf), ")");
      }
      print("%s\n",buf);
      break;

    case Ropen:
    case Ropenfd:
      sprint(buf," <- Ropen");
      if(f.type==Ropenfd) sprint(buf+strlen(buf),"fd");
    case Rcreate:
      if (f.type==Rcreate) sprint(buf," <- Rcreate");
      sprint(buf+strlen(buf)," tag:%ud qid=(", f.tag);
      display_qid(buf, &(f.qid));
      sprint(buf+strlen(buf),") iounit:%ud", f.iounit);
      if (f.type==Ropenfd) sprint(buf+strlen(buf)," unixfd:%d", f.unixfd);
      print("%s\n",buf);
      break;
        
    case Rread:
      if(f.count==0){print(" <- Rread tag:%ud count:%ud\n", f.tag, f.count);}
      else if(f.count == strlen(f.data)) {print(" <- Rread tag:%ud count:%ud data:\"%s\"\n", f.tag, f.count, f.data);}
      else {
	sprint(buf, " <- Rread tag:%ud count:%ud", f.tag, f.count);
	uchar *p = (uchar *) f.data;
	int i=1;
	while((p-(uchar *)f.data)<f.count){
	  ushort n = GBIT16(p)+2;
	  sprint(buf+strlen(buf), " stat%d:{", i);
	  print_stat(buf, p, n);
	  sprint(buf+strlen(buf),"}");
	  p+=n;
	  i++;
	}
	print("%s\n", buf);
      }
      break;

    case Rwrite:
      print(" <- Rwrite tag:%ud count:%ud\n", f.tag, f.count);
      break;

    case Rclunk:
      print(" <- Rclunk tag:%ud\n", f.tag);
      break;
      
    case Rremove:
      print(" <- Rremove tag:%ud\n", f.tag);
      break;

    case Rstat:
      sprint(buf, " <- Rstat tag:%ud stat:{", f.tag);
      print_stat(buf, f.stat, f.nstat);
      print("%s}\n", buf);
      break;

    case Rwstat:
      print(" <- Rwstat tag:%ud\n", f.tag);
      break;
    }
  free(buf);  
  return 0;
}

int
send_msg(int ifd, char *msg, uint n)
{
  Fcall f;
  char *type, *tmp, *dump, *context;
  char buf[10000];
  
  strtok_r(msg, " ", &context);
  type = strtok_r(NULL, " ", &context);
  strtok_r(NULL, ":", &context);
  tmp = strtok_r(NULL, " ", &context);
  f.tag = (ushort) strtoul(tmp, &dump, 10);
  f.nstat = 0;
  f.count = 0;
  buf[0]=0;
  
  if(strcmp(type, "Tversion")==0){
    f.type = Tversion;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.msize = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    f.version = strtok_r(NULL, " ", &context)+1;
    f.version[strlen(f.version)-1]=0;
  }

  if(strcmp(type, "Tflush")==0){
    f.type = Tflush;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.oldtag = (ushort) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Tauth")==0){
    f.type = Tauth;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.afid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    f.uname = strtok_r(NULL, " ", &context)+1;
    f.uname[strlen(f.uname)-1]=0;
    strtok_r(NULL, ":", &context);
    f.aname = strtok_r(NULL, " ", &context)+1;
    f.aname[strlen(f.aname)-1]=0;
  }
  
  if(strcmp(type, "Tattach")==0){
    f.type = Tattach;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.afid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    f.uname = strtok_r(NULL, " ", &context)+1;
    f.uname[strlen(f.uname)-1]=0;
    strtok_r(NULL, ":", &context);
    f.aname = strtok_r(NULL, " ", &context)+1;
    f.aname[strlen(f.aname)-1]=0;
  }

  if(strcmp(type, "Twalk") == 0){
    f.type = Twalk;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.newfid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, " ", &context);
    strtok_r(NULL, ":", &context);
    for(f.nwname = 0; (f.wname[f.nwname]=strtok_r(NULL, " ", &context)) != NULL; f.nwname++){
      (f.wname[f.nwname])++;
      (f.wname[f.nwname])[strlen(f.wname[f.nwname])-1]=0;
      strtok_r(NULL, ":", &context);
    }
  }
  
  if(strcmp(type, "Topen") == 0){
    f.type = Topen;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.mode = (uchar) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Topenfd") == 0){
    f.type = Topenfd;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.mode = (uchar) strtoul(tmp, &dump, 10);
  } 

  if(strcmp(type, "Tcreate") == 0){
    f.type = Tcreate;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    f.name = strtok_r(NULL, " ", &context)+1;
    f.name[strlen(f.name)-1]=0;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.perm = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.mode = (uchar) strtoul(tmp, &dump, 10);
  }

  if (strcmp(type, "Tread") == 0){
    f.type = Tread;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.offset = string_to_vlong(tmp);
    strtok_r(NULL, ":", &context);    
    tmp = strtok_r(NULL, " ", &context);
    f.count = (u32int) strtoul(tmp, &dump, 10);
  }
  
  if(strcmp(type, "Twrite") == 0){
    f.type = Twrite;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.offset = string_to_vlong(tmp);
    strtok_r(NULL, ":", &context);
    tmp=strtok_r(NULL, " ", &context);
    strtok_r(NULL, ":", &context);
    while((tmp=strtok_r(NULL, " ",&context))!=NULL){
      strcat(buf, " ");
      strcat(buf, tmp);
    }
    int l=strlen(buf);
    if(buf[l-1]==' ') {buf[l-2]=0;} else {buf[l-1]=0;}
    f.data = malloc((1+strlen(buf+2))*sizeof(char));
    strcpy(f.data, buf+2);
    f.count = strlen(f.data);
  }

  if(strcmp(type, "Tclunk") == 0){
    f.type = Tclunk;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Tremove") == 0){
    f.type = Tremove;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Tstat") == 0){
    f.type = Tstat;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Twstat") == 0){
    f.type = Twstat;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.fid = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, " ", &context);
    get_stat(&context, &f);
  }
      /*
       */

  if (strcmp(type, "Rversion") == 0){
    f.type = Rversion;
    strtok_r(NULL, ":", &context);
    f.version = strtok_r(NULL, " ", &context)+1;
    f.version[strlen(f.version)-1]=0;
    f.msize = strlen(f.version);
  }

  if(strcmp(type, "Rerror") == 0){
    f.type = Rerror;
    strtok_r(NULL, ":", &context);
    f.ename = strtok_r(NULL, " ", &context);
  }
  
  if(strcmp(type, "Rflush") == 0){
    f.type = Rflush;
  }

  if(strcmp(type, "Rauth") == 0){
    f.type = Rauth;
    __string_to_qid(&context, &(f.aqid));
  }

  if(strcmp(type, "Rattach") == 0){
    f.type = Rattach;
    __string_to_qid(&context, &(f.qid));
  }

  if(strcmp(type, "Rwalk") == 0){
    f.type = Rwalk;
    strtok_r(NULL, " ", &context);
    for(f.nwqid=0; __string_to_qid(&context, &(f.wqid[f.nwqid])); f.nwqid++);
  }

  if(strcmp(type, "Ropen") == 0){
    f.type = Ropen;
    __string_to_qid(&context, &(f.qid));
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.iounit = (u32int) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Ropenfd") == 0){
    f.type = Ropenfd;
    __string_to_qid(&context, &(f.qid));
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.iounit = (u32int) strtoul(tmp, &dump, 10);
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.unixfd = (int) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Rcreate") == 0){
    f.type = Rcreate;
    __string_to_qid(&context, &(f.qid));
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.iounit = (u32int) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Rread") == 0){
    f.type = Rread;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.count = (u32int) strtoul(tmp, &dump, 10);
    tmp = strtok_r(NULL, " ", &context);
    if(tmp==NULL){
      f.count = 0;
      f.data = malloc(sizeof(char));
      f.data[0]=0;
    }
    else if(tmp[0]=='d'){
      tmp+=5;
      while(tmp){ 
	strcat(buf, " ");
	strcat(buf, tmp);
	tmp = strtok_r(NULL, " ", &context);
      }
      int l=strlen(buf);
      if(buf[l-1]==' ') {buf[l-2]=0;} else {buf[l-1]=0;}
      f.data = malloc((1+strlen(buf+2))*sizeof(char));
      strcpy(f.data, buf+2);
      f.count = (u32int) strlen(f.data);
    } else {
      int n=0;
      f.data = malloc(f.count*sizeof(char));
      while(n<f.count){
	get_stat(&context, &f);
	strtok_r(NULL, " ", &context);
	for(int i=0;i<f.nstat;i++){
	  f.data[n+i]=(char)f.stat[i];
	}
	n+=f.nstat;
	f.nstat=0;
	free(f.stat);
      }
    }
  }

  if(strcmp(type, "Rwrite") == 0){
    f.type = Rwrite;
    strtok_r(NULL, ":", &context);
    tmp = strtok_r(NULL, " ", &context);
    f.count = (u32int) strtoul(tmp, &dump, 10);
  }

  if(strcmp(type, "Rclunk") == 0){
    f.type = Rclunk;
  }

  if(strcmp(type, "Rremove") == 0){
    f.type = Rremove;
  }

  if(strcmp(type, "Rstat") == 0){
    f.type = Rstat;
    strtok_r(NULL, " ", &context);
    get_stat(&context, &f);
  }

  if(strcmp(type, "Rwstat") == 0){
    f.type = Rwstat;
  }

  n = convS2M(&f, (uchar*) buf, 10000);
  if(n <= BIT16SZ){
    fprint(2, "Error sending the 9P message\n");
    return 1;
  } else {
    write(ifd, buf, n);
  }

  if(f.count && strcmp(type, "Tread"))
    free(f.data);

  if(f.nstat)
    free(f.stat);

  return 0;
}

void
f_h29p(void *ifd)
{
  char *buf;
  long l;

  buf=malloc(10000);
  
  while(1){
    if((l=read(0, buf, 9000))<0)
      sysfatal("read error: %r");
    buf[l-1]=0;
    if (*((int*)ifd+2))
      fprint(2, "%s\n", buf);
    if (send_msg(*((int*)ifd), buf, l)==1)
      sysfatal("sending error");
        
  }
}

void
f_9p2h(void *ofd)
{
  char *buf;
  long l;
  
  buf=malloc(10000);
  while(1){
    if((l=read(*((int*)ofd), buf, 9000))<0)
      sysfatal("read error: %r");
    buf[l]=0;
    if (print2h(buf, l)==1)
      sysfatal("printing error");
  }
}

void
usage()
{
  fprint(2, "usage: 9p2h cmd [cmd args]\n");
  threadexitsall("usage");
}

#undef _exits
void
threadmain(int argc, char** argv)
{
  int to_in[2];
  int to_inarg[3];
  int to_out[2];
  int logged = 1;
  
  if(strcmp(argv[1], "-l"))
    logged = 0;

  if(argc==1 || (logged && argc==2))
    usage();
  
  
  pipe(to_in);
  pipe(to_out);

  to_inarg[0]=to_in[0];
  to_inarg[1]=to_in[1];
  to_inarg[2]=logged;
    
  switch(rfork(RFFDG|RFPROC)){
  case -1:
    fprint(2, "unable to fork, exit");
    threadexitsall(0);
    break;
  case 0:
    dup(to_in[1], 0);
    dup(to_out[1], 1);
    for(int i=3;i<20;i++)
      close(i);

    if(logged){
      fprint(2, "exec %s\n", argv[2]);
      execvp(argv[2], argv+2);}
    else {
	fprint(2, "exec %s\n", argv[1]);
      execvp(argv[1], argv+1);
    }
    
    _exits("exec");
    break;
  default:
    proccreate(f_h29p, (void*) to_inarg, 1000);
    f_9p2h((void*) to_out);
    break;
  }
  threadexitsall(0); 
}
  
  
  
