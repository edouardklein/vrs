/*
9plog.c

A wrapper intended to log all 9p conversations
clones all 9p messages onto unix socket `namespace`/socketname (default `namespace`/9plog)
warning: this wrapper parses all messages and regenerate them, using two 9p2h processes!

author: Guillaume Gette (C3N)
*/

#include <u.h>
#include <libc.h>
#include <thread.h>

void
usage(void)
{
  fprint(2, "usage: 9plog [-U socketname] cmd [cmd args]\n");
  threadexitsall("usage");
}

#undef _exits
void
threadmain(int argc, char** argv)
{
  int from_server[2];
  int to_server[2];
  char *ns, *s, *us="9plog";
  int offset = 0;
  char **argvson;
  
  if(argc==1 || (!strcmp(argv[1], "-U") && argc <4))
    usage();

  if(!strcmp(argv[1], "-U")){
    us = argv[2];
    offset = 2;
  }
  
  pipe(to_server);
  pipe(from_server);
  
  switch(rfork(RFFDG|RFPROC)){
  case -1:
    fprint(2, "unable to fork (client), exit");
    threadexitsall(0);
    break;
  case 0:
    dup(from_server[0], 0);
    dup(to_server[1], 1);
    for(int i=3;i<20;i++)
      close(i);

    argvson = malloc((argc-offset+2)*sizeof(char*));
    argvson[0]="./9p2h";
    argvson[1]="-l";
    for(int i=0;i<argc-offset;i++)
      argvson[i+2]=argv[i+1+offset];

    fprint(2, "exec %s\n", argv[offset+1]);
    execvp(argvson[0], argvson);

    _exits("exec");
    break;
  default:
    switch(rfork(RFFDG|RFPROC)){
    case -1:
      fprint(2, "unable to fork (server), exit");
      threadexitsall(0);
      break;
    case 0:
      dup(from_server[1], 1);
      dup(to_server[0], 0);

      for(int i=3;i<20;i++)
	close(i);

      if((ns = getns()) == nil)
	sysfatal("cannot get namespace");
    
      s = smprint("unix!%s/%s", ns, us);
      fprint(2, "exec 9pserve\n");
      
      execlp("./9p2h", "./9p2h", "-l", "9pserve", "-M", "8192", s, (char*)0);

      _exits("exec");
      break;
    default:
      while(1);
      break;
    
    }
    
  }
  threadexitsall(0); 
}
