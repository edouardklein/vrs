/*
 * gq - an implementation of the guillou-quisquater protocol
 * 
 *
 * parameters : n = pq from RSA protocol ; public exponent e ; public certificate I ; (client only) private certificate S
 *
 * Client protocol:
 *	choose random r in Z/Zn
 *      write x = r^v mod n
 *	read e (1<=e<=v)
 *	write y = r*S^e mod n
 *	read authenticator[authentlen]
 *
 * Server protocol:
 * 	read x
 *	write random 1<=e<=v
 *	read y
 *      compute I^e * y^v and compares it to x
 *	write authenticator[authentlen]
 */

#include "std.h"
#include "dat.h"

extern Proto gq;

static int
gqgenerate(Conv *c)
{
  Key *k;
  RSApriv *rsa;
  mpint *cert, *priv;
  int nlen, lbuf;
  char *buf, *user, *dom;
  int ret;
    
  ret = -1;
  user = nil;
  dom = nil;

  /*find rsa key*/
  user = strfindattr(c->attr, "user");
  dom = strfindattr(c->attr, "dom");
  if (user==nil || dom==nil){
    werrstr("incomplete information, cannot generate private certificate");
    return -1;
  }
  c->state = "read rsa key";
  k = keylookup("proto=rsa role=gqac dom=%s size=%s", dom, strfindattr(c->attr, "size"));
  if(k == nil || !k->privattr){
    werrstr("missing valid rsa key, cannot generate private certificate");
    goto outgen;
  }

  c->state = "gen cert";
  rsa = (RSApriv *)k->priv;
  nlen = mpsignif(rsa->pub.n);
  cert = mpnew(nlen);
  priv = mpnew(nlen);
  gencert(user, strlen(user), dom, strlen(dom), cert, rsa->pub.n, priv, nlen);
  rsadecrypt(rsa, priv, priv);
  
  lbuf = (nlen+3)/4;
  buf = calloc(1+lbuf, sizeof(char));

  mptoa(priv, 16, buf, lbuf);
  c->attr = addattr(c->attr, "cap=%sx%sx%s", strfindattr(k->attr, "n"), strfindattr(k->attr, "ek"), buf);
  
  free(buf);
  free(cert);
  free(priv);
  ret = 0;
  
outgen:
  if(ret < 0)
    flog("gqgen: %r");
  keyclose(k);
  return ret;
}

static int
gqprove(Conv *c)
{
  Key *k;
  GQpriv *key;
  RSApub *rsa;
  mpint *r, *x, *y, *chal;
  int nlen, elen, lbuf, lbufe;
  char *buf;
  int ret;
  int loop;
  char norsa;

  norsa = 0;   
  ret = -1;

  /*find gq key*/
  c->state = "find key";
  k = keylookup("%A", c->attr);
  if(k == nil)
    goto outprove;
  key = k->priv;

  /*find rsa key*/
  rsa=&(key->pub.rsa);
  if(rsa->n == nil || rsa->ek == nil){
    norsa = 1;
    freeattr(k->attr);
    freeattr(k->privattr);
    k = keylookup("proto=rsa role=gq dom=%s", strfindattr(k->attr, "dom"));
    if(k == nil){
      werrstr("missing public rsa key, cannot retrieve public certificate");
      goto outprove;
    }
    rsa = &(((RSApriv *)k->priv)->pub);
    nlen = mpsignif(rsa->n);
    key->pub.cert = mpnew(nlen);
    gencert(key->pub.user, strlen(key->pub.user), key->pub.dom, strlen(key->pub.dom), key->pub.cert, rsa->n, nil, nlen);
  }
  
  /* make sure we have the private certificate */
  if(!key->privcert){
    werrstr("missing private certificate -- cannot authenticate");
    goto outprove;
  } 

  nlen = mpsignif(rsa->n);
  elen = mpsignif(rsa->ek);
  lbuf = (nlen+3)/4;
  lbufe = (elen+3)/4;
  buf = calloc(1+lbuf, sizeof(char));
  r = mpnew(nlen);
  x = mpnew(nlen);
  y = mpnew(nlen);
  chal = mpnew(elen);

  
  do {
    /*committing*/
    c->state = "gen commitment";
    geninvertible(nlen, r, rsa->n, nil);
    rsaencrypt(rsa, r, x);

    c->state = "send commitment";
    mptoa(x, 16, buf, lbuf);
    convwrite(c, buf, 1+lbuf);
    
    /*challenge*/
    c->state = "recv challenge";
    if(convread(c, buf, 1+lbufe))
      goto loopoutprove;
    strtomp(buf, nil, 16, chal);

    /*response*/
    mpexp(key->privcert, chal, rsa->n, y);
    mpmul(y, r, y);
    mpmod(y, rsa->n, y);
    c->state = "send response";
    mptoa(y, 16, buf, lbuf);
    convwrite(c, buf, 1+lbuf);

    /*get confirmation*/
    c->state = "recv confirmation";
    if(convread(c, buf, 8))
      goto loopoutprove;
    if(strncmp((const char *) buf, "SUCCESS", 7) == 0)
       loop = 0;
    if(strncmp((const char *) buf, "FAILURE", 7) == 0){
      goto loopoutprove;
    }
    if(strncmp((const char *) buf, "DOAGAIN", 7) == 0)
      loop = 1;

  } while(loop);

  ret = 0;

loopoutprove:  
  mpfree(x);
  mpfree(chal);
  mpfree(y);
  mpfree(r);
  free(buf);
  
outprove:
  if(norsa)
    gqprivfree(key);
  if(ret < 0)
    flog("gqprover: %r");
  keyclose(k);
  return ret;
}

static int
gqverify(Conv *c)
{
  Key *k;
  GQpub *key;
  RSApub *rsa;
  mpint *x, *y, *chal, *x2;
  int nlen, lbuf, lbufe, elen, nloop, loop;
  char *buf;
  int ret;
  char norsa;

  loop = 0;
  nloop = 5;
  ret=-1;
  norsa = 0;
  
  /*find gq key*/
  c->state = "find key";
  k = keylookup("%A", c->attr);
  if(k == nil)
    goto outverify;
  key = &(((GQpriv*)(k->priv))->pub);
  rsa = &(key->rsa);
  if(rsa->n == nil || rsa->ek == nil){
    norsa = 1;
    freeattr(k->attr);
    freeattr(k->privattr);
    /*find rsa key*/
    k = keylookup("proto=rsa role=gq dom=%s", strfindattr(k->attr, "dom"));
    if(k == nil){
      werrstr("missing public rsa key, cannot retrieve public certificate");
      goto outverify;
    }
    rsa = &(((RSApriv *)k->priv)->pub);
    nlen = mpsignif(rsa->n);
    key->cert = mpnew(nlen);
    gencert(key->user, strlen(key->user), key->dom, strlen(key->dom), key->cert, rsa->n, nil, nlen);
  }
    
  nlen = mpsignif(rsa->n);
  elen = mpsignif(rsa->ek);
  lbuf = (nlen+3)/4;
  lbufe = (elen+3)/4;
  buf = calloc(1+lbuf, sizeof(char));

  x = mpnew(nlen);
  chal = mpnew(elen);
  y = mpnew(nlen);
  x2 = mpnew(nlen);

  do {
    /*committing*/
    c->state = "recv commitment";
    if(convread(c, buf, 1+lbuf)<0)
      goto loopoutverify;
    strtomp(buf, nil, 16, x);
    
    /*challenge*/
    c->state = "send challenge";
    do mprand(elen, genrandom, chal);
    while (mpcmp(chal, mpzero)<=0||mpcmp(chal, rsa->ek)>=0);
    mptoa(chal, 16, buf, lbufe);
    buf[lbufe]=0;
    convwrite(c, buf, 1+lbufe);

    /*response*/
    c->state = "recv response";
    if(convread(c, buf, 1+lbuf)<0)
      goto loopoutverify;

    /*checking*/
    strtomp(buf, nil, 16, y);
    rsaencrypt(rsa, y, y);
    mpexp(key->cert, chal, rsa->n, x2);
    mpmul(x2, y, x2);
    mpmod(x2, rsa->n, x2);
    
  
    if(mpcmp(x2, x)){
      werrstr("bad response to challenge");
      convwrite(c, "FAILURE", 8);
      goto loopoutverify;
    }

    loop++;
    if(loop == nloop)
      convwrite(c, "SUCCESS", 8);
    else
      convwrite(c, "DOAGAIN", 8);
    
  } while(loop<nloop);

  ret = 0;
  
loopoutverify:
  mpfree(x);
  mpfree(chal);
  mpfree(y);
  mpfree(x2);
  free(buf);
  
outverify:
  if(norsa)
    gqpubfree(key);
  if(ret < 0)
    flog("gqverifier: %r");
  keyclose(k);
  return ret;
}

/*
 * convert to canonical form (lower case) 
 * for use in attribute matches.
 */
static void
strlwr(char *a)
{
  for(; *a; a++){
    if('A' <= *a && *a <= 'Z')
      *a += 'a' - 'A';
  }
}

static GQpriv*
readgqpriv(Key *k)
{
  char *a, *user, *dom;
  GQpriv *priv;
  int nlen, ulen, dlen;
  char hasrsa;

  hasrsa = 1;
  priv = gqprivalloc();
  
  user = strfindattr(k->attr, "user");
  dom = strfindattr(k->attr, "dom");
  if(user==nil || dom==nil){
    werrstr("need user and dom attributes");
    goto Error;
  }

  ulen = strlen(user);
  dlen = strlen(dom);
  if(ulen >= 50){
    werrstr("user name too long");
    goto Error;
  }
  if(dlen >= 50){
    werrstr("auth dom name too long");
    goto Error;
  }

  priv->pub.user = calloc(ulen+1, sizeof(char));
  priv->pub.dom = calloc(dlen+1, sizeof(char));

  strcpy(priv->pub.user, user);
  strcpy(priv->pub.dom, dom);
  
  if((a=strfindattr(k->attr, "ek"))==nil
     || (priv->pub.rsa.ek=strtomp(a, nil, 16, nil))==nil)
    hasrsa=0;
  else
    strlwr(a);
  
  if((a=strfindattr(k->attr, "n"))==nil
     || (priv->pub.rsa.n=strtomp(a, nil, 16, nil))==nil)
    hasrsa=0;
  else
    strlwr(a);
  
  if(hasrsa){
    nlen = mpsignif(priv->pub.rsa.n);
    priv->pub.cert = mpnew(nlen);
    gencert(user, ulen, dom, dlen, priv->pub.cert, priv->pub.rsa.n, nil, nlen);
  }
  
  if(k->privattr == nil)	/* only public half */
    return priv;

  if((a=strfindattr(k->privattr, "!priv"))==nil 
     || (priv->privcert=strtomp(a, nil, 16, nil))==nil)
    goto Error;
  strlwr(a);
  return priv;

 Error:
  gqprivfree(priv);
  return nil;
}

static int
gqcheck(Key *k)
{
  static int first = 1;

  if(first){
    fmtinstall('B', mpfmt);
    first = 0;
  }
  
  if((k->priv = readgqpriv(k)) == nil){
    werrstr("malformed key data");
    return -1;
  }
  return 0;
}

static void
gqclose(Key *k)
{
  gqprivfree(k->priv);
  k->priv = nil;
}

static Role
gqroles[] = 
{
	"prover",	gqprove,
  	"verifier",	gqverify,	/* public operation */
	"client",	gqprove,
	"server",	gqverify,	/* public operation */
	"generate",     gqgenerate,
	0
};

Proto gq = {
	"gq",
	gqroles,
	"role? user? dom?",
	gqcheck,
	gqclose
};
