/*
 * gq - an implementation of the guillou-quisquater protocol
 * 
 *
 * parameters : n = pq from RSA protocol ; public exponent e ; public certificate I ; (client only) private certificate S
 *
 * Client protocol:
 *	choose random r in Z/Zn
 *      write x = r^v mod n
 *	read e (1<=e<=v)
 *	write y = r*S^e mod n
 *	read authenticator[authentlen]
 *
 * Server protocol:
 * 	read x
 *	write random 1<=e<=v
 *	read y
 *      compute I^e * y^v and compares it to x
 *	write authenticator[authentlen]
 */

#include "std.h"
#include "dat.h"
#include "gq.h"

extern Proto gq;

static int
gqprove(Conv *c)
{
  Key *k;
  GQpriv *key;
  mpint *r, *x, *y, *chal;
  int nlen, elen, lbuf, lbufe;
  uchar *buf;
  int ret;
  int loop;
    
  ret = -1;

  /*find key*/
  c->state = "find key";
  k = keylookup("%A", c->attr);
  if(k == nil)
    goto out;
  key = k->priv;

  /* make sure we have the private certificate */
  if(!key->privcert){
    werrstr("missing private certificate -- cannot authenticate");
    goto out;
  } 

  nlen = mpsignif(key->pub.pubrsa.n);
  elen = mpsignif(key->pub.pubrsa.ek);
  lbuf = (nlen+7)/8;
  lbufe = (elen+7)/8;
  buf = calloc(lbuf, sizeof(uchar));
  r = mpnew(nlen);
  x = mpnew(nlen);
  y = mpnew(nlen);
  chal = mpnew(elen);

  
  //do {
    /*committing*/
    c->state = "gen commitment";
  
    geninvertible(nlen, r, key->pub.pubrsa.n, nil);

    rsaencrypt(&key->pub.pubrsa, r, x);

    c->state = "send commitment";
    mptobe(x, buf, lbuf, nil);
    convwrite(c, buf, lbuf);
    
    /*challenge*/
    c->state = "recv challenge";
    convread(c, buf, lbufe);
    betomp(buf, lbufe, chal);

    /*response*/
    mpexp(key->privcert, chal, key->pub.pubrsa.n, y);
    mpmul(y, r, y);
    mpmod(y, key->pub.pubrsa.n, y);
    c->state = "send response";
    mptobe(y, buf, lbuf, nil);
    convwrite(c, buf, lbuf);

    /*get confirmation*/
    c->state = "recv confirmation";
    convread(c, buf, 8);
    if(strncmp((const char *) buf, "SUCCESS", 7))
       loop = 0;
    if(strncmp((const char *) buf, "FAILURE", 7)){
      mpfree(x); mpfree(chal); mpfree(y); mpfree(r);
      free(buf);
      goto out;
    }
    if(strncmp((const char *) buf, "DOAGAIN", 7))
      loop = 1;

    //} while(loop);
  
  mpfree(x);
  mpfree(chal);
  mpfree(y);
  mpfree(r);
  free(buf);
  ret = 0;
  
out:
  if(ret < 0)
    flog("gqprover: %r");
  keyclose(k);
  return ret;
}

static int
gqverify(Conv *c)
{
  Key *k;
  GQpub *key;
  mpint *x, *y, *chal, *x2;
  int nlen, lbuf, lbufe, elen, nloop, loop;
  uchar *buf;
  int ret;

  loop = 0;
  nloop = 5;
  ret=-1;
  
  /*find key*/
  c->state = "find key";
  k = keylookup("%A", c->attr);
  if(k == nil)
    goto out;
  key = &(((GQpriv*)(k->priv))->pub);
  

  nlen = mpsignif(key->pubrsa.n);
  elen = mpsignif(key->pubrsa.ek);
  lbuf = (nlen+7)/8;
  lbufe = (elen+7)/8;
  buf = calloc(lbuf, sizeof(uchar));

  x = mpnew(nlen);
  chal = mpnew(elen);
  y = mpnew(nlen);
  x2 = mpnew(nlen);

  //do {
    /*committing*/
    c->state = "recv commitment";
    convread(c, buf, lbuf);
    betomp(buf, lbuf, x);
    
    /*challenge*/
    c->state = "send challenge";
    do mprand(elen, genrandom, chal);
    while (mpcmp(chal, mpzero)<=0||mpcmp(chal, key->pubrsa.ek)>=0);
    mptobe(chal, buf, lbufe, nil);
    convwrite(c, buf, lbufe);

    /*response*/
    c->state = "recv response";
    convread(c, buf, lbuf);

    /*checking*/
    y = betomp(buf, lbuf, nil);
    rsaencrypt(&key->pubrsa, y, y);
    x2 = mpnew(nlen);
    mpexp(key->pubcert, chal, key->pubrsa.n, x2);
    mpmul(x2, y, x2);
    mpmod(x2, key->pubrsa.n, x2);
    
  
    if(mpcmp(x2, x)){
      werrstr("bad response to challenge");
      convwrite(c, "FAILURE", 8);
      mpfree(x);
      mpfree(chal);
      mpfree(y);
      mpfree(x2);
      free(buf);
      goto out;
    }

    loop++;
    if(loop == nloop)
      convwrite(c, "SUCCESS", 8);
    else
      convwrite(c, "DOAGAIN", 8);
    
    //} while(loop<nloop);

  mpfree(x);
  mpfree(chal);
  mpfree(y);
  mpfree(x2);
  free(buf);
  
  ret = 0;
  
out:
  if(ret < 0)
    flog("gqverifier: %r");
  keyclose(k);
  return ret;
}

/*
 * convert to canonical form (lower case) 
 * for use in attribute matches.
 */
static void
strlwr(char *a)
{
  for(; *a; a++){
    if('A' <= *a && *a <= 'Z')
      *a += 'a' - 'A';
  }
}

static GQpriv*
readgqpriv(Key *k)
{
  char *a, *user, *dom;
  GQpriv *priv, *tmp;
  int nlen;
  
  priv = gqprivalloc();
  
  user = strfindattr(k->attr, "user");
  dom = strfindattr(k->attr, "dom");
  if(user==nil || dom==nil){
    werrstr("need user and dom attributes");
    goto Error;
  }
	
  if(strlen(user) >= 50){
    werrstr("user name too long");
    goto Error;
  }
  if(strlen(dom) >= 50){
    werrstr("auth dom name too long");
    goto Error;
  }
	
  if((a=strfindattr(k->attr, "ek"))==nil 
     || (priv->pub.pubrsa.ek=strtomp(a, nil, 16, nil))==nil)
    goto Error;
  strlwr(a);
  if((a=strfindattr(k->attr, "n"))==nil 
     || (priv->pub.pubrsa.n=strtomp(a, nil, 16, nil))==nil)
    goto Error;
  strlwr(a);

  nlen = mpsignif(priv->pub.pubrsa.n);
  priv->pub.pubcert = mpnew(nlen);
  tmp = gqprivalloc();
  gqprivfree(tmp);
  gencert(user, strlen(user), dom, strlen(dom), priv->pub.pubcert, priv->pub.pubrsa.n, nil, nlen);
  if(k->privattr == nil)	/* only public half */
    return priv;

  if((a=strfindattr(k->privattr, "!priv"))==nil 
     || (priv->privcert=strtomp(a, nil, 16, nil))==nil)
    goto Error;
  strlwr(a);
  return priv;

 Error:
  gqprivfree(priv);
  return nil;
}

static int
gqcheck(Key *k)
{
  static int first = 1;

  if(first){
    fmtinstall('B', mpfmt);
    first = 0;
  }
  
  if((k->priv = readgqpriv(k)) == nil){
    werrstr("malformed key data");
    return -1;
  }
  return 0;
}

static void
gqclose(Key *k)
{
	gqprivfree(k->priv);
	k->priv = nil;
}

static Role
gqroles[] = 
{
	"prover",	gqprove,
  	"verifier",	gqverify,	/* public operation */
	"client",	gqprove,
	"server",	gqverify,	/* public operation */
	0
};

Proto gq = {
	"gq",
	gqroles,
	nil, //"user? dom? n? ek?",
	gqcheck,
	gqclose
};
