/*
 * Authenticates a user when given a correct password
 * 
 *
 * Server:
 *	start proto=nss ...
 *	read hashed password from client
 *      
 */

#include "std.h"
#include "dat.h"

static int
nsslogin(Conv *c)
{
	Key *k;
	char buf[10000];
	Attr *attr;

	attr = delattr(copyattr(c->attr), "role");
	k = keyfetch(c, "%A", attr);
	if(k == nil)
		return -1;
	c->state = "read";
	bzero(buf, 10000);
	convreadm(c, &buf);
	convprint(c, "%q %q", 
		strfindattr(k->attr, "user"),
		strfindattr(k->privattr, "!password"));
	return 0;
}

static int
nsspasswd(Conv *c)
{
	Key *k;
	char buf[10000];
	Attr *attr;

	attr = delattr(copyattr(c->attr), "role");
	k = keyfetch(c, "%A", attr);
	if(k == nil)
		return -1;
	c->state = "read";
	bzero(buf, 10000);
	convreadm(c, &buf);
	convprint(c, "%q %q", 
		strfindattr(k->attr, "user"),
		strfindattr(k->privattr, "!password"));
	return 0;
}

static int
nssshadow(Conv *c)
{
	Key *k;
	char buf[10000];
	Attr *attr;

	attr = delattr(copyattr(c->attr), "role");
	k = keyfetch(c, "%A", attr);
	if(k == nil)
		return -1;
	c->state = "read";
	bzero(buf, 10000);
	convreadm(c, &buf);
	convprint(c, "%q %q", 
		strfindattr(k->attr, "user"),
		strfindattr(k->privattr, "!password"));
	return 0;
}

static Role nssroles[] = {
	"login",	nsslogin,
	"passwd",       nsspasswd,
	"shadow",       nssshadow,
	0
};

Proto nss =
{
	"nss",
	nssroles,
	"user? !password?",
	nil,
	nil
};
