enum
{
	MaxRpc = 2048,	/* max size of any protocol message */

	/* keep in sync with rpc.c:/rpcname */
	RpcUnknown = 0,		/* Rpc.op */
	RpcAuthinfo,
	RpcAttr,
	RpcRead,
	RpcStart,
	RpcWrite,
	RpcReadHex,
	RpcWriteHex,

	/* thread stack size - big buffers for printing */
	STACK = 65536
};

typedef struct Conv Conv;
typedef struct Key Key;
typedef struct Logbuf Logbuf;
typedef struct Proto Proto;
typedef struct Ring Ring;
typedef struct Role Role;
typedef struct Rpc Rpc;

struct Rpc
{
	int op;
	void *data;
	int count;
	int hex;	/* should result of read be turned into hex? */
};

struct Conv
{
	int ref;			/* ref count */
	int hangup;		/* flag: please hang up */
	int active;			/* flag: there is an active thread */
	int done;			/* flag: conversation finished successfully */
	ulong tag;			/* identifying tag */
	Conv *next;		/* in linked list */
	char *sysuser;		/* system name for user speaking to us */
	char *state;		/* for debugging */
	char statebuf[128];	/* for formatted states */
	char err[ERRMAX];	/* last error */

	Attr *attr;			/* current attributes */
	Proto *proto;		/* protocol */

	Channel *rpcwait;		/* wait here for an rpc */
	Rpc rpc;				/* current rpc. op==RpcUnknown means none */
	char rpcbuf[MaxRpc];	/* buffer for rpc */
	char reply[MaxRpc];		/* buffer for response */
	int nreply;				/* count of response */
	void (*kickreply)(Conv*);	/* call to send response */
	Req *req;				/* 9P call to read response */

	Channel *keywait;	/* wait here for key confirmation */
	
};

struct Proto
{
  char *name;/* name of protocol */
  Role *roles;/* list of roles and service functions */
  char *keyprompt;/* required attributes for key proto=name */
  int (*checkkey)(Key*);/* initialize k->priv or reject key */
  void (*closekey)(Key*);/* free k->priv */
};

struct Role
{
  char *name;/* name of role */
  int (*fn)(Conv*);/* service function */
};

extern int		debug;		/* main.c */
extern char	*passname;	/* main.c */
extern char*     source;     /* main.c */

extern Srv		fs;			/* fs.c */
extern char	*owner;		/* main.c */

void fsinit0(void);

/* provided by lib9p */
#define emalloc	emalloc9p
#define erealloc	erealloc9p
#define estrdup	estrdup9p

/* hidden in libauth */
#define attrfmt		_attrfmt
#define copyattr	_copyattr
#define delattr		_delattr
#define findattr		_findattr
#define freeattr		_freeattr
#define mkattr		_mkattr
#define parseattr	_parseattr
#define strfindattr	_strfindattr

extern Attr*	addattr(Attr*, char*, ...);
/* #pragma varargck argpos addattr 2 */
extern Attr*	addattrs(Attr*, Attr*);
extern Attr*	sortattr(Attr*);
extern int		attrnamefmt(Fmt*);
/* #pragma varargck type "N" Attr* */
extern int		matchattr(Attr*, Attr*, Attr*);
extern Attr*	parseattrfmt(char*, ...);
/* #pragma varargck argpos parseattrfmt 1 */
extern Attr*	parseattrfmtv(char*, va_list);

extern void confirmflush(Req*);
extern void logflush(Req*);

extern int		extrapasswddir;
