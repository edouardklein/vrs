//based on factotum's fs.c

#include "std.h"
#include "dat.h"
#include <stdio.h>

enum
{
	Qroot,
	Qetc,
	Quserlist,
	Qgrouplist,
	
};

static int qtop;

Qid
mkqid(int type, int path)
{
	Qid q;

	q.type = type;
	q.path = path;
	q.vers = 0;
	return q;
}

static struct
{
	char *name;
	int qidpath;
	ulong perm;
} dirtab[] = {
	/* positions of confirm and needkey known below */
	"passwd",    Quserlist,  0444,
	"group",   Qgrouplist, 0444,
	};

static void
fillstat(Dir *dir, char *name, int type, int path, ulong perm)
{
	dir->name = estrdup(name);
	dir->uid = estrdup(owner);
	dir->gid = estrdup(owner);
	dir->mode = perm;
	dir->length = 0;
	dir->qid = mkqid(type, path);
	dir->atime = time(0);
	dir->mtime = time(0);
	dir->muid = estrdup("");
}

static int
rootdirgen(int n, Dir *dir, void *v)
{
	USED(v);

	if(n > 0)
		return -1;
	
	fillstat(dir, passname, QTDIR, Qetc, DMDIR|0555);
	return 0;
}

static int
fsdirgen(int n, Dir *dir, void *v)
{
	USED(v);

	if(n >= nelem(dirtab))
		return -1;
	fillstat(dir, dirtab[n].name, 0, dirtab[n].qidpath, dirtab[n].perm);
	return 0;
}

static char*
fswalk1(Fid *fid, char *name, Qid *qid)
{
	int i;

	switch((int)fid->qid.path){
	default:
		return "fswalk1: cannot happen";
	case Qroot:
		if(strcmp(name, passname) == 0){
			*qid = mkqid(QTDIR, Qetc);
			fid->qid = *qid;
			return nil;
		}
		if(strcmp(name, "..") == 0){
			*qid = fid->qid;
			return nil;
		}
		return "not found";
	case Qetc:
		for(i=0; i<nelem(dirtab); i++)
			if(strcmp(name, dirtab[i].name) == 0){
				*qid = mkqid(0, dirtab[i].qidpath);
				fid->qid = *qid;
				return nil;
			}
		if(strcmp(name, "..") == 0){
			*qid = mkqid(QTDIR, qtop);
			fid->qid = *qid;
			return nil;
		}
		return "not found";
	}
}

static void
fsstat(Req *r)
{
	int i, path;

	path = r->fid->qid.path;
	switch(path){
	case Qroot:
		fillstat(&r->d, "/", QTDIR, Qroot, 0555|DMDIR);
		break;
	case Qetc:
		fillstat(&r->d, "etc", QTDIR, Qetc, 0555|DMDIR);
		break;
	default:
		for(i=0; i<nelem(dirtab); i++)
			if(dirtab[i].qidpath == path){
				fillstat(&r->d, dirtab[i].name, 0, dirtab[i].qidpath, dirtab[i].perm);
				goto Break2;
			}
		respond(r, "file not found");
		break;
	}
    Break2:
	respond(r, nil);
}

static int
readlist(int off, FILE *fp, Req *r)
{
	char *a, *ea;
	int n;
	int count = off;
	char buf[4096];

   	a = r->ofcall.data;
	ea = a+r->ifcall.count;

	while(count-- > 0){
	  if(fgets(buf, 4096, fp)==NULL){
	    r->ofcall.count = a - (char*)r->ofcall.data;
	    return off;
	  }
	}
	  
	for(;;){
	  if(fgets(buf, 4096, fp)==NULL)
	    n=0;
	  else
	    n = strlen(buf);
	  if(n > (ea - a) || n == 0){
	    r->ofcall.count = a - (char*)r->ofcall.data;
	    return off;
	  }
	  memmove(a,buf,n);
	  a += n;
	  off++;
	}
	/* not reached */
}


			
/*
 * Some of the file system work happens in the fs proc, but
 * fsopen, fsread, fswrite, fsdestroyfid, and fsflush happen in
 * the main proc so that they can access the various shared
 * data structures without worrying about locking.
 */
static int inuse[nelem(dirtab)];
static void
fsopen(Req *r)
{
	int i, *inusep, perm;
	static int need[4] = { 4, 2, 6, 1 };
     
	inusep = nil;
	perm = 5;	/* directory */
	for(i=0; i<nelem(dirtab); i++)
		if(dirtab[i].qidpath == r->fid->qid.path){
			if(dirtab[i].perm & DMEXCL)
				inusep = &inuse[i];
			if(strcmp(r->fid->uid, owner) == 0)
				perm = dirtab[i].perm>>6;
			else
				perm = dirtab[i].perm;
			break;
		}

	if((r->ifcall.mode&~(OMASK|OTRUNC))
	|| (need[r->ifcall.mode&3] & ~perm)){
		respond(r, "permission denied");
		return;
	}

	if(inusep){
		if(*inusep){
			respond(r, "file in use");
			return;
		}
		*inusep = 1;
	}

	respond(r, nil);
}

static void
fsread(Req *r)
{
	FILE *fppasswd;
	FILE *fpgroup;
	char *passwd;
	char *group;
	int slen;

	switch((int)r->fid->qid.path){
	default:
		respond(r, "fsread: cannot happen");
		break;
	case Qroot:
		dirread9p(r, rootdirgen, nil);
		respond(r, nil);
		break;
	case Qetc:
		dirread9p(r, fsdirgen, nil);
		respond(r, nil);
		break;
	case Quserlist:
	        slen = strlen(source);
	        passwd = calloc(8+slen, sizeof(char));
		memmove(passwd, source, slen);
		memmove(passwd+slen, "/passwd", 7);
	        if((fppasswd = fopen(passwd, "r"))==NULL)
		  respond(r, "fsread: file not found");
	        r->fid->aux = (void*)(uintptr)readlist((uintptr)r->fid->aux, fppasswd, r);
		fclose(fppasswd);
		free(passwd);
		respond(r, nil);
		break;
	case Qgrouplist:
	        slen = strlen(source);
	        group = calloc(8+slen, sizeof(char));
		memmove(group, source, slen);
		memmove(group+slen, "/group", 6);
	        if((fpgroup = fopen(group, "r"))==NULL)
		  respond(r, "fsread: file not found");
	        r->fid->aux = (void*)(uintptr)readlist((uintptr)r->fid->aux, fpgroup, r);
		fclose(fpgroup);
		free(group);
		respond(r, nil);
		break;
	}
}

static void
fswrite(Req *r)
{
	respond(r, "fswrite: cannot happen");
}

static void
fsflush(Req *r)
{
  //confirmflush(r->oldreq);
  //	logflush(r->oldreq);
	respond(r, nil);
}

static void
fsdestroyfid(Fid *fid)
{
	
}

static Channel *creq;
static Channel *cfid, *cfidr;

static void
fsreqthread(void *v)
{
	Req *r;

	USED(v);

	while((r = recvp(creq)) != nil){
		switch(r->ifcall.type){
		default:
			respond(r, "bug in fsreqthread");
			break;
		case Topen:
			fsopen(r);
			break;
		case Tread:
			fsread(r);
			break;
		case Twrite:
			fswrite(r);
			break;
		case Tflush:
			fsflush(r);
			break;
		}
	}
}

static void
fsclunkthread(void *v)
{
	Fid *f;

	USED(v);

	while((f = recvp(cfid)) != nil){
		fsdestroyfid(f);
		sendp(cfidr, 0);
	}
}

static void
fsproc(void *v)
{
	USED(v);

	threadcreate(fsreqthread, nil, STACK);
	threadcreate(fsclunkthread, nil, STACK);
	threadexits(nil);
}

static void
fsattach(Req *r)
{
	r->fid->qid = mkqid(QTDIR, qtop);
	r->ofcall.qid = r->fid->qid;
	respond(r, nil);
}

static void
fssend(Req *r)
{
	sendp(creq, r);
}

static void
fssendclunk(Fid *f)
{
	sendp(cfid, f);
	recvp(cfidr);
}

void
fsstart(Srv *s)
{
	USED(s);

	if(extrapasswddir)
		qtop = Qroot;
	else
		qtop = Qetc;
	creq = chancreate(sizeof(Req*), 0);
	cfid = chancreate(sizeof(Fid*), 0);
	cfidr = chancreate(sizeof(Fid*), 0);
	proccreate(fsproc, nil, STACK);
}

Srv fs;

void
fsinit0(void)
{
	fs.attach = fsattach;
	fs.walk1 = fswalk1;
	fs.open = fssend;
	fs.read = fssend;
	fs.write = fssend;
	fs.stat = fsstat;
	fs.flush = fssend;
	fs.destroyfid = fssendclunk;
	fs.start = fsstart;
}

