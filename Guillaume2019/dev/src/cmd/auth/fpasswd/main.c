#include "std.h"
#include "dat.h"
#include <9pclient.h>

int extrapasswddir;
int debug;
char *passname = "passwd";
char *service = "passwd";
char *owner;
char *source = "";
void gflag(char*);

void
usage(void)
{
	fprint(2, "usage: passwd [-Dd] source dest\n");
	threadexitsall("usage");
}

int post9pservicemod(int fd, char *name, char *mtpt);


static void
launchsrv(void *v)
{
  srv(v);
}

void
threadpostmountsrvpasswd(Srv *s, char *name, char *mtpt, int flag)
{
  int fd[2];

  if(!s->nopipe){
    if(pipe(fd) < 0)
      sysfatal("pipe: %r");
    s->infd = s->outfd = fd[1];
    s->srvfd = fd[0];
  }
  if(name || mtpt){
    if(post9pservicemod(s->srvfd, name, mtpt) < 0)
      sysfatal("post9pservice %s: %r", name);
  }else if(!s->nopipe)
    sysfatal("no one to serve");
  if(s->foreground)
    srv(s);
  else
    proccreate(launchsrv, s, 32*1024);
}

void
threadmain(int argc, char *argv[])
{

	owner = getuser();
	quotefmtinstall();
	//fmtinstall('A', attrfmt);
	//fmtinstall('H', encodefmt);
	//fmtinstall('N', attrnamefmt);

	ARGBEGIN{
	default:
		usage();
	case 'D':
		chatty9p++;
		break;
	case 'd':
		debug = 1;
		break;
	}ARGEND

	if(argc != 2)
		usage();

	rfork(RFNOTEG);
	source = argv[0];
	fsinit0();
	threadpostmountsrvpasswd(&fs, service, argv[1], MBEFORE);
	threadexits(nil);
}
