#include <u.h>
#include <libc.h>
#include <mp.h>
#include <libsec.h>
#include <auth.h>
#include <thread.h>

#undef exits
void
usage(void)
{
	fprint(2, "usage: auth/gqgen [-f] [-u user] [-d dom] [-b bits] [-e elen] [-t 'attr=value attr=value ...']\n");
	exits("usage");
}

GQpriv *
gqgenwithfactotum(int nlen, int elen, int rounds, char* user, char* dom){
  AuthInfo *ai;
  GQpriv *key;
  char *rptr;

  rptr = nil;
  ai = auth_proxy(0, auth_getkey, "proto=gq size=%d role=generate user=%s dom=%s", nlen, user, dom);
  if (ai==nil || !ai->secret|| !ai->cap)
    return nil;
  key = gqprivalloc();
  key->privcert = mpnew(nlen);
  key->pub.rsa.n = mpnew(nlen);
  key->pub.rsa.ek = mpnew(elen);
  strtomp(ai->cap, &rptr, 16, key->pub.rsa.n);
  strtomp(rptr+1, &rptr, 16, key->pub.rsa.ek);
  strtomp(rptr+1, nil, 16, key->privcert);
  auth_freeAI(ai);
  return key;
}
									       
void
threadmain(int argc, char **argv)
{
	char *s;
	int bits;
	int elen;
	char *tag;
	GQpriv *key;
	char *user;
	char *dom;
	char hasuser;
	char hasdom;
	char factotum;

	factotum = 0;
	hasuser = 0;
	hasdom = 0;
	bits = 1024;
	elen = 0;
	tag = nil;
	key = nil;
	fmtinstall('B', mpfmt);

	ARGBEGIN{
	case 'b':
		bits = atoi(EARGF(usage()));
		if(bits == 0)
			usage();
		break;
	case 'e':
	  elen = atoi(EARGF(usage()));
	  if(elen == 0)
	    usage();
	  break;
        case 'f':
	  factotum = 1;
	  break;
        case 'u':
	  hasuser = 1;
	  user = EARGF(usage());
	  break;
        case 'd':
	  hasdom = 1;
	  dom = EARGF(usage());
	  break;
	case 't':
		tag = EARGF(usage());
		break;
	default:
		usage();
	}ARGEND

	if(argc != 0)
		usage();

	if(!hasuser)
	  user = "none";

	if(!hasdom)
	  dom = "localhost";
	if (factotum && (key = gqgenwithfactotum(bits, 6, 0, user, dom))==nil){
	  fprint(2, "coudn't retrieve rsa key, generating one");
	  factotum = 0;
	}
	
	if(!factotum)
	  do{
	    if(key)
	      gqprivfree(key);
	    key = gqgen(bits, 6, 0, user, dom);
	  }while(mpsignif(key->pub.rsa.n) != bits);
	
	s = smprint("key proto=rsa role=gq ek=%lB n=%lB size=%d dom=%s\nkey proto=gq role=server user=%s dom=%s %s%shash=sha1\n\nkey proto=gq role=client user=%s dom=%s hash=sha1 ek=%lB n=%lB size=%d %s%s!priv=%lB\n",
		    key->pub.rsa.ek, key->pub.rsa.n, mpsignif(key->pub.rsa.n), dom,
		    user, dom, tag ? tag : "", tag ? " " : "",
		    user, dom, key->pub.rsa.ek, key->pub.rsa.n, mpsignif(key->pub.rsa.n), tag ? tag : "", tag ? " " : "", key->privcert);
	
	if(s == nil)
		sysfatal("smprint: %r");

	if(write(1, s, strlen(s)) != strlen(s))
		sysfatal("write: %r");
	
	threadexitsall(nil);
}
