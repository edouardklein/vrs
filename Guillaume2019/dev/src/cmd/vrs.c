#include <u.h>
#include <libc.h>
#include <fcall.h>
#include <auth.h>
#include <9pclient.h>
//#include "authlocal.h"
#include <thread.h>

#undef _exits
/*
 * Rather than reading /adm/users, which is a lot of work for
 * a toy program, we assume all groups have the form
 *	NNN:user:user:
 * meaning that each user is the leader of his own group.
 */

enum
{
	OPERM	= 0x3,		/* mask of all permission types in open mode */
	Nram	= 2048,
	Maxsize	= 512*1024*1024,
	Maxfdata	= 8192
};

typedef struct Afid
{
  u32int afid;
  ushort tag;
  char *uname;
  char *aname;
  AuthInfo *ai;
  AuthRpc *rpc;
  Qid qid;
  char *inbuf;
  short wrin;
  char *outbuf;
  short wrout;
  short rdout;
  struct Afid *next;
} Afid;

typedef struct Fid
{
  u32int fid;
  Afid *afid;
  int sfd[2];
  struct Fid *next;
} Fid;


enum
{
	Pexec =		1,
	Pwrite = 	2,
	Pread = 	4,
	Pother = 	1,
	Pgroup = 	8,
	Powner =	64
};

enum {
  ARgiveup = 100
};


static int
dorpc(AuthRpc *rpc, char *verb, char *val, int len, AuthGetkey *getkey)
{
  int ret;

  for(;;){
    if((ret = auth_rpc(rpc, verb, val, len)) != ARneedkey && ret != ARbadkey)
      return ret;
    if(getkey == nil)
      return ARgiveup;/* don't know how */
   if((*getkey)(rpc->arg) < 0)
      return ARgiveup;/* user punted */
  }
}


ulong	path;		/* incremented for each new file */
Fid     *fids = NULL;
Afid	*afids = NULL;
int	mfd[2];
char	*user;
uchar	mdata[IOHDRSZ+Maxfdata];
uchar   mdata2[IOHDRSZ+Maxfdata];
char	rdata[Maxfdata];	/* buffer for data in reply */
uchar statbuf[STATMAX];
Fcall thdr;
Fcall	rhdr;
int	messagesize = sizeof mdata;
int     tostdio;

Fid * getfid(u32int);
Afid *	newafid(u32int);
void	error(char*);
void	io(void);
void    ioson(Fid*);
void	*erealloc(void*, ulong);
void	*emalloc(ulong);
char	*estrdup(char*);
void	usage(void);

char	*rflush(Afid*), *rversion(Afid*), *rauth(Afid*),
	*rattach(Afid*), //*rwalk(Afid*),
	//*ropen(Afid*), //*rcreate(Afid*),
  *rread(Afid*), *rwrite(Afid*), *rclunk(Afid*), *rdefault(Afid*);
	//*rremove(Afid*), *rstat(Afid*), *rwstat(Afid*);

char 	*(*fcalls[Tmax])(Afid*);

char *sonproc[3];

static void
initfcalls(void)
{
	fcalls[Tversion]= rversion;
       	fcalls[Tflush]=	rflush;
	fcalls[Tauth]= rauth;
	fcalls[Tattach]= rattach;
	fcalls[Twalk]= rdefault;//rwalk;
	fcalls[Topen]= rdefault;//ropen;
	fcalls[Tcreate]= rdefault;//rcreate;
	fcalls[Tread]= rread;
	fcalls[Twrite]= rwrite;
	fcalls[Tclunk]=	rclunk;
	fcalls[Tremove]= rdefault;//rremove;
	fcalls[Tstat]= rdefault;//rstat;
	fcalls[Twstat]= rdefault;//rwstat;
}

char	Eperm[] =	"permission denied";
char	Enotdir[] =	"not a directory";
char	Eauth[] =	"vrs: authentication required";
char	Enotexist[] =	"file does not exist";
char	Einuse[] =	"file in use";
char	Eexist[] =	"file exists";
char	Eisdir[] =	"file is a directory";
char	Enotowner[] =	"not owner";
char	Eisopen[] = 	"file already open for I/O";
char	Excl[] = 	"exclusive use file already open";
char	Ename[] = 	"illegal name";
char	Eversion[] =	"unknown 9P version";
char	Enotempty[] =	"directory not empty";
char	Ebadfid[] =	"bad fid";

int debug;
int private;

void
notifyf(void *a, char *s)
{
  USED(a);
  if(strncmp(s, "interrupt", 9) == 0)
    noted(NCONT);
  noted(NDFLT);
}


void
threadmain(int argc, char *argv[])
{
  char *defmnt;
  int p[2];
  int stdio = 0;
  char *service;

  sonproc[0]="./ramfsmod";
  sonproc[1]="-Di";
  sonproc[2]=NULL;
  tostdio = 0;
  initfcalls();
  service = "authserver";
  defmnt = nil;
  ARGBEGIN{
    case 'D':
      debug = 1;
      break;
      case 'i':
	defmnt = 0;
	stdio = 1;
	mfd[0] = 0;
	mfd[1] = 1;
	break;
	case 's':
	  defmnt = nil;
	  break;
	  case 'm':
	    defmnt = ARGF();
	    break;
	    case 'p':
	      private++;
	      break;
	      case 'S':
		defmnt = 0;
		service = EARGF(usage());
		break;
		default:
		  usage();
  }ARGEND
     USED(defmnt);

  if(pipe(p) < 0)
    error("pipe failed");
  if(!stdio){
    mfd[0] = p[0];
    mfd[1] = p[0];
    if(post9pservice(p[1], service, nil) < 0)
      sysfatal("post9pservice %s: %r", service);
  }

  user = getuser();
  notify(notifyf);
  
	
  if(debug)
    fmtinstall('F', fcallfmt);
  switch(rfork(RFFDG|RFPROC|RFNAMEG|RFNOTEG)){
  case -1:
    error("fork");
  case 0:
    close(p[1]);
    io();
    break;
  default:
    close(p[0]);	/* don't deadlock if child fails */
  }
  threadexitsall(0);
}

char*
rdefault(Afid *x)
{
  return Eauth;
}

char*
rversion(Afid *x)
{
  Afid *f;

  USED(x);
  for(f = afids; f; f = f->next)
    if(f->ai || f->rpc)
    rclunk(f);
  if(thdr.msize > sizeof mdata)
    rhdr.msize = sizeof mdata;
  else
    rhdr.msize = thdr.msize;
  messagesize = rhdr.msize;
  if(strncmp(thdr.version, "9P2000", 6) != 0)
    return Eversion;
  rhdr.version = "9P2000";
  return 0;
}

char*
rauth(Afid *f)
{
  int lenu, lena;
  char p[]="proto=p9any role=server";
 
  if(f->rpc || f->ai)
    return Ebadfid;
  f->tag =thdr.tag;
  f->ai = NULL;
  lenu = 1+strlen(thdr.uname);
  f->uname = emalloc(sizeof(char)*lenu);
  memcpy(f->uname, thdr.uname, lenu);
  lena = 1+strlen(thdr.aname);
  f->aname = emalloc(sizeof(char)*lena);
  memcpy(f->aname, thdr.aname, lena);
  f->qid.path = ++path;
  f->qid.type = QTAUTH;
  f->qid.vers = 0;
  f->outbuf = calloc(1000,sizeof(char));
  f->inbuf = calloc(1000,sizeof(char));
  f->wrout = 0;
  f->wrin = 0;
  f->rdout = 0;
  f->rpc = auth_allocrpc();
  if(f->rpc == nil)
    return "vrs: couldn't open channel to factotum";

  if(dorpc(f->rpc, "start", p, strlen(p), nil)!=ARok)
    return "vrs: couldn't start authentication via factotum";

  return 0;  
 
}

char*
rflush(Afid *x)
{
  USED(x);
  return 0;
}

char*
rread(Afid *f)
{
  char *buf;
  int cnt;
  char mustread = 1;
    
  if(!f->rpc)
    return Eauth;
  if(f->tag!=thdr.tag)
    return Enotowner;
  rhdr.count = 0;
  buf = rdata;
  cnt = thdr.count;
  if(cnt > Maxfdata)
    cnt = Maxfdata;

  if(f->wrout) mustread = 0;
  
  if(mustread)
    switch(dorpc(f->rpc, "read", nil, 0, nil)){
    case ARok:
      memcpy(f->outbuf, f->rpc->arg, f->rpc->narg);
      f->wrout=f->rpc->narg;
      break;
    case ARphase:
      break;
    default:
      return "vrs: unattended response from factotum";
      break;
    }
  
  if (cnt > f->wrout - f->rdout){
    cnt = f->wrout - f->rdout;
  }
  memmove(buf,f->outbuf+f->rdout,cnt); 
  rhdr.data = buf;
  rhdr.count = cnt;
  for(int i=0;i<cnt;i++) f->outbuf[f->rdout+i]=0;
  f->rdout+=cnt;
  if(f->wrout == f->rdout){
    f->wrout = 0;
    f->rdout = 0;
  } 

  if(mustread)
    switch(dorpc(f->rpc, "read", nil, 0, nil)){
    case ARphase: //expects the client to write something
      break;
    case ARdone:
      f->ai = auth_getinfo(f->rpc);
      auth_freerpc(f->rpc);
      f->rpc = NULL;
      f->wrout = 0;
      f->rdout = 0;
      f->wrin = 0;
      free(f->outbuf);
      f->outbuf = NULL;
      free(f->inbuf);
      f->inbuf = NULL;
      fprint(2, "authinfo: caller:%s server:%s cap:%s nsec:%d\n", f->ai->cuid, f->ai->suid, f->ai->cap, f->ai->nsecret);
      break;
    case ARok: //should not happen, or it will be very nasty
      return "vrs: don't know why factotum has something to say";
      break;
    default:
      return "vrs: unattended 2nd response from factotum";
      break;
    }
  
  return 0;
}

char*
rwrite(Afid *f)
{
  int cnt;

  if(!f->rpc)
    return Ebadfid;
  if(f->tag!=thdr.tag)
    return Enotowner;
  cnt = thdr.count;
  memmove(f->inbuf+f->wrin, thdr.data, cnt);
  f->wrin+=cnt;
  switch(dorpc(f->rpc, "write", f->inbuf, f->wrin, nil)){
  case ARtoosmall:
    if(atoi(f->rpc->arg)>Maxfdata)
      return "rpc write error";
    break;
  case ARok:
    for(int i=0;i<f->wrin;i++) f->inbuf[i]=0;
    f->wrin=0;
    break;
  default:
    return "write error";
  }
  f->qid.vers++;
  rhdr.count = cnt;
  return 0;
}

char *
rattach(Afid *af){
  Fid *f;
  int p[2];
  if(!af->ai)
    return Eauth;
  if(strcmp(af->uname, thdr.uname)||strcmp(af->aname, thdr.aname))
    return Eperm;
  f = getfid(thdr.fid);
  if(!f){
    f = emalloc(sizeof *f);
    f->fid = thdr.fid;
    f->afid = af;
    f->next = fids;
    fids = f;
  }
  thdr.afid = NOFID;
  fprint(2, "%s\n", thdr.uname);
  if(tostdio){
    f->sfd[0] = 0;
    f->sfd[1] = 1;
  } else {
    pipe(p);
    switch(rfork(RFFDG|RFPROC)){
    case -1:
      return "unable to attach";
      break;
    case 0:
      dup(p[1], 0);
      dup(p[1], 1);
      close(p[0]);
      fprint(2, "exec %s\n", sonproc[0]);
      execvp(sonproc[0], sonproc);
      _exits("exec");
      break;
    default:
      dup(p[0], f->sfd[0]);
      dup(p[0], f->sfd[1]);
      close(p[1]);
      break;
    }
  }
  ioson(f);
  if(rhdr.type == Rerror)
    return rhdr.ename;
  return 0;
}

char *
rclunk(Afid *f)
{
  char *e = nil;
  if(f->rpc)
    auth_freerpc(f->rpc);
  if(f->ai)
    auth_freeAI(f->ai);
  if(f->inbuf)
    free(f->inbuf);
  if(f->outbuf)
    free(f->outbuf);
  f->rpc = NULL;
  f->ai = NULL;
  f->inbuf = NULL;
  f->outbuf = NULL;
  f->rdout = 0;
  f->wrout = 0;
  f->wrin = 0;
  fprint(2, "clunk afid %d\n", f->afid);
  if (f->uname)
    free(f->uname);
  if(f->aname)
    free(f->aname);
  f->uname = NULL;
  f->aname = NULL;
  f->tag = 65535;
  return e;
}

Fid *
getfid(u32int fid)
{
  Fid *f;
  for(f = fids; f; f = f->next)
    if(f->fid == fid)
      return f;
  return NULL;
}

Fid *
removefid(u32int fid)
{
  Fid *f, *ff;
  if(fids && fids->fid == fid){
    f=fids;
    fids = fids->next;
    return f;
  }
  for(f = fids; f && f->next; f = f->next)
    if(f->next->fid == fid){
      ff = f->next;
      f->next = f->next->next;
      return ff;
    }
  return NULL;
}
 
Afid *
newafid(u32int afid)
{
  Afid *f, *ff;
  ff = 0;
  for(f = afids; f; f = f->next)
    if(f->afid == afid)
      return f;
    else if(!ff && !f->ai && !f->rpc)
      ff = f;
  if(ff){
  ff->afid = afid;
  return ff;
}
  f = emalloc(sizeof *f);
  f->afid = afid;
  f->next = afids;
  afids = f;
  return f;
}

void
ioson(Fid *f)
{
  int n;
  
  n = convS2M(&thdr, mdata2, messagesize);
  if(n == 0)
    error("convS2M error on write to son");
  if(write(f->sfd[1], mdata2, n) != n)
    error("mount write to son");
  if((n=read9pmsg(f->sfd[0], mdata2, messagesize))<=0)
    error("mount read from son");
  if(convM2S(mdata2, n, &rhdr) != n)
    error("convM2S error on read from son");
}

 
void
io(void)
{
  char *err, buf[20];
  int n, pid, ctl;
  Fid *f, *ff;
  
  pid = getpid();
  if(private){
    snprint(buf, sizeof buf, "/proc/%d/ctl", pid);
    ctl = open(buf, OWRITE);
    if(ctl < 0){
      fprint(2, "can't protect ramfs\n");
    }else{
      fprint(ctl, "noswap\n");
      fprint(ctl, "private\n");
      close(ctl);
    }
  }

  for(;;){
    /*
     * reading from a pipe or a network device
     * will give an error after a few eof reads.
     * however, we cannot tell the difference
     * between a zero-length read and an interrupt
     * on the processes writing to us,
     * so we wait for the error.
     */
    n = read9pmsg(mfd[0], mdata, messagesize);
    if(n < 0)
      error("mount read");
    if(n == 0)
      error("mount eof");
    if(convM2S(mdata, n, &thdr) == 0)
      continue;
    
    if(debug)
      fprint(2, "vrs %d:<-%F\n", pid, &thdr);
    
    if(!fcalls[thdr.type])
      err = "bad fcall type";
    else {
  /* authentication */
      switch(thdr.type){
      case Tauth:
	err = rauth(newafid(thdr.afid));
	break;
      case Tattach:
	err = rattach(newafid(thdr.afid));
	break;
      case Tversion:
	err = rversion(newafid(thdr.fid));
	break;
      case Tclunk:
	if(f=removefid(thdr.fid)){
	  ioson(f);
	  free(f);
	  if(rhdr.type == Rerror)
	    err = rhdr.ename;
	  else
	    err = NULL;
	}
	else
	  err = rclunk(newafid(thdr.fid));
	break;
	    
      default:
	if(f=getfid(thdr.fid)) {
	  ioson(f);
	  if(rhdr.type == Rerror)
	    err = rhdr.ename;
	  else {
	    if(rhdr.type == Rwalk){
	      ff = emalloc(sizeof *f);
	      ff->fid = thdr.newfid;
	      ff->afid = f->afid;
	      ff->sfd[0] = f->sfd[0];
	      ff->sfd[1] = f->sfd[1];
	      ff->next = fids;
	      fids = ff;
	    }
	    err = NULL;
	  }
	}
	else
	  err = (*fcalls[thdr.type])(newafid(thdr.fid));
	break;
      }
    }
    if(err){
      rhdr.type = Rerror;
      rhdr.ename = err;
    } else {
      rhdr.type = thdr.type + 1;
      rhdr.fid = thdr.fid;
    }
    rhdr.tag = thdr.tag;
    if(debug)
      fprint(2, "vrs %d:->%F\n", pid, &rhdr);/**/
    n = convS2M(&rhdr, mdata, messagesize);
    if(n == 0)
      error("convS2M error on write");
    
    if(write(mfd[1], mdata, n) != n)
      error("mount write");
  }
}

void
error(char *s)
{
  fprint(2, "%s: %s: %r\n", argv0, s);
  threadexitsall(s);
}

void *
emalloc(ulong n)
{
  void *p;

  p = malloc(n);
  if(!p)
    error("out of memory");
  memset(p, 0, n);
  return p;
}

void *
erealloc(void *p, ulong n)
{
  p = realloc(p, n);
  if(!p)
    error("out of memory");
  return p;
}

char *
estrdup(char *q)
{
  char *p;
  int n;

  n = strlen(q)+1;
  p = malloc(n);
  if(!p)
    error("out of memory");
  memmove(p, q, n);
  return p;
}

void
usage(void)
{
  fprint(2, "usage: %s [-is] [-m mountpoint]\n", argv0);
  threadexitsall("usage");
}
