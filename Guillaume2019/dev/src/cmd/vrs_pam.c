#include <u.h>
#include <libc.h>
#include <fcall.h>

#include <thread.h>
#include <strings.h>

#include <security/pam_appl.h>
#include <pwd.h>
#undef _exits

#define SIZEBUF 10000

void to_hexa(char *out, const char *in, int n){
  for(int i=0;i<n;i++){
    out[2*i]=(in[i]>>4)&15;
    out[2*i+1]=(in[i])&15;
  }
  for(int i=0;i<2*n;i++){
    out[i]+= (out[i]<10)? 48: 87;
  }
}

void from_hexa(char *out, const char *in, int n){
  char c1, c2;
  for(int i=0;i<n;i++){
    c1=(in[2*i]<90)? in[2*i]-48 : in[2*i]-87;
    c2=(in[2*i+1]<90)? in[2*i+1]-48 : in[2*i+1]-87;
    out[i] = (c1<<4)+c2;
  }
}

/*
 * Rather than reading /adm/users, which is a lot of work for
 * a toy program, we assume all groups have the form
 *	NNN:user:user:
 * meaning that each user is the leader of his own group.
 */

enum
{
	OPERM	= 0x3,		/* mask of all permission types in open mode */
	Nram	= 2048,
	Maxsize	= 512*1024*1024,
	Maxfdata	= 8192
};

typedef struct Afid
{
  u32int afid;
  ushort tag;
  char *uname;
  char *aname;
  pam_handle_t *pamh;
  char authenticating;
  char mustyield;
  Qid qid;
  char *inbuf;
  short wrin;
  char *outbuf;
  short wrout;
  short rdout;
  char *errbuf;
  char session_opened;
  struct Afid *next;
} Afid;

typedef struct Fid
{
  u32int fid;
  Afid *afid;
  int sfd[2];
  struct Fid *next;
} Fid;


enum
{
	Pexec =		1,
	Pwrite = 	2,
	Pread = 	4,
	Pother = 	1,
	Pgroup = 	8,
	Powner =	64
};





ulong	path;	/* incremented for each new file */
Fid     *fids = NULL;
Afid	*afids = NULL;
int	mfd[2];
char	*user;
uchar	mdata[IOHDRSZ+Maxfdata];
uchar   mdata2[IOHDRSZ+Maxfdata];
char	rdata[Maxfdata];	/* buffer for data in reply */
char    errdata[Maxfdata+1];      /*buffer for error reply */
uchar statbuf[STATMAX];
Fcall thdr;
Fcall	rhdr;
int	messagesize = sizeof mdata;
int     tostdio;

Fid * getfid(u32int);
Afid *	newafid(u32int);
void	error(char*);
void	io(void);
char    *ioson(Fid*);
void	*erealloc(void*, ulong);
void	*emalloc(ulong);
char	*estrdup(char*);
void	usage(void);

char	*rflush(Afid*), *rversion(Afid*), *rauth(Afid*),
	*rattach(Afid*), //*rwalk(Afid*),
	//*ropen(Afid*), //*rcreate(Afid*),
  *rread(Afid*), *rwrite(Afid*), *rclunk(Afid*), *rdefault(Afid*);
	//*rremove(Afid*), *rstat(Afid*), *rwstat(Afid*);

char 	*(*fcalls[Tmax])(Afid*);

char *sonproc[3];

static void
initfcalls(void)
{
	fcalls[Tversion]= rversion;
       	fcalls[Tflush]=	rflush;
	fcalls[Tauth]= rauth;
	fcalls[Tattach]= rattach;
	fcalls[Twalk]= rdefault;//rwalk;
	fcalls[Topen]= rdefault;//ropen;
	fcalls[Tcreate]= rdefault;//rcreate;
	fcalls[Tread]= rread;
	fcalls[Twrite]= rwrite;
	fcalls[Tclunk]=	rclunk;
	fcalls[Tremove]= rdefault;//rremove;
	fcalls[Tstat]= rdefault;//rstat;
	fcalls[Twstat]= rdefault;//rwstat;
}

char	Eperm[] =	"permission denied";
char	Enotdir[] =	"not a directory";
char	Eauth[] =	"vrs: authentication required";
char	Enotexist[] =	"file does not exist";
char	Einuse[] =	"file in use";
char	Eexist[] =	"file exists";
char	Eisdir[] =	"file is a directory";
char	Enotowner[] =	"not owner";
char	Eisopen[] = 	"file already open for I/O";
char	Excl[] = 	"exclusive use file already open";
char	Ename[] = 	"illegal name";
char	Eversion[] =	"unknown 9P version";
char	Enotempty[] =	"directory not empty";
char	Ebadfid[] =	"bad fid";

int debug;
int private;

void
notifyf(void *a, char *s)
{
  USED(a);
  if(strncmp(s, "interrupt", 9) == 0)
    noted(NCONT);
  noted(NDFLT);
}

Afid *
newafid(u32int afid)
{
  Afid *f, *ff;
  ff = 0;
  for(f = afids; f; f = f->next)
    if(f->afid == afid)
      return f;
    else if(!ff && !f->pamh)
      ff = f;
  if(ff){
  ff->afid = afid;
  return ff;
}
  f = emalloc(sizeof *f);
  f->afid = afid;
  f->next = afids;
  f->authenticating = 0;
  afids = f;
  return f;
}

int pam_conversation_body(int num_msg, const struct pam_message **msg,
			  struct pam_response **resp, void *appdata_ptr){
  fprint(2,"coucou, entree dans la fonction conv\n");
  Afid *f = ((Afid *)appdata_ptr);
  fprint(2, "passed\n");
  struct pam_response *oldresp = *resp;
  struct pam_response *response;
  int l = 0;
  *resp = NULL;
  if(num_msg)
    if((*resp = malloc(num_msg*sizeof(struct pam_response))) == NULL){
      *resp = oldresp;
      return PAM_CONV_ERR;
    }
  for(int i=0; i<num_msg; i++){
    const struct pam_message *message = &((*msg)[i]);
    response = &((*resp)[i]);
    response->resp_retcode = 0;
    response->resp = NULL;
    switch (message->msg_style){
    case PAM_PROMPT_ECHO_OFF:
      if((response->resp = calloc((1+2*f->wrin), sizeof(char))) == NULL){
	for(int j=0;j<i;j++)
	  free((*resp)[i].resp);
    	free(*resp);
	*resp = oldresp;
	return PAM_CONV_ERR;
      }
      to_hexa(response->resp, f->inbuf, f->wrin);
      bzero(f->inbuf, f->wrin);
      f->wrin = 0;
      break;
    case PAM_TEXT_INFO:
      l = strlen(message->msg)/2;
      if(l>=SIZEBUF-f->wrout)
	l=SIZEBUF-f->wrout-1;
      from_hexa(f->outbuf+f->wrout, message->msg, l); 
      f->wrout+=l;
      f->outbuf[f->wrout]=0;
      break;
    case PAM_ERROR_MSG:
      l = strlen(message->msg);
      if(l>Maxfdata)
	l=Maxfdata;
      memcpy(f->errbuf, message->msg, l);
    default:
      break;
    }
  }
  f->mustyield = 1;
  while (f->mustyield)
    yield();

  return PAM_SUCCESS;
}
     
void
authenticate(void *arg){
  char returnvalue[20];
  pam_handle_t* pamh = (pam_handle_t *) arg;
  int asw = pam_authenticate(pamh, 0);
  sprint(returnvalue, "RETVAL=%d", asw);
  pam_putenv(pamh, returnvalue);
  threadexits(0);
}

void
threadmain(int argc, char *argv[])
{
  char *defmnt;
  int p[2];
  int stdio = 0;
  char *service;

  seteuid(getuid());
  sonproc[0]="./ramfsmod";
  sonproc[1]="-Di";
  sonproc[2]=NULL;
  tostdio = 0;
  initfcalls();
  service = "authserver";
  defmnt = nil;
  ARGBEGIN{
    case 'D':
      debug = 1;
      break;
    case 'i':
      defmnt = 0;
      stdio = 1;
      mfd[0] = 0;
      mfd[1] = 1;
      break;
    case 's':
      defmnt = nil;
      break;
    case 'm':
      defmnt = ARGF();
      break;
    case 'p':
      private++;
      break;
    case 'S':
      defmnt = 0;
      service = EARGF(usage());
      break;
    default:
      usage();
  }ARGEND
     USED(defmnt);

  if(pipe(p) < 0)
    error("pipe failed");
  if(!stdio){
    mfd[0] = p[0];
    mfd[1] = p[0];
    if(post9pservice(p[1], service, nil) < 0)
      sysfatal("post9pservice %s: %r", service);
  }

  user = getuser();
  notify(notifyf);
  
	
  if(debug)
    fmtinstall('F', fcallfmt);
  switch(rfork(RFFDG|RFPROC|RFNAMEG|RFNOTEG)){
  case -1:
    error("fork");
  case 0:
    close(p[1]);
    io();
    break;
  default:
    close(p[0]);	/* don't deadlock if child fails */
  }
  threadexitsall(0);
}

char*
rdefault(Afid *x)
{
  return Eauth;
}

char*
rversion(Afid *x)
{
  Afid *f;

  USED(x);
  for(f = afids; f; f = f->next)
    if(f->pamh)
      rclunk(f);
  if(thdr.msize > sizeof mdata)
    rhdr.msize = sizeof mdata;
  else
    rhdr.msize = thdr.msize;
  messagesize = rhdr.msize;
  if(strncmp(thdr.version, "9P2000", 6) != 0)
    return Eversion;
  rhdr.version = "9P2000";
  return 0;
}

char*
rauth(Afid *f)
{
  int lenu, lena, starterr;
  char *ruser; char *rdom;
  char pam_afid[20];
  const struct pam_conv pam_conversation = {pam_conversation_body, f};
  if(f->authenticating || f->pamh)
    return Ebadfid;
  f->authenticating = 1;
  f->tag = thdr.tag;
  lenu = 1+strlen(thdr.uname);
  f->uname = emalloc(sizeof(char)*lenu);
  memcpy(f->uname, thdr.uname, lenu);
  lena = 1+strlen(thdr.aname);
  f->aname = emalloc(sizeof(char)*lena);
  memcpy(f->aname, thdr.aname, lena);
  f->qid.path = ++path;
  f->qid.type = QTAUTH;
  f->qid.vers = 0;
  f->outbuf = calloc(SIZEBUF,sizeof(char));
  f->inbuf = calloc(SIZEBUF,sizeof(char));
  f->wrout = 0;
  f->wrin = 0;
  f->rdout = 0;
  f->errbuf = calloc(Maxfdata+1, sizeof(char));
  f->session_opened = 0;
  if((starterr=pam_start("factotum", f->uname, &pam_conversation, &(f->pamh)))!=PAM_SUCCESS){
    strcpy(errdata, pam_strerror(f->pamh, starterr));
    return errdata;
  }
  sprint(pam_afid, "AFID=%d", f->afid);
  pam_putenv(f->pamh, pam_afid);

  ruser = calloc(lenu, sizeof(char));
  memmove(ruser, f->uname, lenu);
  rdom = strchr(ruser, '@');
  if(rdom) *(rdom++)=0;
  if(pam_set_item(f->pamh, PAM_RUSER, (void*) ruser)!=PAM_SUCCESS
     || pam_set_item(f->pamh, PAM_RHOST, (void*) rdom)!=PAM_SUCCESS){
    free(ruser);
    return "couldnt retrieve requesting user name and dom";
  }
  free(ruser);
  threadcreate(authenticate, f->pamh, 8192*16);

  return 0;  
 
}

char*
rflush(Afid *x)
{
  USED(x);
  return 0;
}

char*
rread(Afid *f) 
{
  char *buf = rdata, *err = f->errbuf;
  int cnt,errlen;
  if(!f->authenticating)
    return Eauth;
  if(f->tag!=thdr.tag)
    return Enotowner;
  rhdr.count = 0;
  cnt = thdr.count;
  if(cnt > Maxfdata)
    cnt = Maxfdata;

  if(!f->wrout){
    f->mustyield = 0;
    yield();
  }
  errlen = strlen(err);
  if(errlen){
    memmove(errdata, err, errlen);
    bzero(err, errlen);    
    return errdata;
  }
  if (cnt > f->wrout - f->rdout){
    cnt = f->wrout - f->rdout;
  }
  memmove(buf,f->outbuf+f->rdout,cnt); 
  rhdr.data = buf;
  rhdr.count = cnt;
  bzero(f->outbuf+f->rdout, cnt);

  f->rdout+=cnt;
  if(f->wrout == f->rdout){
    f->wrout = 0;
    f->rdout = 0;
  } 

  return 0;
}

char*
rwrite(Afid *f)
{
  char *err = f->errbuf;
  int errlen;
  int cnt;

  if(!f->authenticating)
    return Ebadfid;
  if(f->tag!=thdr.tag)
    return Enotowner;
  cnt = thdr.count;
  memmove(f->inbuf+f->wrin, thdr.data, cnt);
  f->wrin+=cnt;

  f->mustyield = 0;
  yield();

  errlen = strlen(err);
  if(errlen){
    memmove(errdata, err, errlen);
    bzero(err, errlen);    
    return errdata;
  }
  
  f->qid.vers++;
  rhdr.count = cnt;
  return 0;
}

char *
rattach(Afid *af)
{
  Fid *f;
  int p[2];
  int validity;
  char *pamuser = NULL;
  char *returnson = NULL;

  if(!(af->pamh))
    return Eauth;
  if(af->authenticating){
    af->mustyield = 0;
    yield();
    bzero(af->inbuf, SIZEBUF);
    bzero(af->outbuf, SIZEBUF);
    free(af->inbuf);
    free(af->outbuf);
    af->inbuf = NULL;
    af->outbuf = NULL;
    af->wrin = 0;
    af->wrout = 0;
    af->rdout = 0;
    af->authenticating = 0;
  }
  
  if((validity=pam_acct_mgmt(af->pamh, 0))!=PAM_SUCCESS){
    strcpy(errdata, pam_strerror(af->pamh, validity));
    return errdata;
  }
  
  if(pam_setcred(af->pamh, PAM_ESTABLISH_CRED)!=PAM_SUCCESS)
    return Eperm;

  
  pam_get_item(af->pamh, PAM_USER, (const void**) &pamuser);
  
  if(strcmp(af->uname, thdr.uname)||strcmp(af->aname, thdr.aname)||strcmp(af->uname, pamuser))
    return Eperm;
  f = getfid(thdr.fid);
  if(!f){
    f = emalloc(sizeof *f);
    f->fid = thdr.fid;
    f->afid = af;
    f->next = fids;
    fids = f;
  }
  thdr.afid = NOFID;
  fprint(2, "%s\n", thdr.uname);
  pipe(p);
  if(!af->session_opened){
    af->session_opened = 1;
    switch(rfork(RFFDG|RFPROC)){
    case -1:
      return "unable to attach";
      break;
    case 0:
      dup(p[1], 0);
      dup(p[1], 1);
      close(p[0]); 
      if(seteuid(0)==-1 || setuid(getpwnam(pamuser)->pw_uid)==-1){ //cannot su
	close(p[1]);
   	_exits("coudn't start NT-SA");
      }
      fprint(2, "exec %s\n", sonproc[0]);
      execvp(sonproc[0], sonproc);
      _exits("exec");
      break;
    default:
      dup(p[0], f->sfd[0]);
      dup(p[0], f->sfd[1]);
      close(p[1]);
      break; 
    }
  }
  if(returnson=ioson(f)){
    fprint(2, "%s\n", returnson);
    af->session_opened = 0;
    return Enotexist;
  }
  if(rhdr.type == Rerror)
    return rhdr.ename;
  return 0;
}
 
char *
rclunk(Afid *f)
{
  char *e = nil;
  if(f->inbuf)
    free(f->inbuf);
  if(f->outbuf)
    free(f->outbuf);
  if(f->errbuf)
    free(f->errbuf);
  if(f->pamh){
    pam_close_session(f->pamh, 0);
    pam_end(f->pamh, pam_setcred(f->pamh, PAM_DELETE_CRED));
  }
  f->pamh = NULL;
  f->inbuf = NULL;
  f->outbuf = NULL;
  f->errbuf = NULL;
  f->rdout = 0;
  f->wrout = 0;
  f->wrin = 0;
  f->session_opened = 0;
  fprint(2, "clunk afid %d\n", f->afid);
  if (f->uname)
    free(f->uname);
  if(f->aname)
    free(f->aname);
  f->uname = NULL;
  f->aname = NULL;
  f->tag = 65535;
  f->authenticating = 0;
  return e;
}

Fid *
getfid(u32int fid)
{
  Fid *f;
  for(f = fids; f; f = f->next)
    if(f->fid == fid)
      return f;
  return NULL;
}

Fid *
removefid(u32int fid)
{
  Fid *f, *ff;
  if(fids && fids->fid == fid){
    f=fids;
    fids = fids->next;
    return f;
  }
  for(f = fids; f && f->next; f = f->next)
    if(f->next->fid == fid){
      ff = f->next;
      f->next = f->next->next;
      return ff;
    }
  return NULL;
}
 

char*
ioson(Fid *f)
{
  int n;
  
  n = convS2M(&thdr, mdata2, messagesize);
  if(n == 0)
    return "convS2M error on write to son";
  if(write(f->sfd[1], mdata2, n) != n)
    return "mount write to son";
  if((n=read9pmsg(f->sfd[0], mdata2, messagesize))<=0)
    return "mount read from son";
  if(convM2S(mdata2, n, &rhdr) != n)
    return "convM2S error on read from son";

  return NULL;
}
 
 
void
io(void)
{
  char *err, buf[20];
  int n, pid, ctl;
  Fid *f, *ff;
  
  pid = getpid();
  if(private){
    snprint(buf, sizeof buf, "/proc/%d/ctl", pid);
    ctl = open(buf, OWRITE);
    if(ctl < 0){
      fprint(2, "can't protect ramfs\n");
    }else{
      fprint(ctl, "noswap\n");
      fprint(ctl, "private\n");
      close(ctl);
    }
  }

  bzero(errdata, Maxfdata+1);
  
  for(;;){
    /*
     * reading from a pipe or a network device
     * will give an error after a few eof reads.
     * however, we cannot tell the difference
     * between a zero-length read and an interrupt
     * on the processes writing to us,
     * so we wait for the error.
     */
    n = read9pmsg(mfd[0], mdata, messagesize);
    if(n < 0)
      error("mount read");
    if(n == 0)
      error("mount eof");
    if(convM2S(mdata, n, &thdr) == 0)
      continue;
    
    if(debug)
      fprint(2, "vrs %d:<-%F\n", pid, &thdr);
    
    if(!fcalls[thdr.type])
      err = "bad fcall type";
    else {
  /* authentication */
      switch(thdr.type){
      case Tauth:
	err = rauth(newafid(thdr.afid));
	break;
      case Tattach:
	err = rattach(newafid(thdr.afid));
	break;
      case Tversion:
	err = rversion(newafid(thdr.fid));
	break;
      case Tclunk:
	if(f=removefid(thdr.fid)){
	  ioson(f);
	  free(f);
	  if(rhdr.type == Rerror)
	    err = rhdr.ename;
	  else
	    err = NULL;
	}
	else
	  err = rclunk(newafid(thdr.fid));
	break;
	    
      default:
	if(f=getfid(thdr.fid)) {
	  if(err = ioson(f)){
	    f->afid->session_opened = 0;
	    fprint(2, "%s\n", err);
	    err = Enotexist;
	    break;
	  }  
	  if(rhdr.type == Rerror)
	    err = rhdr.ename;
	  else {
	    if(rhdr.type == Rwalk){
	      ff = emalloc(sizeof *f);
	      ff->fid = thdr.newfid;
	      ff->afid = f->afid;
	      ff->sfd[0] = f->sfd[0];
	      ff->sfd[1] = f->sfd[1];
	      ff->next = fids;
	      fids = ff;
	    }
	    err = NULL;
	  }
	}
	else
	  err = (*fcalls[thdr.type])(newafid(thdr.fid));
	break;
      }
    }
    
    if(err){
      rhdr.type = Rerror;
      rhdr.ename = err;
    } else {
      rhdr.type = thdr.type + 1;
      rhdr.fid = thdr.fid;
    }
    rhdr.tag = thdr.tag;
    if(debug)
      fprint(2, "vrs %d:->%F\n", pid, &rhdr);/**/
    n = convS2M(&rhdr, mdata, messagesize);
    if(n == 0)
      error("convS2M error on write");
    
    if(write(mfd[1], mdata, n) != n)
      error("mount write");
    bzero(errdata, strlen(errdata));
  }
}

void
error(char *s)
{
  fprint(2, "%s: %s: %r\n", argv0, s);
  threadexitsall(s);
}

void *
emalloc(ulong n)
{
  void *p;

  p = malloc(n);
  if(!p)
    error("out of memory");
  memset(p, 0, n);
  return p;
}

void *
erealloc(void *p, ulong n)
{
  p = realloc(p, n);
  if(!p)
    error("out of memory");
  return p;
}

char *
estrdup(char *q)
{
  char *p;
  int n;

  n = strlen(q)+1;
  p = malloc(n);
  if(!p)
    error("out of memory");
  memmove(p, q, n);
  return p;
}

void
usage(void)
{
  fprint(2, "usage: %s [-is] [-m mountpoint]\n", argv0);
  threadexitsall("usage");
}
