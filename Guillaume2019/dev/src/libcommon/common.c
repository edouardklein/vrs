#include <common.h>

void to_hexa(char *out, const char *in, int n){
  for(int i=0;i<n;i++){
    out[2*i]=(in[i]>>4)&15;
    out[2*i+1]=(in[i])&15;
  }
  for(int i=0;i<2*n;i++){
    out[i]+= (out[i]<10)? 48: 87;
  }
}

void from_hexa(char *out, const char *in, int n){
  char c1, c2;
  for(int i=0;i<n;i++){
    c1=(in[2*i]<90)? in[2*i]-48 : in[2*i]-87;
    c2=(in[2*i+1]<90)? in[2*i+1]-48 : in[2*i+1]-87;
    out[i] = (c1<<4)+c2;
  }
}
