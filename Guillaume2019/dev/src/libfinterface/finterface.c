#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>

#include <finterface.h>

int
dorpc_fs(char* errstring, int fd, char *verb, char *val, int len, struct rpc_message *msg)
{
  char rwbuf[6000];
  int rdcount;
  int lenpref;
  
  bzero(rwbuf, 6000);
  
  lenpref = strlen(verb)+1;
  strcat(rwbuf, verb);
  strcat(rwbuf, " ");
  strncpy(rwbuf+lenpref, val, len);

  if(write(fd, rwbuf, lenpref+len)==-1){
    strcat(errstring, strerror(errno));
    return RPC_ERROR;
  }

  bzero(rwbuf, lenpref+len);
  
  if((rdcount=read(fd, rwbuf, 4096))==-1){
    strcat(errstring, strerror(errno));
    return RPC_ERROR;
  }
  msg->length = 0;
  bzero(msg->msg, 4096);
  
  if(strncmp(rwbuf, "ok", 2)==0){
    msg->length = rdcount-3;
    if(msg->length!=-1){
      memmove(msg->msg, rwbuf+3, msg->length);
      return RPC_OK_DATA;
    }
    else
      return RPC_OK;
  }

  if(strncmp(rwbuf, "done haveai", 11)==0)
    return RPC_DONE_HAVEAI;

  if(strncmp(rwbuf, "done", 4)==0)
    return RPC_DONE;

  if(strncmp(rwbuf, "phase ", 6)==0){
    msg->length = rdcount-6;
    memmove(msg->msg, rwbuf+6, msg->length);
    return RPC_PHASE;
  }

  if(strncmp(rwbuf, "error ", 6)==0){
    msg->length = rdcount-6;
    memmove(msg->msg, rwbuf+6, msg->length);
    strcat(errstring, msg->msg);
    return RPC_ERROR;
  }

  if(strncmp(rwbuf, "protocol ", 9)==0){
    msg->length = rdcount-9;
    memmove(msg->msg, rwbuf+9, msg->length);
    return RPC_PROTOCOL;
  }
  
  if(strncmp(rwbuf, "needkey ", 8)==0){
    msg->length = rdcount-8;
    memmove(msg->msg, rwbuf+8, msg->length);
    return RPC_NEEDKEY;
  }

  if(strncmp(rwbuf, "toosmall ", 9)==0){
    msg->length = rdcount-9;
    memmove(msg->msg, rwbuf+9, msg->length);
    return RPC_TOOSMALL;
  }

  return RPC_ERROR;
}
  

static uchar*
gcarray(uchar *p, uchar *ep, uchar **s, int *np)
{
  uint n;

  if(p == NULL)
    return NULL;
  if(p+2 > ep)
    return NULL;
  n = ((p)[0]|((p)[1]<<8));
  p += 2;
  if(p+n > ep)
    return NULL;
  *s = malloc(n);
  if(*s == NULL)
    return NULL;
  memmove((*s), p, n);
  *np = n;
  p += n;
  return p;

}
  

uchar*
gstring(uchar *p, uchar *ep, char **s)
{
  uint n;

  if(p+2 > ep)
    return NULL;
  n = ((p)[0]|((p)[1]<<8));
  p += 1;
  if(p+n+1 > ep)
    return NULL;
  memmove(p, p + 1, n);
  p[n] = '\0';
  (*s) = calloc(n+1, sizeof(uchar));
  memmove((*s), p, n+1);
  p += n+1;
  return p;
}

void
auth_freeAI(struct AuthInfo *ai)
{
  if(ai == NULL)
    return;
  free(ai->cuid);
  free(ai->suid);
  free(ai->cap);
  free(ai->secret);
  free(ai);
}

uchar* convM2AI(uchar *p, int n, struct AuthInfo **aip){
  uchar *e = p+n;
  struct AuthInfo *ai;

  ai=calloc(1, sizeof(struct AuthInfo));
  if(ai==NULL)
    return NULL;

  p = gstring(p, e, &ai->cuid);
  p = gstring(p, e, &ai->suid);
  p = gstring(p, e, &ai->cap);

  p = gcarray(p, e, &ai->secret, &ai->nsecret);
  if(p == NULL)
    auth_freeAI(ai);
  else
    *aip = ai;
  return p;
}

