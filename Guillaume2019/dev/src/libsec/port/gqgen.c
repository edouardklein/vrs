#include <u.h>
#include <libc.h>
#include <mp.h>
#include <libsec.h>

GQpub*
gqpuballoc(void)
{
  GQpub *gq;

  gq = mallocz(sizeof(*gq), 1);
  if(gq == nil)
    sysfatal("gqpuballoc");
  return gq;
}

void
gqpubfree(GQpub *gq)
{
  if(gq == nil)
    return;
  mpfree(gq->cert);
  free(gq->user);
  free(gq->dom);
  mpfree(gq->rsa.ek);
  mpfree(gq->rsa.n);
  free(gq);
}


GQpriv*
gqprivalloc(void)
{
  GQpriv *gq;

  gq = mallocz(sizeof(*gq), 1);
  if(gq == nil)
    sysfatal("gqprivalloc");
  return gq;
}

void
gqprivfree(GQpriv *gq)
{
  if(gq == nil)
    return;
  mpfree(gq->pub.cert);
  free(gq->pub.user);
  free(gq->pub.dom);
  mpfree(gq->pub.rsa.ek);
  mpfree(gq->pub.rsa.n);
  mpfree(gq->privcert);
  free(gq);
}

void
gencert(char *user, int ulen, char *dom, int dlen, mpint *pubcert, mpint *n, mpint *d, int nlen){
  mpint *t1, *t2, *pad, *mpdigest;
  char gen_d = (d==nil);
  uchar digest[SHA1dlen];
  char *id;
  int idlen = 1+ulen+dlen;

  id = calloc(1+idlen, sizeof(char));
  strcat(id, user);
  strcat(id, "@");
  strcat(id, dom);
  
  sha1((uchar *) id, idlen, digest, nil); 
  
  t1 = mpnew(nlen);
  t2 = mpnew(nlen);
  if(gen_d)
    d = mpnew(nlen);
  
  mpdigest = mpnew(8*SHA1dlen);
  pad = mpnew(nlen);
  itomp(1, pad);

  betomp(digest, SHA1dlen, mpdigest);
  
  do {
    mpleft(pad, 8*SHA1dlen, pubcert);
    mpadd(pubcert, mpdigest, pubcert);
    mpadd(pubcert, mptwo, pubcert);
    mpextendedgcd(pubcert, n, t1, d, t2);
    mpadd(pad, mpone, pad);
  }  while(mpcmp(t1, mpone)!=0);

  mpfree(t1);
  mpfree(t2);
  if(gen_d)
    mpfree(d);
  mpfree(mpdigest);
  mpfree(pad);
  
  
}

void geninvertible(int nlen, mpint *e, mpint *n, mpint *d){
  mpint *t1, *t2, *nm4;
  char gen_d = (d==nil);
  
  t1 = mpnew(0);
  t2 = mpnew(0);
  if(gen_d)
    d = mpnew(0);
  nm4 = mpnew(nlen);

  mpadd(mptwo, mptwo, nm4);
  mpsub(n, nm4, nm4);

  do {
    do mprand(nlen, genrandom, e);
    while (mpcmp(e, nm4)>0);
    mpadd(e, mptwo, e);
    mpextendedgcd(e, n, t1, d, t2);
  }  while(mpcmp(t1, mpone)!=0);

  mpfree(t1);
  mpfree(t2);
  if(gen_d)
    mpfree(d);
  mpfree(nm4);
  
}

GQpriv*
gqgen(int nlen, int elen, int rounds, char* user, char* dom)
{
  RSApriv *rsa = nil;
  GQpriv *gq = gqprivalloc();
  mpint *pubcert, *privcert;
  int ulen, dlen;

  do{
    if(rsa)
      rsaprivfree(rsa);
    rsa = rsagen(nlen, elen, rounds);
  } while(mpsignif(rsa->pub.n) != nlen);
  
  gq->pub.rsa.ek = mpcopy(rsa->pub.ek);
  gq->pub.rsa.n = mpcopy(rsa->pub.n);

  ulen = strlen(user);
  dlen = strlen(dom);
  
  gq->pub.user = calloc(ulen+1,sizeof(char));
  strcpy(gq->pub.user, user);

  gq->pub.dom = calloc(dlen+1,sizeof(char));
  strcpy(gq->pub.dom, dom);

  pubcert = mpnew(0);
  privcert = mpnew(0);

  gencert(user, strlen(user), dom, strlen(dom), pubcert, rsa->pub.n, privcert, nlen);  
  rsadecrypt(rsa, privcert, privcert);

  gq->pub.cert = pubcert;
  gq->privcert = privcert;

  rsaprivfree(rsa);
  
  return gq;
}

