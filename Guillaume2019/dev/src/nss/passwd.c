#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ctype.h>
#include <errno.h>
#include <pwd.h>

#include <nss.h>


#define FACTOTUM_PATH "/tmp/mnt/public/factotum/users"

static FILE* pfd = NULL;
typedef enum nss_status nss_status_t;

nss_status_t
_nss_altfactotum_setpwent (void)
{
  printf("coucou\n");
  if((pfd = fopen(FACTOTUM_PATH, "r")) == NULL){
    //*errnop = ENOENT;
    return NSS_STATUS_UNAVAIL;
  }
  printf("coucou2\n");
  return NSS_STATUS_SUCCESS;
} 

nss_status_t
_nss_altfactotum_endpwent (void)
{
  if (pfd != NULL)
    fclose(pfd);
  return NSS_STATUS_SUCCESS;
} 

/*int count_entries (void) {
  FILE *pfd;
  char command[1000];
  char buf[100];

  bzero(command, 1000);
  strcat(command, SH_COUNTENTRIES);

  if((pfd = popen(command, "r")) == NULL){
    
  }
  }*/
nss_status_t
_nss_altfactotum_getpwent_r (struct passwd *result, char *buffer, size_t buflen, int *errnop)
{
  char buf[2*buflen];
  char *tmp;
  char *user;
  char * pass;
  char *gecos;
  char *home;
  char *shell;
  uid_t uid=-1;
  gid_t gid=-1;
  char *newline;
  int offset = 0;
  
  //char noentry = 1;
  
  static int entry = 0;

  if(fgets(buf, 2*buflen, pfd) == NULL){
    // if (entry)
    *errnop = ENOENT;
    //else
      //*errnop = SUCCESS;
    return NSS_STATUS_NOTFOUND;
  }

  entry++;

  user = buf;
  pass = strchr(user, ':');
  *(pass++)=0;
  tmp = strchr(pass, ':');
  *(tmp++)=0;
  uid = (uid_t) atoi(tmp);
  tmp = strchr(tmp, ':');
  *(tmp++)=0;
  gid = (gid_t) atoi(tmp);
  gecos = strchr(tmp, ':');
  *(gecos++)=0;
  home = strchr(gecos, ':');
  *(home++)=0;
  shell = strchr(home, ':');
  if(newline=strchr(shell,'\n'))
    *newline=0;
  
  if(strlen(gecos)+strlen(home)+strlen(shell)+strlen(user)+strlen(pass)+5/*nullcaracters*/>buflen){
    *errnop = ERANGE;
    return NSS_STATUS_TRYAGAIN;
  }

  bzero(buffer, buflen);
  strcpy(buffer, user);
  result->pw_name = buffer;
  offset+=strlen(user)+1;
  
  strcpy(buffer+offset, pass);
  result->pw_passwd = buffer+offset;
  offset+=strlen(pass)+1;

  strcpy(buffer+offset, gecos);
  result->pw_gecos = buffer+offset;
  offset+=strlen(gecos)+1;

  strcpy(buffer+offset, home);
  result->pw_dir = buffer+offset;
  offset+=strlen(home)+1;

  strcpy(buffer+offset, shell);
  result->pw_shell = buffer+offset;

  result->pw_uid = uid;
  result->pw_gid = gid;

  return NSS_STATUS_SUCCESS;
  
} 

nss_status_t
_nss_altfactotum_getpwnam_r (const char* name, struct passwd *result, char *buffer, size_t buflen, int *errnop)
{

  char buf[2*buflen];
  char *tmp;
  char *user;
  char * pass;
  char *gecos;
  char *home;
  char *shell;
  uid_t uid=-1;
  gid_t gid=-1;
  char *newline;
  int offset = 0;
  
  char noentry = 1;
  
  do{
    if(fgets(buf, 2*buflen, pfd) == NULL){
      //if(noentry)
      //	*errnop = SUCCESS;
      //else
	*errnop = ENOENT;
      return NSS_STATUS_NOTFOUND;
    }
    noentry = 0;
  } while(strncmp(buf, name, sizeof(name)));
	  
  user = buf;
  pass = strchr(user, ':');
  *(pass++)=0;
  tmp = strchr(pass, ':');
  *(tmp++)=0;
  uid = (uid_t) atoi(tmp);
  tmp = strchr(tmp, ':');
  *(tmp++)=0;
  gid = (gid_t) atoi(tmp);
  gecos = strchr(tmp, ':');
  *(gecos++)=0;
  home = strchr(gecos, ':');
  *(home++)=0;
  shell = strchr(home, ':');
  if(newline=strchr(shell,'\n'))
    *newline=0;
  
  if(strlen(gecos)+strlen(home)+strlen(shell)+strlen(user)+strlen(pass)+5/*nullcaracters*/>buflen){
    *errnop = ERANGE;
    return NSS_STATUS_TRYAGAIN;
  }

  bzero(buffer, buflen);
  strcpy(buffer, user);
  result->pw_name = buffer;
  offset+=strlen(user)+1;
  
  strcpy(buffer+offset, pass);
  result->pw_passwd = buffer+offset;
  offset+=strlen(pass)+1;

  strcpy(buffer+offset, gecos);
  result->pw_gecos = buffer+offset;
  offset+=strlen(gecos)+1;

  strcpy(buffer+offset, home);
  result->pw_dir = buffer+offset;
  offset+=strlen(home)+1;

  strcpy(buffer+offset, shell);
  result->pw_shell = buffer+offset;

  result->pw_uid = uid;
  result->pw_gid = gid;
  
  return NSS_STATUS_SUCCESS;
	  
}

nss_status_t
_nss_factotum_getpwuid_r (uid_t uid, struct passwd *result, char *buffer, size_t buflen, int *errnop)
{
  char buf[2*buflen];
  char *tmp;
  char *user;
  char * pass;
  char *gecos;
  char *home;
  char *shell;
  gid_t gid=-1;
  char *newline;
  int offset = 0;
  
  char noentry = 1;
  
  do{
    if(fgets(buf, 2*buflen, pfd) == NULL){
      //      if(noentry)
	// *errnop = SUCCESS;
      //else
	*errnop = ENOENT;
      return NSS_STATUS_NOTFOUND;
    }
    noentry = 0;
  } while(((uid_t) atoi(strchr(buf, ':')+1))!=uid);
	  
  user = buf;
  pass = strchr(user, ':');
  *(pass++)=0;
  tmp = strchr(pass, ':');
  *(tmp++)=0;
  uid = (uid_t) atoi(tmp);
  tmp = strchr(tmp, ':');
  *(tmp++)=0;
  gid = (gid_t) atoi(tmp);
  gecos = strchr(tmp, ':');
  *(gecos++)=0;
  home = strchr(gecos, ':');
  *(home++)=0;
  shell = strchr(home, ':');
  if(newline=strchr(shell,'\n'))
    *newline=0;
  
  if(strlen(gecos)+strlen(home)+strlen(shell)+strlen(user)+strlen(pass)+5/*nullcaracters*/>buflen){
    *errnop = ERANGE;
    return NSS_STATUS_TRYAGAIN;
  }

  bzero(buffer, buflen);
  strcpy(buffer, user);
  result->pw_name = buffer;
  offset+=strlen(user)+1;
  
  strcpy(buffer+offset, pass);
  result->pw_passwd = buffer+offset;
  offset+=strlen(pass)+1;

  strcpy(buffer+offset, gecos);
  result->pw_gecos = buffer+offset;
  offset+=strlen(gecos)+1;

  strcpy(buffer+offset, home);
  result->pw_dir = buffer+offset;
  offset+=strlen(home)+1;

  strcpy(buffer+offset, shell);
  result->pw_shell = buffer+offset;

  result->pw_uid = uid;
  result->pw_gid = gid;

  return NSS_STATUS_SUCCESS;
} 
