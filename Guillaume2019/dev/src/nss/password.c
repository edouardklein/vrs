#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ctype.h>
#include <errno.h>
#include <pwd.h>

#include <nss.h>


#define FACTOTUM_PATH "/tmp/mnt/factotum/ctl"
#define SH_FINDUSER1 "cat " FACTOTUM_PATH " | grep user="
#define SH_FINDUSER2 " | grep -E 'uid=[0-9]+' | grep -E 'gid=[0-9]+' | sed -n 1p | grep -E -o \"(uid|gid)=[0-9]+|(home|shell)=[a-zA-Z/0-9.]*|info='[^']*'\""

#define SH_COUNTENTRIES " cat " FACTOTUM_PATH " | grep -E 'user=\\w+' | grep -E 'uid=[0-9]+' | grep -E 'gid=[0-9]+' | wc -l"
#define SH_FINDUID1 "cat " FACTOTUM_PATH " | grep -E user=\\w+' | grep uid="
#define SH_FINDUID2 " | grep -E 'gid=[0-9]+' | sed -n 1p | grep -E -o \"user=\\w+|gid=[0-9]+|(home|shell)=[a-zA-Z/0-9.]*|info='[^']*'\""

#define SH_FINDENTRY1 "cat " FACTOTUM_PATH " | grep -E 'user=\\w+' | grep -E 'uid=[0-9]+' | grep -E 'gid=[0-9]+' | sed -n "
#define SH_FINDENTRY2 "p | grep -E -o \"user=\\w+|(uid|gid)=[0-9]+|(home|shell)=[a-zA-Z/0-9.]*|info='[^']*'\"" 
/* #define SH_EX " | grep -E \'" */
/* #define SH_EXNUM "=[0-9]+\'" */
/* #define SH_EXWORD "=\\w+\'" */
/* #define SH_ISOLATE " | cut -d= -f2" */
/* #define SH_RESTRICT " | grep -E -o \'" */
/* #define SH_COMMAND3 "=\\w+\' | cut -d= -f2" */

typedef enum nss_status nss_status_t;

nss_status_t
_nss_factotum_setpwent (void)
{
  return NSS_STATUS_SUCCESS;
} 

nss_status_t
_nss_factotum_endpwent (void)
{
  return NSS_STATUS_SUCCESS;
} 

/*int count_entries (void) {
  FILE *pfd;
  char command[1000];
  char buf[100];

  bzero(command, 1000);
  strcat(command, SH_COUNTENTRIES);

  if((pfd = popen(command, "r")) == NULL){
    
  }
  }*/
nss_status_t
_nss_factotum_getpwent_r (struct passwd *result, char *buffer, size_t buflen, int *errnop)
{
  FILE *pfd;
  char command[1000];
  char tmpbuf[5*buflen];
  char userbuf[buflen];
  char infobuf[buflen];
  char homebuf[buflen];
  char shellbuf[buflen];
  int offset=0;
  char unsetuser = 1;
  char unsetuid = 1;
  char unsetgid = 1;
  uid_t user=-1;
  gid_t group=-1;
  char *apostrophy;
  char *newline;

  //char noentry = 1;
  
  static int entry = 1;
  
  bzero(infobuf, buflen);
  bzero(homebuf, buflen);
  bzero(shellbuf, buflen);
  bzero(userbuf, buflen);
  
  bzero(command, 1000);
  strcat(command, SH_FINDENTRY1);
  sprintf(command+strlen(command), "%d", entry);
  strcat(command, SH_FINDENTRY2);
  
  
  if((pfd = popen(command, "r")) == NULL){
    *errnop = ENOENT;
    return NSS_STATUS_UNAVAIL;
  }
  
  bzero(tmpbuf, 5*buflen);
  for(;fgets(tmpbuf, 5*buflen, pfd)!=NULL;bzero(tmpbuf,strlen(tmpbuf))){
    //noentry = 0;
    if(strncmp(tmpbuf, "uid=", 4)==0){
      user = (uid_t) atoi(tmpbuf+4);
      unsetuid = 0;
      continue;
    }
    if(strncmp(tmpbuf, "user=", 5)==0){
      unsetuser = 0;
      if(strlen(tmpbuf+5)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(userbuf, tmpbuf+5);
      if((newline = strchr(userbuf, '\n')) != NULL)
	bzero(newline, strlen(newline));
    }
    if(strncmp(tmpbuf, "gid=", 4)==0){
      group = (gid_t) atoi(tmpbuf+4);
      unsetgid = 0;
      continue;
    }
    if(strncmp(tmpbuf, "info=", 5)==0){
      if(strlen(tmpbuf+6)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(infobuf, tmpbuf+6); //6 and not 5: do not copy first simple quote
      if((apostrophy = strrchr(infobuf, '\'')) != NULL)
	bzero(apostrophy, strlen(apostrophy));
      continue;
    }
    if(strncmp(tmpbuf, "home=", 5)==0){
      if(strlen(tmpbuf+5)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(homebuf, tmpbuf+5);
      if((newline = strchr(homebuf, '\n')) != NULL)
	bzero(newline, strlen(newline));
      continue;
    }
    if(strncmp(tmpbuf, "shell=", 6)==0){
      if(strlen(tmpbuf+6)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(shellbuf, tmpbuf+6);
      if((newline = strchr(shellbuf, '\n')) != NULL)
	bzero(newline, strlen(newline));
      continue;
    }

    //deadcode
    return NSS_STATUS_UNAVAIL;
  }

  pclose(pfd);

  
  if(unsetuser || unsetuid || unsetgid){
    *errnop = ENOENT;
    return NSS_STATUS_NOTFOUND;
  }
  
  if(strlen(infobuf)+strlen(homebuf)+strlen(shellbuf)+strlen(userbuf)+4/*nullcaracters*/+2/*passwd*/>buflen){
    *errnop = ERANGE;
    return NSS_STATUS_TRYAGAIN;
  }

  bzero(buffer, buflen);
  strcpy(buffer, userbuf);
  result->pw_name = buffer;
  offset+=strlen(userbuf)+1;
  
  strcpy(buffer+offset, "x");
  result->pw_passwd = buffer+offset;
  offset+=2;

  strcpy(buffer+offset, infobuf);
  result->pw_gecos = buffer+offset;
  offset+=strlen(infobuf)+1;

  strcpy(buffer+offset, homebuf);
  result->pw_dir = buffer+offset;
  offset+=strlen(homebuf)+1;

  strcpy(buffer+offset, shellbuf);
  result->pw_shell = buffer+offset;

  result->pw_uid = user;
  result->pw_gid = group;

  entry++;
  
  return NSS_STATUS_SUCCESS;

} 

nss_status_t
_nss_factotum_getpwnam_r (const char* name, struct passwd *result, char *buffer, size_t buflen, int *errnop)
{
  FILE *pfd;
  char command[1000+strlen(name)];
  char tmpbuf[4*buflen];
  char infobuf[buflen];
  char homebuf[buflen];
  char shellbuf[buflen];
  int offset=0;
  char unsetuid = 1;
  char unsetgid = 1;
  uid_t user=-1;
  gid_t group=-1;
  char *apostrophy;
  char *newline;

  bzero(infobuf, buflen);
  bzero(homebuf, buflen);
  bzero(shellbuf, buflen);
  
  bzero(command, 1000);
  strcat(command, SH_FINDUSER1);
  strcat(command, name);
  strcat(command, SH_FINDUSER2);
  
  
  if((pfd = popen(command, "r")) == NULL){
    *errnop = ENOENT;
    return NSS_STATUS_UNAVAIL;
  }
  
  bzero(tmpbuf, 4*buflen);
  for(;fgets(tmpbuf, 4*buflen, pfd)!=NULL;bzero(tmpbuf,strlen(tmpbuf))){
    if(strncmp(tmpbuf, "uid=", 4)==0){
      user = (uid_t) atoi(tmpbuf+4);
      unsetuid = 0;
      continue;
    }
    if(strncmp(tmpbuf, "gid=", 4)==0){
      group = (gid_t) atoi(tmpbuf+4);
      unsetgid = 0;
      continue;
    }
    if(strncmp(tmpbuf, "info=", 5)==0){
      if(strlen(tmpbuf+6)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(infobuf, tmpbuf+6); //6 and not 5: do not copy simple quote
      if((apostrophy = strrchr(infobuf, '\'')) != NULL)
	bzero(apostrophy, strlen(apostrophy));
      continue;
    }
    if(strncmp(tmpbuf, "home=", 5)==0){
      if(strlen(tmpbuf+5)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(homebuf, tmpbuf+5);
      if((newline = strchr(homebuf, '\n')) != NULL)
	bzero(newline, strlen(newline));
      continue;
    }
    if(strncmp(tmpbuf, "shell=", 6)==0){
      if(strlen(tmpbuf+6)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(shellbuf, tmpbuf+6);
      if((newline = strchr(shellbuf, '\n')) != NULL)
	bzero(newline, strlen(newline));
      continue;
    }

    //deadcode
    return NSS_STATUS_UNAVAIL;
  }

  pclose(pfd);
  if(unsetuid || unsetgid){
    *errnop = ENOENT;
    return NSS_STATUS_NOTFOUND;
  }
  
  if(strlen(infobuf)+strlen(homebuf)+strlen(shellbuf)+strlen(name)+4/*nullcaracters*/+2/*passwd*/>buflen){
    *errnop = ERANGE;
    return NSS_STATUS_TRYAGAIN;
  }

  bzero(buffer, buflen);
  strcpy(buffer, name);
  result->pw_name = buffer;
  offset+=strlen(name)+1;
  
  strcpy(buffer+offset, "x");
  result->pw_passwd = buffer+offset;
  offset+=2;

  strcpy(buffer+offset, infobuf);
  result->pw_gecos = buffer+offset;
  offset+=strlen(infobuf)+1;

  strcpy(buffer+offset, homebuf);
  result->pw_dir = buffer+offset;
  offset+=strlen(homebuf)+1;

  strcpy(buffer+offset, shellbuf);
  result->pw_shell = buffer+offset;

  result->pw_uid = user;
  result->pw_gid = group;
  
  return NSS_STATUS_SUCCESS;
}

nss_status_t
_nss_factotum_getpwuid_r (uid_t uid, struct passwd *result, char *buffer, size_t buflen, int *errnop)
{
  FILE *pfd;
  char command[1000];
  char tmpbuf[5*buflen];
  char userbuf[buflen];
  char infobuf[buflen];
  char homebuf[buflen];
  char shellbuf[buflen];
  int offset=0;
  char unsetuser = 1;
  char unsetgid = 1;
  gid_t group=-1;
  char *apostrophy;
  char *newline;

  
  bzero(infobuf, buflen);
  bzero(homebuf, buflen);
  bzero(shellbuf, buflen);
  bzero(userbuf, buflen);
  
  bzero(command, 1000);
  strcat(command, SH_FINDUID1);
  sprintf(command+strlen(command), "%d", uid);
  strcat(command, SH_FINDUID2);
  
  
  if((pfd = popen(command, "r")) == NULL){
    *errnop = ENOENT;
    return NSS_STATUS_UNAVAIL;
  }
  
  bzero(tmpbuf, 5*buflen);
  for(;fgets(tmpbuf, 5*buflen, pfd)!=NULL;bzero(tmpbuf,strlen(tmpbuf))){
    if(strncmp(tmpbuf, "user=", 5)==0){
      unsetuser = 0;
      if(strlen(tmpbuf+5)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(userbuf, tmpbuf+5);
      if((newline = strchr(userbuf, '\n')) != NULL)
	bzero(newline, strlen(newline));
    }
    if(strncmp(tmpbuf, "gid=", 4)==0){
      group = (gid_t) atoi(tmpbuf+4);
      unsetgid = 0;
      continue;
    }
    if(strncmp(tmpbuf, "info=", 5)==0){
      if(strlen(tmpbuf+6)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(infobuf, tmpbuf+6); //6 and not 5: do not copy first simple quote
      if((apostrophy = strrchr(infobuf, '\'')) != NULL)
	bzero(apostrophy, strlen(apostrophy));
      continue;
    }
    if(strncmp(tmpbuf, "home=", 5)==0){
      if(strlen(tmpbuf+5)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(homebuf, tmpbuf+5);
      if((newline = strchr(homebuf, '\n')) != NULL)
	bzero(newline, strlen(newline));
      continue;
    }
    if(strncmp(tmpbuf, "shell=", 6)==0){
      if(strlen(tmpbuf+6)>=buflen){
	pclose(pfd);
	*errnop = ERANGE;
	return NSS_STATUS_TRYAGAIN;
      }
      strcpy(shellbuf, tmpbuf+6);
      if((newline = strchr(shellbuf, '\n')) != NULL)
	bzero(newline, strlen(newline));
      continue;
    }

    //deadcode
    return NSS_STATUS_UNAVAIL;
  }

  pclose(pfd);
  
  if(unsetuser || unsetgid){
    *errnop = ENOENT;
    return NSS_STATUS_NOTFOUND;
  }
  
  if(strlen(infobuf)+strlen(homebuf)+strlen(shellbuf)+strlen(userbuf)+4/*nullcaracters*/+2/*passwd*/>buflen){
    *errnop = ERANGE;
    return NSS_STATUS_TRYAGAIN;
  }

  bzero(buffer, buflen);
  strcpy(buffer, userbuf);
  result->pw_name = buffer;
  offset+=strlen(userbuf)+1;
  
  strcpy(buffer+offset, "x");
  result->pw_passwd = buffer+offset;
  offset+=2;

  strcpy(buffer+offset, infobuf);
  result->pw_gecos = buffer+offset;
  offset+=strlen(infobuf)+1;

  strcpy(buffer+offset, homebuf);
  result->pw_dir = buffer+offset;
  offset+=strlen(homebuf)+1;

  strcpy(buffer+offset, shellbuf);
  result->pw_shell = buffer+offset;

  result->pw_uid = uid;
  result->pw_gid = group;
  
  return NSS_STATUS_SUCCESS;

} 
