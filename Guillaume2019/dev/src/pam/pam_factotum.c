//#include <stdlib.h>

//libpam initialisation
#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD

#include <security/pam_modules.h>

//to interface the application
#include <security/pam_appl.h>


#include <string.h>


#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

//interface with factotum
#include <finterface.h>
#include <common.h>
#include <assert.h>

int
dorpc_pam(pam_handle_t* pamh, int fd, char *verb, char *val, int len, struct rpc_message *msg)
{
  char errstring[256];
  int retval;
  bzero(errstring, 256);
  strcat(errstring, "ERROR=");
  
  if((retval=dorpc_fs(errstring, fd, verb, val, len, msg)) == RPC_ERROR)
    pam_putenv(pamh, errstring);

  return retval;
}
  
//authenticates the client, shall be called at first
PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv) {

  char param[100];
  char *buf = NULL, *msgbuf = NULL, *aibuf = NULL;
  struct pam_message **msg = NULL, **msgread = NULL;
  struct pam_response **resp;
  struct pam_conv* conversation; 
  struct AuthInfo *ai = NULL;
  int retconv;
  int retrpc = RPC_OK;
  int n, m;
  int ret;

  char* service;
  char* proto;
  char* ruser;
  char* rdom;
  char* useratdom;
  
  int mountret, fd;
  char namespacebuf[100];
  //char pathmnt[100];
  char path[100];
  char pathrpc[100];
  char errstring[256];
  //mode_t oldumask;
  struct stat st = {0};
  struct rpc_message rpc;

  assert(argc == 2);
  bzero(errstring, 256);
  strcat(errstring, "ERROR=");

  if (pam_get_item(pamh, PAM_CONV, (const void **)(&conversation))!=PAM_SUCCESS){
    pam_putenv(pamh, "ERROR=unable to load conversation");
    return PAM_ABORT;
  }

  if(argv[1]==NULL)
    pam_putenv(pamh, "ERROR=no mount point specified");

  
  bzero(path, 100);
  strncat(path, argv[1], 90);
  
  bzero(pathrpc, 100);
  strcat(strcat(pathrpc, path), "/rpc");
  if((fd = open(pathrpc, O_RDWR))==-1){
    strcat(errstring, "couldn't open channel to factotum: ");
    pam_putenv(pamh, strcat(errstring, strerror(errno)));
    return PAM_ABORT;
  }

  if(pam_get_item(pamh, PAM_RUSER, (const void **)&ruser)!=PAM_SUCCESS || pam_get_item(pamh, PAM_RHOST, (const void**)&rdom)!=PAM_SUCCESS){
    pam_putenv(pamh, "ERROR= unable to retrieve requesting user and host");
    return PAM_ABORT;
  }

  pam_get_item(pamh, PAM_SERVICE, (const void**)&service);
  if (strncmp(service, "factotum", 8))
    proto="p9any";
  else
    proto="login";
  
  memmove(param, "role=server proto=", 19);
  memmove(param+strlen(param), proto, strlen(proto)+1); 
  memmove(param+strlen(param), " user=", 7);
  memmove(param+strlen(param), ruser, strlen(ruser)+1);
  if(rdom){
    memmove(param+strlen(param), " dom=", 6);
    memmove(param+strlen(param), rdom, strlen(rdom)+1);
  }
  
  if((retrpc=dorpc_pam(pamh, fd, "start", param, strlen(param), &rpc))!=RPC_OK){
    strcat(errstring, "vrs: couldn't start authentication via factotum: ");
    strcat(errstring, pam_getenv(pamh, "ERROR"));
    pam_putenv(pamh, errstring);
    close(fd);
    return PAM_ABORT;
  }

  resp = malloc(sizeof(struct pam_response *));
  if(resp == NULL){
    pam_putenv(pamh, "ERROR=no memory");
    close(fd);
    return PAM_ABORT;
  }

  msgread = malloc(sizeof(struct pam_message *));
  if (msgread == NULL){
    pam_putenv(pamh, "ERROR=no memory");
    free(resp);
    close(fd);
    return PAM_ABORT;
  }
  *msgread = NULL;
  *msgread = malloc(sizeof(struct pam_message));
  if(*msgread == NULL){
    close(fd);
    free(resp);
    free(msgread);
    pam_putenv(pamh, "ERROR=no memory");
    return PAM_ABORT;
  }

  msgread[0]->msg_style = PAM_PROMPT_ECHO_OFF;
  msgread[0]->msg = NULL;
  
  msg = malloc(sizeof(struct pam_message *));
  if(msg ==NULL){
    close(fd);
    free(resp);
    free(*msgread); free(msgread);
    pam_putenv(pamh, "ERROR=no memory");
    return PAM_ABORT;
  }
  *msg = NULL;
  *msg = malloc(sizeof(struct pam_message));
  if(*msg == NULL){
    free(*msgread); free(msgread);
    free(resp);
    free(msg);
    close(fd);
    pam_putenv(pamh, "ERROR=no memory");
    return PAM_ABORT;
  }  
  msg[0]->msg_style = PAM_TEXT_INFO;
  msgbuf = calloc(4096, sizeof(char));
  msg[0]->msg = msgbuf;

  buf = calloc(4096, sizeof(char));

  
  if(buf == NULL || msgbuf == NULL){
    free(buf); free(msgbuf);
    free(*msg); free(msg);
    free(*msgread); free(msgread);
    close(fd);
    pam_putenv(pamh, "ERROR=no memory");
    return PAM_ABORT;
  }

  while(retrpc!=RPC_DONE && retrpc!=RPC_DONE_HAVEAI){
    switch(retrpc = dorpc_pam(pamh, fd, "read", NULL, 0, &rpc)){
    case RPC_DONE:
    case RPC_DONE_HAVEAI:
      break;
    case RPC_OK_DATA:
      to_hexa(msgbuf, rpc.msg, rpc.length);
      
      retconv = conversation->conv(1, (const struct pam_message **) msg,
				  resp, conversation->appdata_ptr);
      bzero(msgbuf, 2*rpc.length);
      free(((*resp)[0]).resp);
      free(*resp);	
      if(retconv != PAM_SUCCESS){
	free(resp);
	strcat(errstring, "auth_proxy write");
	pam_putenv(pamh, errstring);
	free(buf); free(msgbuf);
	free(*msg); free(msg);
	free(*msgread); free(msgread);
	close(fd); 
	return PAM_ABORT;
	}
    case RPC_OK:  
      break;
    case RPC_PHASE:
      n = 0;
      memset(buf, 0, 4096);
      while((ret = dorpc_pam(pamh, fd, "write", buf, n, &rpc)) == RPC_TOOSMALL){
	if(atoi(rpc.msg) > 4096)
	  break;
   	retconv = conversation->conv(1, (const struct pam_message **) msgread, resp, conversation->appdata_ptr);
	if(retconv != PAM_SUCCESS){
	  strcat(errstring, "auth_proxy read");
	  pam_putenv(pamh, errstring);
	  free(buf); free(msgbuf);
	  free(*msg); free(msg);
	  free(*msgread); free(msgread);
	  free(((*resp)[0]).resp);
	  free(*resp);
	  free(resp);
	  close(fd); 
	  return PAM_ABORT;
	}
	m = strlen(((*resp)[0]).resp)/2;
        if(m>=0){
	  from_hexa(buf+n, ((*resp)[0]).resp, m);
	  n += m;
	}
	free(((*resp)[0]).resp);
	free(*resp);
	if(m <= 0){
	  if(m == 0)
	    strcat(errstring, "auth_proxy short read");
	  else
	    strcat(errstring, "auth_proxy read");
	  pam_putenv(pamh, errstring);
	  free(buf); free(msgbuf);
	  free(*msg); free(msg);
	  free(resp);
	  free(*msgread); free(msgread);
	  close(fd); 
	  return PAM_ABORT;
	}
      }
      if(ret != RPC_OK && ret != RPC_OK_DATA){
	strcat(strcat(errstring, "auth_proxy rpc write: "), pam_getenv(pamh, "ERROR"));
	goto ErrorMsg;  
      }
      break;
    default:
      strcat(strcat(errstring, "auth_proxy rpc: "), pam_getenv(pamh, "ERROR"));
      goto ErrorMsg;
    }
  }

  if((dorpc_pam(pamh, fd, "authinfo", NULL, 0, &rpc)) != RPC_OK_DATA){
    strcat(strcat(errstring, "cannot obtain authinfo from factotum: "), pam_getenv(pamh, "ERROR"));
    goto ErrorMsg;
  }
  
  if(convM2AI((uchar*)rpc.msg, rpc.length, &ai) == NULL){
    strcat(errstring, "bad auth info from factotum");
    goto ErrorMsg;
  }
 
  aibuf = calloc(10000, sizeof(char));
  if(aibuf == NULL){
    free(*msg); free(msg);
    free(resp);
    free(buf); free(msgbuf);
    free(*msgread); free(msgread);
    auth_freeAI(ai);
    close(fd); 
    pam_putenv(pamh, "ERROR=no memory");
    return PAM_ABORT;
  }
  sprintf(aibuf, "%s:%s:%s:%d:", ai->cuid, ai->suid, ai->cap, ai->nsecret);
  to_hexa(aibuf+strlen(aibuf), (char*) ai->secret, ai->nsecret);
  if(pam_set_item(pamh, PAM_AUTHTOK, (void*) aibuf)!=PAM_SUCCESS){
    auth_freeAI(ai);
    strcat(errstring, "couldnt store authtok");
    goto ErrorMsg;
  }

  if(rdom){
    useratdom = calloc(2+strlen(ruser)+strlen(rdom), sizeof(char));
    memmove(useratdom, ruser, strlen(ruser));
    if(strlen(rdom)){
      memmove(useratdom+strlen(useratdom), "@", 1);
      memmove(useratdom+strlen(useratdom), rdom, strlen(rdom));
    }
  }
  else
    useratdom = ruser;
      
  if(pam_set_item(pamh, PAM_USER, (void*) useratdom)!=PAM_SUCCESS){
    auth_freeAI(ai);
    strcat(errstring, "couldnt store authenticated user name");
    if(rdom)
      free(useratdom);
    goto ErrorMsg;
  }

  free(useratdom);
  free(buf); free(msgbuf); free(aibuf);
  free(*msg); free(msg);
  free(*msgread); free(msgread);
  free(resp);
  auth_freeAI(ai);
  close(fd);
  return PAM_SUCCESS;
  
 ErrorMsg:
  pam_putenv(pamh, errstring);
  (*msg)->msg = errstring+6;
  (*msg)->msg_style = PAM_ERROR_MSG;
  conversation->conv(1, (const struct pam_message **) msg, resp, conversation->appdata_ptr);
  free(((*resp)[0]).resp);
  free(*resp);
  free(*msg); free(msg);
  free(resp);
  free(buf); free(msgbuf);
  free(*msgread); free(msgread);
  close(fd); 
  return PAM_AUTH_ERR;  
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv) {

  return PAM_SUCCESS;
  //  return PAM_SERVICE_ERR;
}

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const char **argv) {

  return PAM_SUCCESS;
  //return PAM_SERVICE_ERR;
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t *pamh, int flags, int argc, const char **argv) {

  return PAM_SERVICE_ERR;
}

PAM_EXTERN int pam_sm_close_session(pam_handle_t *pamh, int flags, int argc, const char **argv) {

  return PAM_SERVICE_ERR;
}

PAM_EXTERN int pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc, const char **argv) {

  return PAM_SERVICE_ERR;
}

