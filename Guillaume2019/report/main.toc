\babel@toc {french}{}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{5}
\contentsline {chapter}{\numberline {1}Etude du syst\IeC {\`e}me Plan 9 from Bell Labs}{6}
\contentsline {section}{\numberline {1.1}Le successeur de UNIX}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{8}
\contentsline {paragraph}{}{8}
\contentsline {section}{\numberline {1.2}La refonte totale des IPC : ``Everything is a File''}{8}
\contentsline {paragraph}{}{8}
\contentsline {paragraph}{}{8}
\contentsline {paragraph}{}{8}
\contentsline {paragraph}{}{8}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{}{9}
\contentsline {section}{\numberline {1.3}Une gestion centralis\IeC {\'e}e de l'authentification}{9}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{}{9}
\contentsline {subsection}{\numberline {1.3.1}Principes g\IeC {\'e}n\IeC {\'e}raux}{10}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{}{10}
\contentsline {subsection}{\numberline {1.3.2}Comparaison avec PAM pour Linux}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{11}
\contentsline {section}{\numberline {1.4}Portages pr\IeC {\'e}c\IeC {\'e}dents de Plan 9 sur Linux}{11}
\contentsline {paragraph}{}{11}
\contentsline {subsection}{\numberline {1.4.1}Portage du protocole 9P : v9fs}{11}
\contentsline {paragraph}{}{11}
\contentsline {subsection}{\numberline {1.4.2}``Plan 9 from User Space'' : la communaut\IeC {\'e} 9fans}{11}
\contentsline {paragraph}{}{11}
\contentsline {subsection}{\numberline {1.4.3}Tentatives de portage des serveurs et de l'authentification sur Linux}{12}
\contentsline {paragraph}{}{12}
\contentsline {paragraph}{}{12}
\contentsline {chapter}{\numberline {2}Une nouvelle gestion des utilisateurs sous Linux}{13}
\contentsline {section}{\numberline {2.1}But recherch\IeC {\'e} et cas d'usage typique}{13}
\contentsline {paragraph}{}{13}
\contentsline {paragraph}{}{13}
\contentsline {paragraph}{}{13}
\contentsline {section}{\numberline {2.2}Assemblage des outils plan9port existants pour obtenir un client compatible Plan 9}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {section}{\numberline {2.3}Impl\IeC {\'e}mentation d'un serveur 9P capable d'authentifier et \\d\IeC {\'e}multiplexer les connexions clientes}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{20}
\contentsline {paragraph}{}{20}
\contentsline {section}{\numberline {2.4}Portage du m\IeC {\'e}canisme d'authentification Plan 9}{20}
\contentsline {subsection}{\numberline {2.4.1}Nouvelle gestion de l'identit\IeC {\'e}}{20}
\contentsline {paragraph}{}{20}
\contentsline {paragraph}{}{22}
\contentsline {paragraph}{}{22}
\contentsline {subsection}{\numberline {2.4.2}Impl\IeC {\'e}mentation d'un m\IeC {\'e}canisme d'authentification par preuve \IeC {\`a} divulgation nulle de connaissance}{22}
\contentsline {paragraph}{}{22}
\contentsline {paragraph}{}{22}
\contentsline {paragraph}{}{22}
\contentsline {paragraph}{}{22}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{}{25}
\contentsline {section}{\numberline {2.5}Cr\IeC {\'e}ation d'un serveur d'authentification compatible avec Factotum}{25}
\contentsline {subsection}{\numberline {2.5.1}Authentification avec PAM}{25}
\contentsline {paragraph}{}{25}
\contentsline {paragraph}{}{25}
\contentsline {paragraph}{}{26}
\contentsline {paragraph}{}{26}
\contentsline {paragraph}{}{26}
\contentsline {subsection}{\numberline {2.5.2}Cr\IeC {\'e}ation de processus fils et d\IeC {\'e}multiplexage des connexions}{26}
\contentsline {paragraph}{}{26}
\contentsline {paragraph}{}{26}
\contentsline {paragraph}{}{28}
\contentsline {paragraph}{}{28}
\contentsline {paragraph}{}{28}
\contentsline {paragraph}{}{29}
\contentsline {paragraph}{}{29}
\contentsline {paragraph}{}{29}
\contentsline {chapter}{\numberline {3}Analyse de la solution propos\IeC {\'e}e}{31}
\contentsline {section}{\numberline {3.1}Propri\IeC {\'e}t\IeC {\'e}s mises en oeuvre}{31}
\contentsline {subsection}{\numberline {3.1.1}L'agnostisme de s\IeC {\'e}curit\IeC {\'e} (Security agnosticism)}{31}
\contentsline {paragraph}{}{31}
\contentsline {subsection}{\numberline {3.1.2}La transparence r\IeC {\'e}seau (Network transparency)}{31}
\contentsline {paragraph}{}{31}
\contentsline {paragraph}{}{31}
\contentsline {paragraph}{}{31}
\contentsline {subsection}{\numberline {3.1.3}Modularit\IeC {\'e}, simplicit\IeC {\'e} et robustesse}{32}
\contentsline {paragraph}{}{32}
\contentsline {paragraph}{}{32}
\contentsline {paragraph}{}{32}
\contentsline {paragraph}{}{32}
\contentsline {paragraph}{}{32}
\contentsline {paragraph}{}{32}
\contentsline {section}{\numberline {3.2}Future works}{32}
\contentsline {subsection}{\numberline {3.2.1}Limites identifi\IeC {\'e}es}{32}
\contentsline {paragraph}{}{32}
\contentsline {paragraph}{}{33}
\contentsline {paragraph}{}{33}
\contentsline {paragraph}{}{33}
\contentsline {paragraph}{}{33}
\contentsline {subsection}{\numberline {3.2.2}Axes d'\IeC {\'e}volution souhait\IeC {\'e}s pour la suite du projet}{33}
\contentsline {paragraph}{}{33}
\contentsline {paragraph}{}{33}
\contentsline {paragraph}{}{33}
\contentsline {paragraph}{}{33}
\contentsline {chapter}{\numberline {A}Les Laboratoires Bell}{35}
\contentsline {paragraph}{}{35}
\contentsline {paragraph}{}{35}
\contentsline {paragraph}{}{35}
\contentsline {chapter}{\numberline {B}La preuve \IeC {\`a} divulgation nulle de connaissance de Guillou-Quisquater (GQ)}{36}
\contentsline {paragraph}{}{36}
\contentsline {paragraph}{}{36}
\contentsline {paragraph}{}{36}
\contentsline {chapter}{\numberline {C}Le C3N en d\IeC {\'e}tails}{37}
\contentsline {paragraph}{}{37}
\contentsline {paragraph}{}{37}
\contentsline {paragraph}{}{37}
\contentsline {paragraph}{}{38}
\contentsline {paragraph}{}{38}
