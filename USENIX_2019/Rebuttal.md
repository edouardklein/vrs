Review #113A Rebuttal
===========================================================================



> Overall merit
> -------------
> 1. Reject

> Reviewer expertise
> ------------------
> 2. Some familiarity

> Reviewer confidence
> -------------------
> 3. High

We understand reviewers are unpaid volunteers. We do not wish to vilify reviewer A,
who took the time to read our paper and write their commentary.

Still, as we will show, their self confidence is unrelated to their actual understanding
of our paper. This led to an unfair assessment of the quality and interest of our work. We wrote 
the following not to pick a fight with reviewer A, but to prove to knowledgeable readers that A's
review is indeed mistaken, which is the basis for our request for the Program Committee to ignore 
A's review and if possible seek a third party assessment from a knowledgeable reviewer.

What we believe may have happen is: what A thinks they know about Plan 9 is slightly wrong,
which in turn may have caused A to skim the paper instead of closely following our step by step 
explanations like the other reviewers did. It is hard to blame A for this, as they are an unpaid 
volunteer.

However, we could have stomached the unnecessarilly harsh tone had A been right in their criticism.
Conversely, we would have not been hurt as much had A formulated their mistaken criticism politely.
Sadly, A went too far when they questionned our academic integrity with respect to the PAM module.
We expect an apology on this precise point.

We understand that our work has flaws, some of them have been pointed out by reviewer B, C and D (the review of whom
we agree with and are thankful for). We 
do not ask for a free pass, what we do ask for
is a chance at getting a fair assessment of our work.


> Paper summary
> -------------
> This papers describes adding a public-key authentication scheme to the
> Plan 9's Factotum and 9P file system service.   It attempts to extend
> the existing port of Plan 9 services to Linux in a fashion that makes it
> more useful for the author's use cases.

> Strengths
> ---------
> Alas, I had a really hard time finding strengths in this paper.
> It's... clever.

The use of 'clever' as an insult is uncalled for and unprofessionnal.

> Weaknesses
> ----------
> This paper is extremely badly organized.   It is extremely hard to
> understand unless the reader has already internalized the original Plan
> 9 architecture --- at which point the non-sequitors, contradictions, and
> things that are simply wrong leap out of the page.

Reviewer A may not have internalized the original Plan 9 architecture as well
as they think they have. We dedicated 3 pages (Sec 2.1 and 2.2) to introduce the 
necessary background. We are afraid reviewer A merely skimmed it. Making a self
content paper on only 11 pages on 9P was not easy.

> Fundamentally, this paper seems to be focused from a "let's throw thigns
> together in the most convenient way as an implementation hack", and not
> from the perspective of first understanding the orignal architecture,
> and how to make the changes in a clean way.

We would like to hear more about the 'clean way'.

> Detailed feedback
> -----------------
> 1.  Introduction

> (Some of these comments here will be general ones which are touch upon
> topics covered in the introduction, but also are applicable to other
> portions of the paper.)

> This paper is very muddled because it doesn't cleanly separate issues
> which are implementation details (e.g., the fact that Kerberos uses a
> library code to do its crypto manipulation versus a separate process)
> and fundamental architectural issues from a cryptographic or protocol
> level.

Kerberos' reliance on cryptographic code, and more importantly **shared secrets**
in applicative space is a not an implementation detail but a fundamental flaw.

> The paper also doesn't describe some of the tradeoffs of monomaical
> emphasis on Network Transparency and Security Agnosticism at all costs,
> and it's not at all clear if the authors are aware that there are
> disadvantages towards a security architecture which is fundamentally
> based on a globally consistent UID namespace and where all access
> control decisions are UID based, and performed by the server host's
> kernel.

Given the limited space and the fact that implementing access control in 
application space is the status quo, we felt we had to plead our case by
extoling the kernel's innate ability to control access.

If UIDs were strings like they are in Plan 9 we would see no flaw in a globally
consistent UID namespace. Numeric UID collision is a problem. But we won't
know if it is a hard problem until we try.

> The paper only discusses using this architecture for a single
> application --- a file server --- and doesn't explore how this
> architecture would apply to more complex applications (which might want
> to do more complex access control decisions than just ones based on Unix
> permissions and file accesses --- for example, Administrative Assistance
> Albert should be allowed to add and remove mailing lists belonging to
> her department, while Departmental Administrator Barbara should be
> allowed to create create mailing lists, and I/T Administrator Charlie
> should be allowed to start and stop printers, but not add printers; that
> right is reserved to Senior I/T Tech Darlene.


This is UNIX 101, we have no idea what is so flabbergasting to reviewer A.

```
root@debian91base:/mnt# groups albert barbara charlie darlene
albert : albert administration
barbara : barbara administration depheads
charlie : charlie itjunior
darlene : darlene itsenior itjunior
root@debian91base:/mnt# ls -l
total 8
drwxrwxr-x 4 root depheads 4096 Mar 28 15:26 mailing_lists
drwxrwxr-x 2 root itsenior 4096 Mar 28 15:36 printers
root@debian91base:/mnt# ls -l mailing_lists/
total 8
drwxrwxr-x 2 root administration 4096 Mar 28 15:01 admin
drwxrwxr-x 2 root itjunior       4096 Mar 28 14:59 it

```
Albert can create and remove mailing lists for his department, but not for another department
and not globally either:
```
albert@debian91base:/mnt$ echo SOME CONF > mailing_lists/admin/HR  # Create for his dept.
albert@debian91base:/mnt$ rm mailing_lists/admin/General  # remove for his dept.
albert@debian91base:/mnt$ echo SOME CONF > mailing_lists/it/HR  # Create for another dept.
bash: mailing_lists/it/HR: Permission denied
albert@debian91base:/mnt$ rm mailing_lists/it/fluff  # Remove for another dept.
rm: remove write-protected regular file 'mailing_lists/it/fluff'? y
rm: cannot remove 'mailing_lists/it/fluff': Permission denied
albert@debian91base:/mnt$ echo SOME CONF > mailing_lists/albert_news  # Create globally
bash: mailing_lists/albert_news: Permission denied
albert@debian91base:/mnt$
```
Barbara can create a new mailing list:
```
barbara@debian91base:/mnt$ echo SOME_CONF > mailing_lists/barbara_news
barbara@debian91base:/mnt$
```
Charlie can start and stop existing printers, but cannot create printers:
```
charlie@debian91base:/mnt$ echo start >> printers/basement
charlie@debian91base:/mnt$ echo stop >> printers/lobby
charlie@debian91base:/mnt$ echo SOME CONF > printers/charlies_office
bash: printers/charlies_office: Permission denied
charlie@debian91base:/mnt$
```

Darlene is able to add printers (and make them manageable to charlie):
```
darlene@debian91base:/mnt$ echo SOME_CONF > printers/charlies_office
darlene@debian91base:/mnt$ chown darlene:itjunior printers/charlies_office
darlene@debian91base:/mnt$ chmod g+w printers/charlies_office
darlene@debian91base:/mnt$

```

In this day and age, a lot of sofware expose functionalities through HTTP
endpoints one can GET data from or PUT or POST payloads to. 9P works exactly the same 
way, only access control comes for free instead of being managed at the application
level. There is nothing remotely impossible about software exposing
its fonctionality as a virtual file system. Even on Linux, FUSE exists.

> Also not discussed is assumptions about their environment, and how it
> might be applicable more generally.  For example, an apparent non-goal
> by the authors is supporting interoperating between different
> adminsitrative boundaries, where departments such as Labotary for
> Computer Science and Media Lab might have their own username and uid
> namespaces, but still might want to engage in some distributing
> computing sharing, given that they all belong to the same University.

To A's credit, this is a fair question. This would necessitate 
mounting a 'cat overlay' FS over the two department's factotums (factota?),
which would, when queried, read one first and then the other and return
the concatenation of both. Then, as in described in the paper, point
the nss altfiles module to it.

> 2.  Network-Transparent, Security Agnostic servers and clients


> The discussion in this muddles the distinction between clients as hosts
> (e.g., "Bob's workstation") and clients as users ("Alice and Charlie are
> both using a time sharing system").  The authors also seem to be
> confused by the fact that in certain 9P man pages that they quote,
> "clients" refer to processes.  For example, in the 9pserver man page,
> the use of clients very clearly refers to processes.  The first sentence
> that they quote in section 2.2.4 makes this clear.  "The Plan 9 kernel
> multiplexes the potentially many _processes_ access the server into a
> single 9P conversation".

Reviewer A does not seem to understand that 9P is a network protocol.
It makes no difference to the server that its client is another process
on the same machine or another process on another machine. Usually, 9P servers
speak 9P on their standard IO anyway.

> The sentences: "Furthermore, 9pserve is unable to authenticate a
> connection.  It expects the server to handle Tauth message" makes no
> sense.  What "server" is referred to here?  9pserve *is* the server.  

9pserve is **both** a client and a server. As can be seen in Fig. 6., there
is a chain of software, from Alice's foo on Atlantis to Alice's u9fs on Bermuda 
that maintain a server-client relationship. Servers are upstream, clients are downstream.

Going upstream:

* Foo, via v9fs is client to srv,
* srv is 9pserve's client,
* 9pserve is vrs' client,
* vrs is u9fs client.

Going downstream:

* u9fs is vrs' server,
* vrs is 9pserve's server,
* 9pserve is srv's server,
* and srv is the server v9fs connects to, to mount the FS foo is using.

No other reviewer struggled with this.

> As
> originally designed by Plan 9, the Tauth messages are initiated by
> client kernel, and sent to 9pserve on the server machine will relay
> those messages to its factotem on the server host.

9pserve does not exist in Plan 9... It was created whithin the plan9port
project to mask the differences between Plan 9's kernel and Linux.

9pserve does nothing with a Tauth message but:
- answering with a Rerror if already attached or if the -n option was passed
- passing the Tauth message to its server (in our case, vrs)

This can be easily checked in the code:
https://9fans.github.io/usr/local/plan9/src/cmd/9pserve.c

We stand by our hypothesis that reviewer A does not understand Plan 9 as well
as they think they do.


> Something which makes **absolutely** no sense is the claim in section
> 2.4 that the afid are capabilities which must not be transmitted in the
> clear.  The afid is a small integer which *points* to a capability.  In
> Plan 9, the afid is essentially a specialized file descriptor which can
> be used so that a particular local file system operation can be executed
> with the privileges of the user corresponding to the afid.  But the
> claim that TLS is needed to protect the confidentiality of afid is
> nonsensical.  This would be roughly equivalent as claiming that if
> /sbin/passwd calls open("/etc/passwd", O_RDWR) and is given the fd 3,
> that if an adversary could know the file descriptor number, this would
> give the adversary on another machine would now have some kind of access
> to /etc/passwd.  Even if the adversary had control of a different
> process on the same machine, the file descriptor number has no meaning
> outside of the process of /sbin/passwd.  Similarly, the afid is a small
> integer that has no meaning outside of the context of the 9pserv
> process.

While reviewer A's explanation of the return value of UNIX's open is correct,
they are jumping too fast to conclusions.
They are absolutely wrong in the case of 9P, which is a network protocol.

Were we not using TLS, nothing would prevent a man in the middle attack
between srv and 9pserve. Once the attacker gets hold of the afid, they can
forge a valid Tattach message, and after have the same level of access as Alice.

On page 6, we even quoted Cox et al. saying that an afid act like a capability.
Their full paper also explain, in a section called "Transport Layer Security":

> Since the Plan 9 operating system is designed for
> use in network elements that must withstand
> direct attack, unguarded by firewall or VPN, we
> seek to ensure that all applications use channels
> with appropriate mutual authentication and
> encryption

We dedicated two full pages to schematics explaning how fids and afids are
received, multiplexed, validated, etc. We have no idea how we could have been more clear.
No other reviewer had a problem with this. This criticism is factually wrong. Reviewer A
can not claim a confidence of 3 and an expertise of 2 while making such mistakes.

> As an aside, Linux does not have this feature of using an afid to allow
> a system call be executed with the privileges of another user.  *This*
> is the reason why vrs is needed --- and why it is not needed in Plan 9.
> The paper's explanation of what vrs in 2.3.1 is and its justification
> for its existence is extremely confusing.

It seems to us the last sentence of 2.3.1 says the same thing, except that
we could not talk about fids and afids so early in the paper, this notion is
introduced in 2.3.2.


> 3.  System administration

> The use of PAM seems to add complexity and adds no value to architecture
> described in the paper.  It might be useful "if other applications use
> factotem as an auth mechanism", but that doesn't follow.  First of all,
> this use is not described in the paper, and it is claimed that it's a
> really bad idea and showed be avoided.  If that's true, why add it to
> the design at all?  Someone cynically minded might be tempted to think
> that in fact, the authors are aware that are really good reasons why
> Security Agnosticism is not all its chalked up to be, and has severe
> limitations to the sort of applications can that be implemented --- but
> the authors don't want to admit this in the paper, so they left this PAM
> hook in.


This is unacceptable. We have been quite forthcoming with the limitations of 
our setup:

* "[...] yet still early [...]", page 2
* "In our real life use-cases most of the software our users need expose their functionality
as HTTP endpoints speaking JSON or XML. Nevertheless, we contend that despite its seemingly
marginal adoption in the Linux world, 9P [...]"
* The whole 5.3 subsection
* **In the conclusion**: "Some applications still require SSH tunnelling [...]".

We also never downplayed the usefulness and popularity of the alternatives (see e.g.
the second paragraph of sec. 5.4).

Reviewer A is accusing us of academic misconduct. This is a heavy charge to bring
from somebody whith such a small understanding of the paper.

The reason why we created a PAM module is explicitely spelled out in the paper:
 "it defers the choice of the authoritative factotum to the system wide
 configuration of PAM, where an administrator would expect to find it, and
 where it is protected from malicious interference." (page 9).
 
 Reviewer A, who wants to do things "the clean way" should have appreciated this.
 
 We expect a formal apology on this precise point. One can not just throw around
 this kind of allegation.



> Secondly, given that the factotum is running as its process,
> applications that want to use factotum can simply call out to process
> directly, instead of using PAM interfaces.  The applications will need
> to be modified to use PAM in this non-standard fashion *anyway*, so why
> not change them to talk to factotum direcrtly?

Because our module **is** standard, it follows the same interface as other PAMs.
Because a minimal edit in the PAM boilerplate is better that incorporating the few dozen lines
needed to talk to factotum.

> 4.  Adding the Guillou-Qisquater ZKIP to factotum

> The security architecture is really fracturered.  Using TLS (implemented
> via stunnel) to provide server-level authentication and message
> authentication, and then layering on top of this another RSA exchange
> for user authentication is extremely complex from an architectural point
> of view.

TLS is **Transport Layer** Security while 9P deals with auth at the applicative layer.

This is clean, expected and standard.

When Reviewer A connect to their webmail, do they log in at the TLS layer ? Or do they send their login info to an HTTPS endpoint ?

> 5.  Related and future works

> This section includes a grab-bag of unrelated subsections.  Some are
> actually problem statements (hence the forward references from earler
> parts of the paper); some are related works; and some are future planned
> works.  These should be separated out.

We could create three different sections. We don't care much one way or the other.
