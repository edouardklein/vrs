(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "letterpaper" "twocolumn" "10pt" "draft")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("abstract" "original") ("caption" "justification=centering") ("fixme" "marginclue" "inline")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "settings"
    "diagram"
    "vrs"
    "acronym"
    "article"
    "art10"
    "usenix2019_v3"
    "epsfig"
    "endnotes"
    "natbib"
    "abstract"
    "amsmath"
    "amssymb"
    "url"
    "graphicx"
    "hyperref"
    "listings"
    "caption"
    "fixme")
   (TeX-add-symbols
    '("secref" ["argument"] 1)
    '("figref" ["argument"] 1)
    '("circled" 1)
    "fact")
   (LaTeX-add-labels
    "sec:example"
    "fig:aim"
    "sec:plan9port"
    "fig:AS"
    "sec:dialog"
    "fig:twofactotum"
    "sec:v9fs"
    "fig:srv"
    "sec:9pserve"
    "fig:9pserve"
    "sec:vrs"
    "fig:complete"
    "fig:vrsin"
    "fig:vrsout"
    "sec:tls"
    "sec:sysadmin"
    "sec:pam"
    "sec:factotumsysadmin"
    "sec:nss"
    "sec:crypto"
    "sec:httpover9p"
    "sec:filesharing"
    "sec:u9p"
    "sec:kerberos")
   (LaTeX-add-bibliographies
    "biblio"))
 :latex)

