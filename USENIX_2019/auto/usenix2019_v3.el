(TeX-add-style-hook
 "usenix2019_v3"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8") ("microtype" "kerning" "spacing")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "twocolumn"
    "mathptmx"
    "fontenc"
    "inputenc"
    "pslatex"
    "microtype"
    "flushend"
    "cite"
    "breakurl"
    "url"
    "xcolor"
    "hyperref")
   (TeX-add-symbols
    "maketitle"
    "thanks"
    "section")
   (LaTeX-add-environments
    "abstract"))
 :latex)

